<?php

namespace App\Imports;

use App\Models\Part;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Exception;
class PartsImport implements ToModel,WithStartRow,WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $auth_user = auth()->user();
        $partModel = new Part();
        $serial_no = $partModel->generate_serial_no();
        return new Part([
            'created_by' => $auth_user->id,
            'serial_no' => $serial_no,
            'part_number' => ($row[0])?$row[0]:"",
            'atl_pn' => $row[1],
            'atl_2_pn' => $row[2],
            'name' => ($row[3])?$row[3]:"",
            'description' => $row[4],
            'ui' => $row[5],
            'nsn' => $row[6],
            'nsn_2' => $row[7],
            'oem' => $row[8],
            'basic_details' => $row[9],
            'cage_code' => $row[10],
            'manufacturer' => $row[11],
            'niin' => $row[12],
            'codification_country' => $row[13],
            'flag' => 1,
        ]);
    }
    public function startRow(): int
    {
        return 2;
    }
    public function rules(): array
    {
        return [
            //'0' => 'required|max:255|unique:parts,part_number',
            '0' => 'max:255',
            '1' => 'max:255',
            '2' => 'max:255',
            '3' => 'max:255',
            '5' => 'max:255',
            '6' => 'max:255',
            '7' => 'max:255',
            '8' => 'max:255',
            '10' => 'max:255',
            '11' => 'max:255',
            '12' => 'max:255',
            '13' => 'max:255',

        ];
    }
    public function customValidationMessages()
    {
        return [
            '0.required' => trans('msg.part_number_req'),
            '0.unique' => trans('msg.part_number_exist'),
            '3.required' => trans('msg.part_name_req'),
        ];
    }
}
