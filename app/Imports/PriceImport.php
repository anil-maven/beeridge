<?php

namespace App\Imports;

use App\Models\Quotation;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Part;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PriceImport implements ToModel,WithStartRow,WithValidation
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $request;
    public function __construct($request)
    {
       $this->request = $request;
    }
    public function model(array $row)
    {
        $auth_user = auth()->user();
        $partData = Part::find($this->request->part_id);
        if(!empty($partData))
        {
            $data = [
                'created_by' => $auth_user->id,
                'part_id' => $this->request->part_id,
                'req_part_number' => $partData->part_number, 
                'contact_person_id' => ($this->request->contact_person_id)?$this->request->contact_person_id:0,
                'unit_price' => ($row[0])?$row[0]:"",
                'quoted_part_number' => ($row[1])?$row[1]:"",
                'alternate' =>  ($row[2])?$row[2]:"",
                'nsn' =>  ($row[3])?$row[3]:"",
                'oem' =>  ($row[4])?$row[4]:"",
                'rfq_platform'=> ($row[5])?$row[5]:"",
                'quote_via'=> ($row[6])?$row[6]:"",
                'quote_reference'=> ($row[7])?$row[7]:"",
                'condition'=> ($row[8])?$row[8]:"",
                'delivery_time'=> ($row[9])?$row[9]:"",
                'trace_certificate'=> ($row[10])?$row[10]:"",
                'shipping'=> ($row[11])?$row[11]:"",
                'payment_terms'=> ($row[12])?$row[12]:"",
                'description'=> ($row[13])?$row[13]:"",
                'remarks'=> ($row[14])?$row[14]:"",
                'price_status' => 1,
            ];
            //print_r($data);die();
            return new Quotation($data);
        }
      
    }
    public function startRow(): int
    {
        return 2;
    }
    public function rules(): array
    {
        return [
            //'0' => 'required|max:255|unique:parts,part_number',
            '0' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }
    public function customValidationMessages()
    {
        return [
            '0.required' => trans('msg.price_unit_price_req'),
            '0.regex' => trans('msg.wrong_price_format'),
          
        ];
    }
}
