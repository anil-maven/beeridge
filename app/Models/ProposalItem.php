<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use DB;

class ProposalItem extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['id'];
    function add_update_item($request)
    {
        Log::info(84);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = ProposalItem::firstOrNew(['id' => $id]);
        $obj->proposal_id = $request->proposal_id;
        $obj->part_id = $request->part_id;
        $obj->qty = $request->qty;
        $obj->description = $request->description;
        $obj->save();
        return $obj;
    }
    function search_proposal_part($request)
    {
        Log::info(85);
        DB::enableQueryLog();
        $query = ProposalItem::query();
        $query->join('parts AS t2','t2.id','proposal_items.part_id');
        $query->select('proposal_items.*','t2.part_number','t2.name','t2.serial_no','t2.manufacturer','t2.ui',DB::raw('(select quotations.unit_price from quotations where quotations.flag = 1 and quotations.proposal_item_id = proposal_items.id LIMIT 1) AS quotation_unit_price'));
        $query->where('proposal_items.proposal_id', '=',$request->proposal_id);
        $query->orderBy('proposal_items.id','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function proposal_data()
    {
        return $this->belongsTo(Proposal::class,'proposal_id','id');
    }
    function part_data()
    {
        return $this->belongsTo(Part::class,'part_id','id');
    }
    function get_single_proposal_item($id)
    {
        Log::info(86);
        return ProposalItem::with('proposal_data')->with('part_data')->findOrFail($id);
    }
    function update_client_amount($request)
    {
        Log::info(121);
        $query = ProposalItem::query();
        $query->select('proposal_items.id',DB::raw('(select quotations.unit_price from quotations where quotations.flag = 1 and quotations.proposal_item_id = proposal_items.id LIMIT 1) AS quotation_unit_price'));
        $query->where('proposal_items.id', '=',$request->proposal_item_id);
        $proposalData = $query->first();
        if(isset($proposalData->quotation_unit_price) && !empty($proposalData->quotation_unit_price))
        {
            $total_client_price = 0;
            if($request->client_price_type == 'value')
            {
                $total_client_price = $proposalData->quotation_unit_price + $request->client_price;
            }
            else
            {
                $percentage_value = ($request->client_price / 100) * $proposalData->quotation_unit_price;
                $total_client_price = $proposalData->quotation_unit_price + $percentage_value;
            }
            $obj = ProposalItem::find($request->proposal_item_id);
            $obj->client_price_type = $request->client_price_type;
            $obj->client_price = $request->client_price;
            $obj->total_client_price = $total_client_price;
            $obj->item_status = 3;
            $obj->save();
            return $obj;
        }
    }
    function check_quotation_ready_for_submit($request)
    {
        Log::info(123);
        $proposalItemData = ProposalItem::where('proposal_id','=',$request->proposal_id)->get();
        foreach ($proposalItemData as $key => $value)
        {
            if(empty($value->total_client_price))
            {
                return ['status' => 400];
            }
        }
        return ['status' => 200];
    }
    function export_client_proposal($request)
    {
        Log::info(130);
        $query = ProposalItem::query();
        $query->join('parts AS t2','t2.id','proposal_items.part_id');
        $query->select('t2.part_number','t2.serial_no','proposal_items.qty','proposal_items.total_client_price',DB::raw('(proposal_items.total_client_price * proposal_items.qty) AS total_price'),'t2.ui');
        $query->where('proposal_items.proposal_id', '=',$request->proposal_id);
        $query->orderBy('proposal_items.id','DESC');
        $data = $query->get();
        return $data;
    }
    function update_new_client_amount($request)
    {
        Log::info(132);
        if(isset($request->client_proposal_data) && count($request->client_proposal_data) != 0)
        {
            foreach ($request->client_proposal_data AS $key => $value)
            {
                $obj = ProposalItem::find($value['proposal_item_id']);
                $obj->new_client_price = $value['new_client_price'];
                $obj->save();
            }
        }
        return true;
    }
}
