<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Role extends Model
{
    use HasFactory;
    protected $fillable = ['id'];

    function search_roles($request)
    {
        Log::info(39);
        $query = Role::query();
        $query->select('roles.*');
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('roles.created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('role_name', 'like','%'.$request->search_string.'%')
                ;
            });
        }
        $query->orderBy('roles.created_at','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function get_single_role($role_id)
    {
        Log::info(45);
        return Role::findOrFail($role_id);
    }
    function add_update_role($request)
    {
        Log::info(47);
        $newModuleArr = [];
        foreach ($request->module_name as $key => $value)
        {
            $newModuleArr[$value] = ['All'];
        }
        $sub_module_data = json_encode($newModuleArr);
        $moduleArr = $request->module_name;
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $roleModel = Role::firstOrNew(['id' => $id]);
        if($id == 0)
            $roleModel->created_by = $request->created_by;
        $roleModel->role_name = $request->role_name;
        $roleModel->modules = $sub_module_data;
   /*     $roleModel->reporting_to = $request->reporting_to;
        $roleModel->organization_id = $request->organization_id;*/
        if($id == 0)
            $roleModel->flag = 1;

        //$roleModel->status = isset($request->status) && !empty($request->status)?$request->status:0;
        $roleModel->status = 1;
        $roleModel->save();
        $response = $roleModel;
        //RolePermission::where('role_id','',$response->id)->delete();
        foreach ($moduleArr as $key2 => $value2)
        {
            $rolePermissionModel = RolePermission::firstOrNew(['role_id' =>$response->id,'module_name' => $value2,'sub_module_name' => 'All']);
            $rolePermissionModel->role_id =$response->id;
            $rolePermissionModel->module_name = $value2;
            $rolePermissionModel->sub_module_name = 'All';
            $rolePermissionModel->save();
        }
        return true;
    }
    function delete_single_role($role_id)
    {
        Log::info(50);
        Role::destroy($role_id);
        return true;
    }
    function get_active_role_list()
    {
        Log::info(51);
        return Role::where('status','=',1)->where('id','!=',1)->get();
    }
}
