<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Quotation extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['id','created_by','part_id','req_part_number','quoted_part_number','alternate','description','nsn','oem','rfq_platform','quote_via','quote_reference','contact_person_id','condition','unit_price','delivery_time','trace_certificate','shipping','payment_terms','remarks','price_status'];
    function add_update_quotation($request)
    {
     
        Log::info(112);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = Quotation::firstOrNew(['id' => $id]);
        if($id == 0)
        $obj->created_by = $request->created_by;
        $obj->proposal_item_id = $request->proposal_item_id;
        $obj->part_id = ($request->part_id)?$request->part_id:0;
        $obj->req_part_number = $request->req_part_number;
        $obj->quoted_part_number = $request->quoted_part_number;
        $obj->alternate = $request->alternate;
        $obj->description = $request->description;
        $obj->nsn = $request->nsn;
        $obj->oem = $request->oem;
        $obj->rfq_platform = $request->rfq_platform;
        $obj->quote_via = $request->quote_via;
        $obj->quote_reference = $request->quote_reference;
        $obj->contact_person_id = $request->contact_person_id;
        $obj->condition = $request->condition;
        $obj->unit_price = $request->unit_price;
        $obj->delivery_time = $request->delivery_time;
        $obj->trace_certificate = $request->trace_certificate;
        $obj->shipping = $request->shipping;
        $obj->payment_terms = $request->payment_terms;
        $obj->remarks = $request->remarks;
        $obj->save();
        ProposalItem::where('id','=',$request->proposal_item_id)->update(['item_status' => 1]);
        return $obj;
    }
    function proposal_item()
    {
        return $this->belongsTo(ProposalItem::class,'proposal_item_id','id');
    }
    function search_quotation($request)
    {
        Log::info(114);
        $query = Quotation::query();
        $query->leftJoin('parts AS t2','t2.id','=','quotations.part_id');
        $query->select('quotations.*','t2.ui');
        $query->where('quotations.price_status','=',0); 
        $query->with('proposal_item');
        $query->with('contact_person');
        $query->where('quotations.proposal_item_id', '=',$request->proposal_item_id);

       /* if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('contact_person.name', 'like','%'.$request->search_string.'%')
                    ->orWhere('contact_person.email',  'like','%'.$request->search_string.'%')
                    ->orWhere('contact_person.department',  'like','%'.$request->search_string.'%');
            });

        }*/
        $query->orderBy('quotations.created_at','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function contact_person()
    {
        return $this->belongsTo(ContactPerson::class,'contact_person_id','id');
    }
    function get_single_quotation($id)
    {
        Log::info(116);
        return Quotation::with('proposal_item')->with('contact_person')->find($id);
    }
    function update_quotation_flag($quotation_id)
    {
        Log::info(119);
        Quotation::where('id','=',$quotation_id)->update(['flag' => 1]);
        $quotationData = Quotation::find($quotation_id);
        ProposalItem::where('id','=',$quotationData->proposal_item_id)->update(['item_status' => 2]);
        return true;
    }
    function get_selected_quotation($proposal_item_id)
    {
        Log::info(128);
        return Quotation::with('contact_person')->where('proposal_item_id','=',$proposal_item_id)->where('flag','=',1)->first();
    }
    function add_update_part_price($request)
    {
        Log::info(135);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = Quotation::firstOrNew(['id' => $id]);
        if($id == 0)
        $obj->created_by = $request->created_by;
        $obj->part_id = $request->part_id;
        $obj->unit_price = $request->unit_price;
        $obj->price_status = 1;

        $obj->req_part_number = $request->req_part_number;
        $obj->quoted_part_number = $request->quoted_part_number;
        $obj->alternate = $request->alternate;
        $obj->description = $request->description;
        $obj->nsn = $request->nsn;
        $obj->oem = $request->oem;
        $obj->rfq_platform = $request->rfq_platform;
        $obj->quote_via = $request->quote_via;
        $obj->quote_reference = $request->quote_reference;
        $obj->contact_person_id = $request->contact_person_id;
        $obj->condition = $request->condition;
        $obj->delivery_time = $request->delivery_time;
        $obj->trace_certificate = $request->trace_certificate;
        $obj->shipping = $request->shipping;
        $obj->payment_terms = $request->payment_terms;
        $obj->remarks = $request->remarks;

        $obj->save();
        return $obj;
    }
    function part_data()
    {
        return $this->belongsTo(Part::class,'part_id','id');
    }
    function search_part_price($request)
    {
        Log::info(138);
        $query = Quotation::query();
        $query->join('parts','parts.id','=','quotations.part_id');
        $query->select('quotations.*');
        $query->with('part_data');
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) 
            {
                $q->where('parts.serial_no', 'like','%'.$request->search_string.'%')
                    ->orWhere('parts.part_number',  'like','%'.$request->search_string.'%')
                    ;
            });
        }
       

        $query->where('quotations.price_status','=',1); 
        $query->orderBy('quotations.created_at','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function get_single_part_price($id)
    {
        Log::info(139);
        return Quotation::with('part_data')->find($id);
    }
    function get_part_price_history($request)
    {
        Log::info(144);
        $query = Quotation::query();
        $query->select('quotations.*');
        $query->with('part_data');
        $query->where('quotations.part_id', '=',$request->part_id);
        $query->orderBy('quotations.id','DESC');
        $data = $query->limit(10)->get();
        return $data;
    }

}
