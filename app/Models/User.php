<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Log;
use DB;
use File;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    // GET USER FULL NAME
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
    public function getCreatedAtAttribute($value)
    {
        return  dateFormat($value);
    }
    public function getUserRoleNameAttribute()
    {
        if(!empty($this->role_id))
        {
            $roleData = Role::where('id','=',$this->role_id)->value('role_name');
            if(!empty($roleData))
                return $roleData;
        }
        return "";

    }
    // GET USER FULL IMAGE URL
    public function getFullImageUrlAttribute()
    {
        if(empty($this->image))
            return  asset('/').'assets/img/user.png';
        else
        {
            if(File::exists('public/storage/image/'.$this->image))
                return   asset('/storage/image').'/'.$this->image;
            else
                return  asset('/').'assets/img/user.png';
        }


    }
    // UPDATE USER PROFILE DETAILS
    function update_user_profile($request)
    {
        Log::info(11);
        $obj = User::find($request->id);
        $obj->first_name = $request->first_name;
        $obj->last_name = $request->last_name;
        if($request->hasFile('image')) {
            if (!empty($obj->image)) {
                $full_path_dest = 'storage/app/public/image/' . $obj->image;
                @unlink($full_path_dest);
            }
            $obj->image = upload_image($request->image,'profile_image_');
        }
        $obj->save();
        return $obj;
    }
    function add_update_user($request)
    {
        Log::info(52);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = User::firstOrNew(['id' => $id]);
        if($id == 0)
            $obj->created_by = $request->created_by;
        $obj->first_name = $request->first_name;
        $obj->last_name = $request->last_name;
        $obj->email = $request->email;
        $obj->role_id = $request->role_id;
        if(isset($request->password) && !empty($request->password))
            $obj->password = bcrypt($request->password);
        $obj->status = isset($request->status) && !empty($request->status)?$request->status:0;
        if($request->hasFile('image')) {
            if (!empty($obj->image)) {
                $full_path_dest = 'storage/app/public/image/' . $obj->image;
                @unlink($full_path_dest);
            }
            $obj->image = upload_image($request->image,'user_');
        }
        $obj->save();
        return $obj;
    }
    function search_users($request)
    {
        Log::info(54);
        $query = User::query();
        $query->join('roles AS t3','t3.id','=','users.role_id');
        $query->select('users.*','t3.role_name');
        $query->where('users.role_id', '!=',1);
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('users.created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where(DB::raw('CONCAT(users.first_name," ",users.last_name)'), 'like','%'.$request->search_string.'%')
                    ->orWhere('users.email',  'like','%'.$request->search_string.'%')
                    ->orWhere('t3.role_name',  'like','%'.$request->search_string.'%');
            });

        }
        if(isset($request->filter_order_by) && $request->filter_order_by == 'Name-ASC')
            $query->orderBy('users.first_name','ASC');
        elseif(isset($request->filter_order_by) && $request->filter_order_by == 'Name-DESC')
            $query->orderBy('users.first_name','DESC');
        if(isset($request->filter_order_by) && $request->filter_order_by == 'Date-ASC')
            $query->orderBy('users.created_at','ASC');
        elseif(isset($request->filter_order_by) && $request->filter_order_by == 'Date-DESC')
            $query->orderBy('users.created_at','DESC');
        else
            $query->orderBy('users.created_at','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function get_single_user($id)
    {
        return User::find($id);
    }
    function delete_single_user($id)
    {
        Log::info(57);
        $data = User::find($id);
        if(!empty($data))
        {
            $full_path_dest = 'storage/app/public/image/' . $data->image;
            @unlink($full_path_dest);
            $data->delete();
        }
        return true;
    }
    function export_user_data($request)
    {
        Log::info(60);
        $query = User::query();
        $query->join('roles AS t3','t3.id','=','users.role_id');
        $query->select('users.first_name','users.last_name','email','t3.role_name','users.created_at');
        $query->where('users.role_id', '!=',1);
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('users.created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where(DB::raw('CONCAT(users.first_name," ",users.last_name)'), 'like','%'.$request->search_string.'%')
                    ->orWhere('users.email',  'like','%'.$request->search_string.'%')
                    ->orWhere('t3.role_name',  'like','%'.$request->search_string.'%');
            });

        }
        if(isset($request->filter_order_by) && $request->filter_order_by == 'Name-ASC')
            $query->orderBy('users.first_name','ASC');
        elseif(isset($request->filter_order_by) && $request->filter_order_by == 'Name-DESC')
            $query->orderBy('users.first_name','DESC');
        if(isset($request->filter_order_by) && $request->filter_order_by == 'Date-ASC')
            $query->orderBy('users.created_at','ASC');
        elseif(isset($request->filter_order_by) && $request->filter_order_by == 'Date-DESC')
            $query->orderBy('users.created_at','DESC');
        else
            $query->orderBy('users.created_at','DESC');
        $data = $query->get();
        return $data;
    }
    // UPDATE USER STATUS
    function update_user_status($request)
    {
        Log::info(61);
        $obj = User::find($request->user_id);
        $obj->status = $request->status;
        $obj->save();
        return $obj;
    }
    function get_admin_data()
    {
        Log::info(89);
        return User::where('role_id','=',1)->first();
    }
}
