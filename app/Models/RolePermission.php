<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class RolePermission extends Model
{
    use HasFactory;
    protected $fillable = ['id'];
    function get_role_permission_list($request)
    {
        Log::info(41);
        $roleData = Role::find($request->role_id);
        $sub_module_arr = "";
        foreach (json_decode($roleData->modules) as $key => $value)
        {
            if($key == $request->module_name)
            {
                $sub_module_arr =  $value;
            }
        }
        $query = RolePermission::query();
        $query->whereIn('sub_module_name',$sub_module_arr);
        $query->where('role_id','=',$request->role_id);
        $query->where('module_name','=',$request->module_name);
        $data = $query->get();
        return $data;
    }
    function update_role_permission($request)
    {
        Log::info(43);
        $column_name = $request->column_name;
        $obj = RolePermission::find($request->permission_id);
        $obj->$column_name = $request->checkbox_value;
        $obj->save();
        return $obj;
    }
}
