<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ContactPerson extends Model
{
    protected $table = 'contact_person';
    use HasFactory;
    protected $fillable = ['name'];
    function add_update_contact_person($request)
    {
        Log::info(102);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = ContactPerson::firstOrNew(['id' => $id]);
        if($id == 0)
        $obj->created_by = $request->created_by;
        $obj->supplier = $request->supplier;
        $obj->name = $request->name;
        $obj->email = $request->email;
        $obj->phone_number = isset($request->phone_number) && !empty($request->phone_number)?$request->phone_number:"";
        $obj->department = $request->department;
        $obj->save();
        return $obj;
    }
    function search_contact_person($request)
    {
        Log::info(104);
        $query = ContactPerson::query();
        $query->select('contact_person.*');
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('contact_person.created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('contact_person.name', 'like','%'.$request->search_string.'%')
                    ->orWhere('contact_person.supplier',  'like','%'.$request->search_string.'%')
                    ->orWhere('contact_person.email',  'like','%'.$request->search_string.'%')
                    ->orWhere('contact_person.department',  'like','%'.$request->search_string.'%');
            });

        }
        $query->orderBy('contact_person.created_at','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function get_single_contact_person($id)
    {
        Log::info(106);
        return ContactPerson::find($id);
    }
    function get_contact_person_list($request)
    {
        Log::info(109);
        $query = ContactPerson::query();
        $query->select('id','supplier','name','email');
        if($request->role_id != 1)
            $query->where('created_by','=',$request->created_by);
        if(isset($request->term['term']) && !empty($request->term['term']))
        {
            $query->where(function ($q) use ($request){
                $q->where('name','like','%'.$request->term['term'])
                    ->orWhere('email','like','%'.$request->term['term'].'%')
                    ->orWhere('supplier','like','%'.$request->term['term'].'%');
            });
        }
        $query->limit(5);
        $data = $query->get();
        return $data;
    }
}
