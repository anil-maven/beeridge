<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Module extends Model
{
    use HasFactory;
    protected $fillable = ['id'];
    function get_module_list()
    {
        Log::info(14);
        $auth_user = auth()->user();
        $query = Module::query();
        $query->select('modules.*');
        if($auth_user->role_id > 1)
        {
            $moduleArr = [];
            $roleData = Role::find($auth_user->role_id);
            $roleModule = json_decode($roleData->modules);
            foreach ($roleModule as $key => $value)
            {
                array_push($moduleArr,$key);
            }
            $query->whereIn('modules.module_name',$moduleArr);
        }
        $query->where('modules.status','=',1);
        $query->where('modules.parent_id','=',0);
        $query->orderBy('modules.order_no','ASC');
        return  $query->get();
    }
    public function child_module()
    {
        return $this->hasMany('App\Models\Module', 'parent_id', 'id')
            ->select('modules.*')
            ->where('status','=',1)
            ->orderBy('modules.order_no', 'ASC');
    }
    function get_module_with_sub_module_list()
    {
        Log::info(79);
        $query = Module::query();
        $query->with('child_module');
        $query->where('status','=',1);
        $query->where('parent_id','=',0);
        $query->orderBy('order_no','ASC');
        return  $query->get();
    }
}
