<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;
class Notification extends Model
{
    use HasFactory;
    public function getCreatedAtAttribute($value)
    {
        return  Carbon::parse($value)->diffForHumans();
    }
    function create_notification($request)
    {
        Log::info(63);
        $obj = new Notification();
        $obj->sender_id = $request->sender_id;
        $obj->receiver_id = $request->receiver_id;
        if(isset($request->title) && !empty($request->title))
            $obj->title = $request->title;
        $obj->msg = $request->msg;
        if(isset($request->url) && !empty($request->url))
            $obj->url = $request->url;
        $obj->save();
        return true;
    }
    function search_notification($request)
    {
        Log::info(64);
        $query = Notification::query();
        $query->join('users AS t2','t2.id','=','notifications.sender_id');
        $query->join('roles AS t3','t3.id','=','t2.role_id');
        $query->where('notifications.receiver_id', '=',$request->receiver_id);
        $query->select('notifications.*',DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS sender_name'),'t3.role_name');
        $query->orderBy('notifications.id','DESC');
        $data = $query->paginate(10);
        return $data;
    }
}
