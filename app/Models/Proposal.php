<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use DB;

class Proposal extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['proposal_name'];
    public function getCreatedAtAttribute($value)
    {
        return  dateFormat($value);
    }
    function add_update_proposal($request)
    {
        Log::info(70);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = Proposal::firstOrNew(['id' => $id]);
        $obj->created_by = $request->created_by;
        $obj->proposal_name = $request->proposal_name;
        $obj->save();
        return $obj;
    }
    function search_proposal($request)
    {
        Log::info(72);
        $query = Proposal::query();
        $query->join('users AS t2','t2.id','proposals.created_by');
        $query->select('proposals.*',DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS created_by_name'),DB::raw('(SELECT SUM(proposal_items.qty) FROM  proposal_items WHERE proposal_items.proposal_id = proposals.id) AS total_item'));
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('proposals.created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('proposal_name', 'like','%'.$request->search_string.'%');
            });
        }
        $query->orderBy('id','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function get_single_proposal($id)
    {
        Log::info(74);
        $query = Proposal::query();
        $query->leftJoin('users AS t2','t2.id','=','proposals.service_provider');
        $query->leftJoin('users AS t3','t3.id','=','proposals.created_by');
        $query->where('proposals.id','=',$id);
        $query->select('proposals.*',DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS service_provider_name'),DB::raw('CONCAT(t3.first_name," ",t3.last_name) AS created_by_name'));
        return $query->first();
    }
    function update_proposal_client_want_status($request)
    {
        Log::info(91);
        $obj = Proposal::find($request->proposal_id);
        $obj->client_want_status = $request->client_want_status;
        $obj->status = 1;
        $obj->faq_receive_date = Carbon::now();
        $obj->save();
        return $obj;
    }
    function search_rfq($request)
    {
        Log::info(94);
        $query = Proposal::query();
        $query->join('users AS t2','t2.id','proposals.created_by');
        $query->select('proposals.*',DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS created_by_name'),DB::raw('(SELECT SUM(proposal_items.qty) FROM  proposal_items WHERE proposal_items.proposal_id = proposals.id) AS total_item'));
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->service_provider) && !empty($request->service_provider))
                $query->where('proposals.service_provider', '=',$request->service_provider);
        }
        $query->where('proposals.client_want_status','!=',0);
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('proposal_name', 'like','%'.$request->search_string.'%');
            });
        }
        if(isset($request->search_type) && $request->search_type == 'RFQ')
            $query->orderBy('faq_receive_date','DESC');
        else
            $query->orderBy('created_at','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function update_submit_quotation_status($request)
    {
        Log::info(124);
        Proposal::where('id','=',$request->proposal_id)->update(['quotation_status' => 1,'status' => 2]);
        $proposalData = Proposal::find($request->proposal_id);
        ProposalItem::where('proposal_id','=',$request->proposal_id)->update(['item_status' => 4]);
        return $proposalData;
    }
}
