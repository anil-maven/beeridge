<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use DB;

class Part extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['id','created_by','serial_no','part_number','name','atl_pn','atl_2_pn','description','ui','nsn','nsn_2','oem','basic_details','cage_code','manufacturer','niin','codification_country','flag'];
    public function getPartImageUrlAttribute()
    {
        if(empty($this->part_image))
            return  "";
        else
            return  asset('/storage/image').'/'.$this->part_image;

    }
    public function getPartVideoUrlAttribute()
    {
        if(empty($this->part_video))
            return  "";
        else
            return  asset('/storage/video').'/'.$this->part_video;

    }
    public function getCreatedAtAttribute($value)
    {
        return  dateFormat($value);
    }
    function add_update_part_details($request)
    {
        Log::info(17);
        $id = isset($request->id) && !empty($request->id)?$request->id:0;
        $obj = Part::firstOrNew(['id' => $id]);
        if($id == 0)
        {
            $obj->created_by = $request->created_by;
            $obj->serial_no = $this->generate_serial_no();
        }
        $obj->part_number = $request->part_number;
        $obj->name = $request->name;
        $obj->atl_pn = $request->atl_pn;
        $obj->atl_2_pn = $request->atl_2_pn;
        $obj->description = $request->description;
        $obj->ui = $request->ui;
        $obj->nsn = $request->nsn;
        $obj->nsn_2 = $request->nsn_2;
        $obj->oem = $request->oem;
        $obj->basic_details = $request->basic_details;
        $obj->cage_code = $request->cage_code;
        $obj->manufacturer = $request->manufacturer;
        $obj->niin = $request->niin;
        $obj->codification_country = $request->codification_country;
        $obj->flag = $request->flag;
        if($request->hasFile('part_image')) {
            if (!empty($obj->part_image)) {
                $full_path_dest = 'storage/app/public/image/' . $obj->part_image;
                @unlink($full_path_dest);
            }
            $obj->part_image = upload_image($request->part_image,'part_image_');
        }
        if($request->hasFile('part_video')) {
            if (!empty($obj->part_video)) {
                $full_path_dest = 'storage/app/public/video/' . $obj->part_video;
                @unlink($full_path_dest);
            }
            $obj->part_video = upload_video($request->part_video,'part_video_');
        }
        $obj->save();
        return $obj;
    }
    function generate_serial_no()
    {
        $data =  DB::table('parts')->select('serial_no')->orderBy('id','DESC')->first();
        if(empty($data))
        {
            $new_serial_no = Date('Ymd').'-0001';
        }
        else{

            $splitData = explode('-',$data->serial_no);
            if(isset($splitData[1]) && !empty($splitData[1]))
            {
                $next = $splitData[1]+1;
                $date_stamp = Date('Ymd');
                if($splitData[0] != $date_stamp)
                    $next = 1;
                $nextValue = str_pad($next, 4, "0", STR_PAD_LEFT);
                $new_serial_no = $date_stamp.'-'.$nextValue;
            }
        }
        $check = DB::table('parts')->where('serial_no','=',$new_serial_no)->first();
        if(empty($check))
            return $new_serial_no;
        else
            $this->generate_serial_no();
    }
    function get_single_part_details($id)
    {
        Log::info(20);
        return Part::findOrFail($id);
    }
    function search_parts($request)
    {
        Log::info(24);
        $query = Part::query();
        $query->join('users AS t2','t2.id','=','parts.created_by');
        $query->select('parts.*',DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS created_by_name'));
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('parts.created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('parts.serial_no', 'like','%'.$request->search_string.'%')
                    ->orWhere('parts.part_number',  'like','%'.$request->search_string.'%')
                    ->orWhere('parts.atl_pn',  'like','%'.$request->search_string.'%')
                    ->orWhere('parts.atl_2_pn',  'like','%'.$request->search_string.'%')
                    ->orWhere('parts.oem',  'like','%'.$request->search_string.'%')
                    ->orWhere('parts.manufacturer',  'like','%'.$request->search_string.'%')
                    ->orWhere('parts.name',  'like','%'.$request->search_string.'%');
            });
        }
        if(isset($request->var_filter_status) && !empty($request->var_filter_status))
        {
            $query->where(function ($q) use ($request) {
                if(isset($request->search_by_serial_no) && !empty($request->search_by_serial_no))
                    $q->where('parts.serial_no', 'like','%'.$request->search_by_serial_no.'%');
                if(isset($request->search_by_part_number) && !empty($request->search_by_part_number))
                    $q->orWhere('parts.part_number',  'like','%'.$request->search_by_part_number.'%');
                if(isset($request->search_by_name) && !empty($request->search_by_name))
                    $q->orWhere('parts.name',  'like','%'.$request->search_by_name.'%');
                if(isset($request->search_by_atl_pn) && !empty($request->search_by_atl_pn))
                    $q->orWhere('parts.atl_pn',  'like','%'.$request->search_by_atl_pn.'%');
                if(isset($request->search_by_atl_2_pn) && !empty($request->search_by_atl_2_pn))
                    $q->orWhere('parts.atl_2_pn',  'like','%'.$request->search_by_atl_2_pn.'%');
                if(isset($request->search_by_oem) && !empty($request->search_by_oem))
                    $q->orWhere('parts.oem',  'like','%'.$request->search_by_oem.'%');
                if(isset($request->search_by_manufacturer) && !empty($request->search_by_manufacturer))
                    $q->orWhere('parts.manufacturer',  'like','%'.$request->search_by_manufacturer.'%');
                if(isset($request->search_by_codification_country) && !empty($request->search_by_codification_country))
                    $q->orWhere('parts.codification_country',  'like','%'.$request->search_by_codification_country.'%');
            });
        }

        $query->orderBy('parts.id','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function delete_single_part($part_id)
    {
        Log::info(2444);
        $data = Part::find($part_id);
        if(!empty($data))
        {
            $full_path_dest = 'storage/app/public/image/' . $data->part_image;
            @unlink($full_path_dest);
            $full_path_dest = 'storage/app/public/video/' . $data->part_video;
            @unlink($full_path_dest);
            $data->delete();
        }
        return true;
    }
    function export_part_data($request)
    {
        Log::info(28);
        $query = Part::query();
        $query->select('serial_no','part_number','name','atl_pn','atl_2_pn','description','ui','nsn','nsn_2','oem','basic_details','cage_code','manufacturer','niin','codification_country','created_at');
        if(isset($request->role_id) && $request->role_id != 1)
        {
            if(isset($request->created_by) && !empty($request->created_by))
                $query->where('created_by', '=',$request->created_by);
        }
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('serial_no', 'like','%'.$request->search_string.'%')
                    ->orWhere('part_number',  'like','%'.$request->search_string.'%')
                    ->orWhere('name',  'like','%'.$request->search_string.'%');
            });
        }
        if(isset($request->var_filter_status) && !empty($request->var_filter_status))
        {
            $query->where(function ($q) use ($request) {
                if(isset($request->search_by_serial_no) && !empty($request->search_by_serial_no))
                    $q->where('serial_no', 'like','%'.$request->search_by_serial_no.'%');
                if(isset($request->search_by_part_number) && !empty($request->search_by_part_number))
                    $q->orWhere('part_number',  'like','%'.$request->search_by_part_number.'%');
                if(isset($request->search_by_name) && !empty($request->search_by_name))
                    $q->orWhere('name',  'like','%'.$request->search_by_name.'%');
                if(isset($request->search_by_atl_pn) && !empty($request->search_by_atl_pn))
                    $q->orWhere('atl_pn',  'like','%'.$request->search_by_atl_pn.'%');
                if(isset($request->search_by_atl_2_pn) && !empty($request->search_by_atl_2_pn))
                    $q->orWhere('atl_2_pn',  'like','%'.$request->search_by_atl_2_pn.'%');
                if(isset($request->search_by_oem) && !empty($request->search_by_oem))
                    $q->orWhere('oem',  'like','%'.$request->search_by_oem.'%');
                if(isset($request->search_by_manufacturer) && !empty($request->search_by_manufacturer))
                    $q->orWhere('manufacturer',  'like','%'.$request->search_by_manufacturer.'%');
                if(isset($request->search_by_codification_country) && !empty($request->search_by_codification_country))
                    $q->orWhere('codification_country',  'like','%'.$request->search_by_codification_country.'%');
            });
        }

        $query->orderBy('id','DESC');
        $data = $query->get();
        return $data;
    }
    // GET PART LIST
    function get_part_list($request)
    {
        Log::info(29);
        $query = Part::query();
        $query->select('id','serial_no','part_number','name','atl_pn','oem','manufacturer','part_image');
        if($request->role_id != 1)
            $query->where('created_by','=',$request->created_by);
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('serial_no', 'like','%'.$request->search_string.'%')
                    ->orWhere('part_number',  'like','%'.$request->search_string.'%')
                    ->orWhere('name',  'like','%'.$request->search_string.'%');
            });
        }
        if(isset($request->part_id_arr) && count($request->part_id_arr) != 0)
        {
            $query->whereNotIn('id',$request->part_id_arr);
        }

        if(isset($request->term['term']) && !empty($request->term['term']))
        $query->where('part_number','like','%'.$request->term['term'].'%');
        $query->limit(5);
        $data = $query->get();
        return $data;
    }
    // GET PART LIST FOR PROPOSAL
    function get_part_list_for_proposal($request)
    {
        Log::info(78);
        $query = Part::query();
        $query->select('id','part_number');
        if(isset($request->term['term']) && !empty($request->term['term']))
            $query->where('part_number','like','%'.$request->term['term'].'%');
        $query->limit(10);
        $data = $query->get();
        return $data;
    }

}
