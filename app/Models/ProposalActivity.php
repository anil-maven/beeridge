<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ProposalActivity extends Model
{
    use HasFactory;
    function create_activity($request)
    {
        Log::info(87);
        $activityData = $this->get_proposal_icon($request->proposal_key);
        $obj = new ProposalActivity();
        $obj->created_by = $request->created_by;
        $obj->proposal_id = $request->proposal_id;
        $obj->icon = $activityData['icon'];
        $obj->title = $activityData['title'];
        $obj->save();
        return $obj;
    }
    function get_proposal_icon($proposal_key)
    {
        $arr = [
            ['icon' => 'draft.png','title' => 'Proposal Created','key' => 'proposal_created'],
            ['icon' => 'add.png','title' => 'Item Created','key' => 'item_created'],
            ['icon' => 'send2.png','title' => 'RFQ Sent','key' => 'rfq_sent'],
            ['icon' => 'search2.png','title' => 'Searching Quotation','key' => 'search_quotation'],
            ['icon' => 'QuotationSaved.png','title' => 'Quotation Saved','key' => 'saved_quotation'],
        ];
        foreach ($arr as $key => $value)
        {
            if($value['key'] == $proposal_key)
            {
                return $value;
            }
        }
    }
    function get_proposal_activity($proposal_id)
    {
        Log::info(88);
        $query = ProposalActivity::query();
        $query->where('proposal_id','=',$proposal_id);
        $data = $query->get();
        return $data;
    }

}
