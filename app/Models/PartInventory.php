<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class PartInventory extends Model
{
    use HasFactory;
    public function getCreatedAtAttribute($value)
    {
        return  dateFormat($value);
    }
    function add_part_inventory($request)
    {
        Log::info(32);
        $obj = new PartInventory();
        $obj->created_by = $request->created_by;
        $obj->part_id = $request->part_id;
        $obj->credit_qty = $request->credit_qty;
        $obj->save();
        return $obj;
    }
    function search_part_inventory($request)
    {
        Log::info(34);
        $query = PartInventory::query();
        $query->join('parts AS t2','t2.id','part_inventories.part_id');
        $query->select('part_inventories.*','t2.part_number','t2.serial_no','t2.name');
        if(isset($request->created_by) && !empty($request->created_by))
            $query->where('part_inventories.created_by', '=',$request->created_by);
        if(isset($request->search_string) && !empty($request->search_string))
        {
            $query->where(function ($q) use ($request) {
                $q->where('serial_no', 'like','%'.$request->search_string.'%')
                    ->orWhere('part_number',  'like','%'.$request->search_string.'%')
                    ->orWhere('name',  'like','%'.$request->search_string.'%');
            });
        }
        $query->orderBy('id','DESC');
        $data = $query->paginate(10);
        return $data;
    }
    function delete_single_inventory($inventory_id)
    {
        Log::info(34);
        PartInventory::destroy($inventory_id);
        return true;
    }
}
