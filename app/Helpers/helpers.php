<?php

use App\Models\Module;
use App\Models\RolePermission;
use Illuminate\Support\Facades\Log;
use App\Models\Role;
function upload_image($filename, $img_name = 'image_')
{
    Log::info(10);
    $file = $filename;
    $filetype = $file->getClientOriginalExtension();
    $destinationPath = 'storage/app/public/image/';
    $image_name = $img_name;
    $new_file_name = $image_name.Date('d_m_Y').'_'.time().'.'.$filetype;
    $file->move($destinationPath,$new_file_name);
    return $new_file_name;
}
function upload_video($filename, $img_name = 'video_')
{
    Log::info(10);
    $file = $filename;
    $filetype = $file->getClientOriginalExtension();
    $destinationPath = 'storage/app/public/video/';
    $image_name = $img_name;
    $new_file_name = $image_name.Date('d_m_Y').'_'.time().'.'.$filetype;
    $file->move($destinationPath,$new_file_name);
    return $new_file_name;
}
function dateFormat($timeStamp='')
{
    $date = Date('Y-m-d g:i a', strtotime($timeStamp));
    return $date;
}
function check_module()
{
    Log::info(13);
    $segment1 = request()->segment(1);
    $segment2 = request()->segment(2);
    $moduleModel = new Module();
    $moduleData = $moduleModel->get_module_list();
    $data['module_data'] = $moduleData;
    $check_module  = 0;
    if($segment1 == 'dashboard')
    {
        $data['sub_module_data'] = [];
        $data['status'] = 200;
        return  $data;
    }
    if(!empty($moduleData))
    {
        foreach ($moduleData AS $key => $value)
        {
            $explode_data = explode('/',$value->url);
            if(isset($explode_data[1]) && $segment1 == $explode_data[1])
                $check_module  = 1;
        }
    }
    if($check_module == 0)
        return ['status' => 400,'url' => url('/dashboard')];
   // $subModuleData = $moduleModel->get_sub_module_list();
    $check_module  = 0;
    //   $check_path  = '/'.request()->path();
    $check_path  = '/'.$segment1;
    if(!empty($segment2))
        $check_path  = '/'.$segment1.'/'.$segment2;
   /* if(!empty($subModuleData))
    {
        foreach ($subModuleData as $key => $value)
        {
            if($check_path == $value->url)
            {
                $check_module = 1;
            }
        }
    }*/

   /* if($check_module == 0)
    {
        if(isset($subModuleData[0]->url) && !empty($subModuleData[0]->url))
            return ['status' => 400,'url' => $subModuleData[0]->url];
        return ['status' => 400,'url' => url('/dashboard')];
    }
    $data['sub_module_data'] = $subModuleData;*/
    $data['status'] = 200;
    return $data;
}
function role($module_name,$sub_module_name,$permission)
{
    Log::info(62);
    $check = false;
    $auth_user = auth()->user();
    if($auth_user->role_id == 1)
        $check = true;
    else{
        $roleData = Role::find($auth_user->role_id);
        $roleModule = json_decode($roleData->modules);
        $checkModule = 0;
        foreach ($roleModule as $key => $value)
        {
            if($key == $module_name)
            {
                $checkModule = 1;
                break;
            }
        }
        if($checkModule == 1)
        {
            $roleData = RolePermission::where('role_id','=',$auth_user->role_id)->where('module_name',$module_name)->where('sub_module_name',$sub_module_name)->value($permission);
            if($roleData)
                $check = true;
        }

    }
    return $check;
}
function get_client_proposal_status($status)
{
    if($status == 0)
    {
        return ['btn' => '<button type="button" class="btn btn_draft btn-sm">Pending</button>'];
    }
    elseif($status == 1)
    {
        return ['btn' => '<button type="button" class="btn sty_sent btn-sm">RFQ Sent</button>'];
    }
    elseif($status == 2)
    {
        return ['btn' => '<button type="button" class="btn btn_recived btn-sm">RFQ Received</button>'];
    }
    return  ['btn' => 'NA'];
}

function get_sp_proposal_status($status)
{
    if($status == 0)
    {
        return ['btn' => '<button type="button" class="btn btn_draft btn-sm">Pending</button>'];
    }
    elseif($status == 1)
    {
        return ['btn' => '<button type="button" class="btn sty_sent btn-sm">RFQ Received</button>'];
    }
    elseif($status == 2)
    {
        return ['btn' => '<button type="button" class="btn btn_recived btn-sm">Submitted</button>'];
    }
    return  ['btn' => 'NA'];
}
function convert($money)
{
    return number_format($money, 2);
}
function get_part_units()
{
    return [
        ['unit' => 'EA',], //Each
        ['unit' => 'MTR'],
        ['unit' => 'SET'], //singleton
        ['unit' => 'KG'], // Kilogram
        ['unit' => 'GR'], // Gram
        ['unit' => 'QT'], // Quintal
        ['unit' => 'GL'], //Gallon
        ['unit' => 'GR'],

    ];
   
}
function get_rfq_item_status($status)
{
    if($status == 0)
    {
        return ['btn' => '<button type="button" class="btn btn_draft btn-sm">Pending</button>'];
    }
    elseif($status == 1)
    {
        return ['btn' => '<button type="button" class="btn sty_sent btn-sm">Search price</button>'];
    }
    elseif($status == 2)
    {
        return ['btn' => '<button type="button" class="btn sty_sent btn-sm">Select price</button>'];
    }
    elseif($status == 3)
    {
        return ['btn' => '<button type="button" class="btn sty_sent btn-sm">Update price</button>'];
    }
    elseif($status == 4)
    {
        return ['btn' => '<button type="button" class="btn btn_recived btn-sm">Submitted</button>'];
    }
    return  ['btn' => 'NA'];
}
