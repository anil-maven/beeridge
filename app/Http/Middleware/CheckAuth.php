<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::check())
        {
            return redirect(url('/'));
        }
        if(Auth::user()->status != 1)
        {
            Auth::logout();
            return redirect(url('/').'?status=account_block');
        }
        return $next($request);
    }
}
