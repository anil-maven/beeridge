<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Quotation;
use App\Models\Part;
use App\Imports\PriceImport;
use PHPUnit\Exception;
use Excel;
class PriceController extends Controller
{
    // LOAD PRICE VIEW
    function price_view()
    {
        Log::info(133);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.price');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $form_data = [];
        $data['html_form'] = view('price.add_price_modal',$form_data);
        $data['import_html_form'] = view('price.price_import_modal',$form_data);
        return view('price.price_list',$data);
    }
    // ADD UPDATE PRICE DETAILS
    function add_update_price(Request $request)
    {
        Log::info(134);
        $rules['unit_price'] = 'required|numeric|min:1';
        $msg['unit_price.required'] = trans('msg.price_unit_price_req');
        $msg['unit_price.min'] = trans('msg.price_unit_price_min');
        $request->validate($rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $partData = Part::find($request->part_id);
        $request->req_part_number = $partData->part_number;
        $response = (new Quotation())->add_update_part_price($request);
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_updated_part_price'):trans('msg.succ_added_part_price');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // SEARCH PRICE DATA
    function search_price(Request $request)
    {
        Log::info(136);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $data['data'] = (new Quotation())->search_part_price($request);
        return view('price.price_table',$data);
    }
    // GET SINGLE PRICE DETAIL FOR EDIT
    function single_price(Request $request)
    {
        Log::info(137);
        $response = (new Quotation())->get_single_part_price($request->id);
        $form_data['data'] = $response;
        return view('price.add_price_modal',$form_data);
    }
    // DELETE PRICE DETAIL
    function delete_price(Request $request)
    {
        Log::info(140);
        Quotation::destroy($request->id);
        return response()->json(['status' => 200,'msg' => trans('msg.succ_delete_part_price')],200);
    }
    // GET PART PRICE HISTORY
    function part_price_history(Request $request)
    {
        Log::info(141);
        $response = (new Quotation())->get_part_price_history($request);
        $form_data['data'] = $response;
        return view('price.part_price_history',$form_data);
    }
    // IMPORT PRICE DATA
    function import_price_data(Request $request)
    {
        Log::info(142);
        $rules['part_id'] = 'required';
        $rules['price_import'] = 'required';
        $msg['part_id.required'] = trans('msg.part_id_req');
        $msg['price_import.required'] = 'File is required.';
        $request->validate($rules,$msg);
        try {
            $response =  Excel::import(new PriceImport($request), $request->file('price_import'));
            return response()->json(['status' => 200,'msg' => 'Import Successfully'],200);
            
        }
        catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $error)
            {
                $e = json_decode(json_encode($error));
                $msg = $e->errors[0].' At '.$e->row.' Row';
                return response()->json(['status' => 400,'msg' => $msg],400);
            }
        }
        catch (Exception $e) {
            $failures = $e->failures();
            foreach ($failures as $error)
            {
                $e = json_decode(json_encode($error));
                $msg = $e->errors[0].' At '.$e->row.' Row';
                return response()->json(['status' => 400,'msg' => $msg],400);
            }
        }
    }
    // SHOW PRICE DETAILS
    function view_price_details(Request $request)
    {
        Log::info(143);
        $quotationData = (new Quotation())->get_single_quotation($request->price_id);
        $form_data['quotation_data'] = $quotationData;
        $partModel = new Part();
        $form_data['data'] = $partModel->get_single_part_details($quotationData->part_id);
        return view('price.single_price',$form_data);
    }
    
}
