<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    // LOAD DASHBOARD VIEW
    function dashboard_view()
    {
        Log::info(3);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.dashboard');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        return view('dashboard',$data);
    }
    // UPDATE USER PROFILE DETAILS
    function update_profile(Request $request)
    {
        Log::info(9);
        $rules['first_name'] = 'required|string|max:255|regex:/^[a-zA-Z]*$/';
        $rules['last_name'] = 'required|string|max:255|regex:/^[a-zA-Z]*$/';
        if($request->hasFile('image'))
            $rules['image'] = 'required|mimes:jpeg,jpg,png,gif';

        $msg['first_name.required'] = trans('msg.first_name_req');
        $msg['last_name.required'] = trans('msg.last_name_req');
        $msg['first_name.regex'] = trans('msg.first_name_invalid');
        $msg['last_name.regex'] = trans('msg.last_name_invalid');
        $this->validate($request,$rules,$msg);

        $auth_user = auth()->user();
        $request->id = $auth_user->id;
        $userModel = new User();
        $response = $userModel->update_user_profile($request);
        $resp_msg = trans('msg.succ_update_profile');
        return response()->json(['status' => 200, 'msg' => $resp_msg,'full_image_url' => $response->full_image_url,'full_name' => $response->full_name], 200);
    }
    // CHANGE PASSWORD
    function change_password(Request $request)
    {
        Log::info(12);
        $rules['old_password'] = 'required';
        $rules['password'] = 'required|min:6|different:old_password';
        $msg['old_password.required'] = trans('msg.old_password_req');
        $msg['password.required'] = trans('msg.new_password_req');
        $msg['password.min'] = trans('msg.new_min_password');
        $msg['password.different'] = trans('msg.new_password_different');
        $this->validate($request,$rules,$msg);
        $auth_user = auth()->user();
        $user  = User::find($auth_user->id);
        if(Hash::check($request->old_password, $user->password))
        {
            $user->fill(['password' => Hash::make($request->password)])->save();
            return response()->json(['status' => 200, 'msg' => trans('msg.succ_update_password')], 200);
        } else
        {
            $password['old_password'][] = trans('msg.wrong_old_password');
            return response()->json(['status' => 422, 'errors' => $password], 422);
        }
    }
    // LOAD NOTIFICATION VIEW
    function notification_view()
    {
        Log::info(65);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.notification');
        $moduleModel = new Module();
        $data['module_data'] = $moduleModel->get_module_list();
        return view('notification',$data);
    }
    // SEARCH NOTIFICATION
    function search_notification(Request $request)
    {
        Log::info(66);
        $auth_user = auth()->user();
        $request->receiver_id = $auth_user->id;

        $notifiModel = new Notification();
        $data['data'] = $notifiModel->search_notification($request);
        return view('search_notification_table',$data);
    }
    // DELETE NOTIFICATION
    function delete_single_notification(Request $request)
    {
        Log::info(67);
        if(isset($request->notification_id) && !empty($request->notification_id))
        {
            Notification::destroy($request->notification_id);
        }
        return response()->json(['status' => 200,'msg' => trans('msg.succ_delete_notification')],200);
    }
}
