<?php

namespace App\Http\Controllers;

use App\Models\Part;
use App\Models\Proposal;
use App\Models\ProposalActivity;
use App\Models\ProposalItem;
use App\Models\Quotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RFQController extends Controller
{
    // LOAD RFQ LIST VIEW
    function rfq_view()
    {
        Log::info(92);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.single_rfq');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        return view('rfq.rfq_list',$data);
    }
    // SEARCH RFQ DATA
    function search_rfq(Request $request)
    {
        Log::info(93);
        $auth_user = auth()->user();
        $request->service_provider = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $request->search_type = 'RFQ';
        $proposalModel = new Proposal();
        $data['data'] = $proposalModel->search_rfq($request);
        return view('rfq.rfq_table',$data);
    }
    // LOAD SINGLE FAQ VIEW
    function single_rfq_view(Request $request)
    {
        Log::info(95);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.single_rfq');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data' ] = $moduleData['module_data'];
        $proposalModel = new Proposal();
        $data['data'] = $proposalModel->get_single_proposal($request->id);
        $data['item_count'] = ProposalItem::where('proposal_id','=',$request->id)->sum('qty');
        $activityModel = new ProposalActivity();
        $activityData = $activityModel->get_proposal_activity($request->id);
        $data['proposal_activity_html'] = view('rfq.rfq_activity',['data' => $activityData])->render();
        return view('rfq.single_rfq',$data);
    }
    // SEARCH PROPOSAL ITEM
    function search_rfq_item(Request $request)
    {
        Log::info(96);
        $itemModel = new ProposalItem();
        $data['data'] = $itemModel->search_proposal_part($request);
        return view('rfq.rfq_part_table',$data);
    }
    // GET SINGLE PROPOSAL ITEM DETAIL FOR EDIT
    function single_rfq_item(Request $request)
    {
        Log::info(97);
        $itemModel = new ProposalItem();
        $response = $itemModel->get_single_proposal_item($request->id);
        $form_data['proposal_item_data'] = $response;
        $partModel = new Part();
        $form_data['data'] = $partModel->get_single_part_details($response->part_id);
        $form_data['part_id'] = $response->part_id;
        $form_data['proposal_id'] = $response->proposal_id;
        return view('rfq.single_rfq_item',$form_data);
    }
    // LOAD FAQ DETAIL VIEW
    function rfq_details(Request $request)
    {
        Log::info(98);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.rfq_detail');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $proposalModel = new Proposal();
        $data['data'] = $proposalModel->get_single_proposal($request->id);
        return view('rfq.rfq_details',$data);
    }
    // SEARCH RFQ ITEM DETAILS
    function search_rfq_item_details(Request $request)
    {
        Log::info(99);
        $data['proposal_data'] = Proposal::find($request->proposal_id);
        $itemModel = new ProposalItem();
        $data['data'] = $itemModel->search_proposal_part($request);
        return view('rfq.rfq_detail_table',$data);
    }
    // UPDATE RFQ CLIENT PRICE
    function update_client_price(Request $request)
    {
        Log::info(120);
        $response = (new ProposalItem())->update_client_amount($request);
        return response()->json(['status' => 200,'response' => $response,'msg' => trans('msg.succ_update_client_price')],200);
    }

}
