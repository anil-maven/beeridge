<?php

namespace App\Http\Controllers;

use App\Exports\ClientQuotationExport;
use App\Models\Notification;
use App\Models\Part;
use App\Models\Proposal;
use App\Models\ProposalActivity;
use App\Models\ProposalItem;
use App\Models\Quotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Excel;

class QuotationController extends Controller
{
    function quotation_view(Request $request)
    {
        Log::info(108);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.rfq_price_detail');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $proposalModel = new ProposalItem();
        $data['data'] = $proposalModel->get_single_proposal_item($request->proposal_item_id);
        
        $data['contact_person_html_form'] = view('contact_person.add_contact_person_modal',[]);
        $form_data['proposal_item_id'] = $request->proposal_item_id;
        $form_data['proposal_data'] = $data['data'];
        $data['html_form'] = view('quotation.add_quotation_modal',$form_data);
        return view('quotation.quotation_list',$data);
    }
    //ADD-UPDATE QUOTATION
    function add_update_quotation(Request $request)
    {
        Log::info(111);
        $rules['contact_person_id'] = 'required';
       // $rules['quoted_part_number'] = 'required';
        $rules['unit_price'] = 'required|min:1';
        $msg['contact_person_id.required'] = trans('msg.contact_person_id_req');
        $msg['supplier_company.required'] = trans('msg.supplier_company_req');
        $msg['quoted_part_number.required'] = trans('msg.quoted_part_number_req');
        $msg['unit_price.required'] = trans('msg.unit_price_req');
        $request->validate($rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $proposalItemData = (new ProposalItem())->get_single_proposal_item($request->proposal_item_id);
        $request->req_part_number = $proposalItemData->part_data->part_number;
        $request->part_id = $proposalItemData->part_data->id;
        $response = (new Quotation())->add_update_quotation($request);
        if(isset($proposalItemData->proposal_data->id) && !empty($proposalItemData->proposal_data->id))
        {
            $check = ProposalActivity::where('proposal_id','=',$proposalItemData->proposal_data->id)->where('title','=','Searching Quotation')->first();
            if(empty($check))
            {
                $proposalObj = (object)[];
                $proposalObj->created_by = $auth_user->id;
                $proposalObj->proposal_id = $proposalItemData->proposal_data->id;
                $proposalObj->proposal_key = 'search_quotation';
                $proposalModel = new ProposalActivity();
                $proposalModel->create_activity($proposalObj);
            }
        }
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_quotation'):trans('msg.succ_added_quotation');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // SEARCH QUOTATION DETAILS
    function search_quotation(Request $request)
    {
        Log::info(113);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $data['data'] = (new Quotation())->search_quotation($request);
        return view('quotation.quotation_table',$data);
    }
    // GET SINGLE QUOTATION
    function get_single_quotation(Request $request)
    {
        Log::info(115);
        $response = (new Quotation())->get_single_quotation($request->quotation_id);
        $form_data['proposal_item_id'] = $response->proposal_item_id;
        $form_data['proposal_data'] = (new ProposalItem())->get_single_proposal_item($response->proposal_item_id);
        $form_data['data'] = $response;
        return view('quotation.add_quotation_modal',$form_data);
    }
    // DELETE QUOTATION
    function delete_quotation(Request $request)
    {
        Log::info(117);
        Quotation::destroy($request->quotation_id);
        return response()->json(['status' => 200,'msg' => trans('msg.succ_delete_quotation')],200);
    }
    // UPDATE QUOTATION STATUS
    function update_quotation_status(Request $request)
    {
        Log::info(118);
        $response = (new Quotation())->update_quotation_flag($request->quotation_id);
        return response()->json(['status' => 200,'msg' => trans('msg.succ_update_quotation_flag')],200);
    }
    // SUBMIT QUOTATION TO CLIENT
    function submit_quotation_to_client(Request $request)
    {
        Log::info(122);
        $auth_user = auth()->user();
        $check_item = (new ProposalItem())->check_quotation_ready_for_submit($request);
        if($check_item['status'] == 400)
            return response()->json(['status' => 400,'msg' => trans('msg.first_select_quotation')],400);

        $response = (new Proposal())->update_submit_quotation_status($request);
        $proposalObj = (object)[];
        $proposalObj->created_by = $auth_user->id;
        $proposalObj->proposal_id = $request->proposal_id;
        $proposalObj->proposal_key = 'saved_quotation';
        $proposalModel = new ProposalActivity();
        $proposalModel->create_activity($proposalObj);

        $notifiModel = new Notification();
        $notifiObj = (object)[];
        $notifiObj->sender_id = $auth_user->id;
        $notifiObj->receiver_id = $response->created_by;
        $notifiObj->title = trans('notification.user_get_quotation_submitted_title');
        $notifiObj->msg = str_replace(['__USERNAME','__PROPOSAL_NAME'],[$auth_user->full_name,$response->proposal_name],trans('notification.user_get_quotation_submitted_msg'));
        $notifiObj->url = '/proposal/quotation/'.$response->id;
        $notifiModel->create_notification($notifiObj);
        return response()->json(['status' => 200,'msg' => trans('msg.succ_submit_quotation_to_client')],200);
    }
    // LOAD FAQ DETAIL VIEW
    function client_proposal_quotation(Request $request)
    {
        Log::info(125);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.quotation_detail');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $proposalModel = new Proposal();
        $data['data'] = $proposalModel->get_single_proposal($request->id);
        $data['single_proposal_item'] = ProposalItem::where('proposal_id','=',$request->id)->first();
        return view('proposal.quotation_details',$data);
    }
    function search_client_quotation(Request $request)
    {
        Log::info(126);
        $data['proposal_data'] = Proposal::find($request->proposal_id);
        $itemModel = new ProposalItem();
        $data['data'] = $itemModel->search_proposal_part($request);
        $data['quotation_status'] = isset($request->quotation_status) && !empty($request->quotation_status)?$request->quotation_status:"";
        return view('proposal.client_quotation_table',$data);

    }
    // GET SINGLE PROPOSAL ITEM DETAIL FOR EDIT
    function single_rfq_item_with_quotation(Request $request)
    {
        Log::info(127);
        $itemModel = new ProposalItem();
        $response = $itemModel->get_single_proposal_item($request->id);
        $form_data['proposal_item_data'] = $response;
        $partModel = new Part();
        $form_data['data'] = $partModel->get_single_part_details($response->part_id);
        $form_data['part_id'] = $response->part_id;
        $form_data['proposal_id'] = $response->proposal_id;
        $form_data['quotation_data'] = (new Quotation())->get_selected_quotation($request->id);
        return view('quotation.single_quotation_with_rfq',$form_data);
    }
    // EXPORT CLIENT QUOTATION
    function export_client_quotation(Request $request)
    {
        Log::info(129);
        $file_name = 'quotation_'.date('d_m_Y_h_i_s').'.xlsx';
        return Excel::download(new ClientQuotationExport($request), $file_name);
    }
    // UPDATE RFQ CLIENT NEW PRICE
    function update_client_new_price(Request $request)
    {
        Log::info(131);
        $response = (new ProposalItem())->update_new_client_amount($request);
        return response()->json(['status' => 200,'response' => $response,'msg' => trans('msg.succ_update_client_price')],200);
    }
}
