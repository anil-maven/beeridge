<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RoleController extends Controller
{
    // LOAD ROLE AND PERMISSION VIEW
    function role_permission_view()
    {
        Log::info(37);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.role_and_permission');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $moduleModel = new Module();
        $role_data['module_with_sub_data'] = $moduleModel->get_module_with_sub_module_list();
        $data['html_form'] = view('roles.add_role_modal',$role_data);
        return view('roles.roles_list',$data);
    }
    // SEARCH ROLES
    function search_roles(Request $request)
    {
        Log::info(38);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $roleModel = new Role();
        $data['data'] = $roleModel->search_roles($request);
        return view('roles.roles_table',$data);
    }
    // GET ROLE PERMISSION DETAILS
    function get_role_permission_view(Request $request)
    {
        Log::info(40);
        $rolePermissionModel = new RolePermission();
        $data['data'] = $rolePermissionModel->get_role_permission_list($request);
        return view('roles.role_permission_table',$data);
    }
    // UPDATE ROLE PERMISSION DETAILS
    function update_permission_status(Request $request)
    {
        Log::info(42);
        $rolePermissionModel = new RolePermission();
        $response = $rolePermissionModel->update_role_permission($request);
        return response()->json(['status' => 200],200);
    }
    // GET SINGLE ROLE DETAILS
    function get_single_role_detail(Request $request)
    {
        Log::info(44);
        $roleModel = new Role();
        $roleData = $roleModel->get_single_role($request->role_id);
        $role_data['data'] = $roleData;
        $moduleModel = new Module();
        $role_data['module_with_sub_data'] = $moduleModel->get_module_with_sub_module_list();

        /*   $roleModel = new Role();
          $role_data['role_data'] = $roleModel->get_role_list($roleData->organization_id);
          */
        return view('roles.add_role_modal',$role_data);
    }
    // ADD UPDATE ROLES
    function add_update_roles(Request $request)
    {
        Log::info(46);
        $rules['role_name'] = 'required|string|max:255';
       /* $rules['sub_module_name'] = 'required';*/
     /*   $rules['organization_id'] = 'required|min:1';
        $rules['reporting_to'] = 'required|min:1';*/
        $msg['role_name.required'] = trans('msg.role_name_req');
        $msg['role_name.regex'] = trans('msg.role_name_invalid');
        $msg['organization_id.required'] = trans('msg.organization_id_req');
        $msg['reporting_to.required'] = trans('msg.reporting_to_req');
        $this->validate($request,$rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $roleModel = new Role();
        $response = $roleModel->add_update_role($request);
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_role_details'):trans('msg.succ_add_role_details');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // DELETE SINGLE ROLE
    function delete_single_role(Request $request)
    {
        Log::info(48);
        $checkUserData = User::where('role_id','=',$request->role_id)->first();
        if(!empty($checkUserData))
            return response()->json(['status' => 400,'msg' => trans('msg.employee_exist_under_role')],400);

        $roleModel = new Role();
        $response = $roleModel->delete_single_role($request->role_id);
        return response()->json(['status' => 200,'response' => $response,'msg' => trans('msg.succ_delete_role_details')],200);
    }
}
