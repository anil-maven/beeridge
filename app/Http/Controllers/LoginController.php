<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use Auth;
use Mail;
use DB;
use Hash;
use Illuminate\Support\Str;
class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // LOAD LOGIN VIEW
    function login_view()
    {
        Log::info('1');
        $data['page_title'] = trans('page_title.login_view');
        return view('login_view',$data);
    }
    // LOGIN API
    function login_web(Request $request)
    {
        Log::info(2);
        $rules['email'] = 'required|email';
        $rules['password'] = 'required';
        $msg['email.required'] = trans('msg.email_req');
        $msg['email.email'] = trans('msg.email_invalid');
        $msg['password.required'] = trans('msg.password_req');
        $validator = Validator::make($request->all(), $rules, $msg);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1], $request->remember)) {
            return redirect()->intended(url('/dashboard'));
        }
        return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
            'password' => trans('msg.credential_wrong'),
        ]);
    }
    // LOGOUT
    function logout()
    {
        Log::info(4);
        auth()->logout();
        return redirect(url('/'));
    }
    // LOAD FORGE PASSWORD PAGE
    function forget_password()
    {
        Log::info(5);
        $data['page_title'] = trans('page_title.forget_password');
        return view('forget_password', $data);
    }
    // SEND FORGET PASSWORD LINK
    function send_forget_password_link(Request $request)
    {
        Log::info(6);
        if (isset($request->email) && !empty($request->email)) {
            $check = User::where('email', '=', $request->email)->where('status', '=', '1')->first();
            if (empty($check))
                return redirect()->back()->withErrors(['error_email' => trans('msg.email_not_register')])->withInput();
        }
        $token = Str::random(64);
        DB::beginTransaction();
        try {
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            Mail::send('email.forget_password', ['token' => $token], function ($message) use ($request) {
                $message->to($request->email);
                $message->subject('Reset Password');
            });
            DB::commit();
            return back()->with('success', 'We have e-mailed your password reset link!');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['error_email' => $e->getMessage()])->withInput();
        }
    }
    // SHOW RESET PASSWORD FORM
    public function showResetPasswordForm($token)
    {
        Log::info(7);
        return view('reset_password', ['token' => $token]);
    }
    // SUBMIT RESET PASSWORD FORM
    public function submitResetPasswordForm(Request $request)
    {
        Log::info(8);
        $rules['token'] = 'required';
        $rules['password'] = 'required|min:6';
        $rules['confirm_password'] = 'required';
        $msg['password.required'] = trans('msg.new_password_req');
        $msg['password.min'] = trans('msg.new_min_password');
        $request->validate($rules,$msg);

        $updatePassword = DB::table('password_resets')->where(['token' => $request->token])->first();
        if (!$updatePassword) {
            return back()->withInput()->with('error', 'Invalid token!');
        }
        $user = User::where('email', $updatePassword->email)
            ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email' => $updatePassword->email])->delete();
        return redirect('/')->with('success', 'Your password has been changed!');
    }

}
