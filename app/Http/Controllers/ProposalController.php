<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Part;
use App\Models\Proposal;
use App\Models\ProposalActivity;
use App\Models\ProposalItem;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
class ProposalController extends Controller
{
    // LOAD PROPOSAL VIEW
    function proposal_view()
    {
        Log::info(68);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.proposal');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $data['html_form'] = view('proposal.add_proposal_modal',[]);
        if($authUser->role_id == 1)
            $data['proposal_count'] = Proposal::count();
        else
            $data['proposal_count'] = Proposal::where('created_by','=',$authUser->id)->count();

        return view('proposal.proposal_list',$data);
    }
    // ADD-UPDATE PROPOSAL DETAILS
    function add_update_proposal(Request $request)
    {
        Log::info(69);
        $rules['proposal_name'] = 'required|max:250';
        $msg['proposal_name.required'] = trans('msg.proposal_name_req');
        $this->validate($request,$rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $proposalModel = new Proposal();
        $response = $proposalModel->add_update_proposal($request);
        $url = '';
        if(!isset($request->id) || empty($request->id))
        {
            $proposalObj = (object)[];
            $proposalObj->created_by = $auth_user->id;
            $proposalObj->proposal_id = $response->id;
            $proposalObj->proposal_key = 'proposal_created';
            $proposalModel = new ProposalActivity();
            $proposalModel->create_activity($proposalObj);
            $url = url('/proposal/view/'.$response->id);
            $userModel = new User();
            $adminData = $userModel->get_admin_data();

            $notifiModel = new Notification();
            $notifiObj = (object)[];
            $notifiObj->sender_id = $auth_user->id;
            $notifiObj->receiver_id = $adminData->id;
            $notifiObj->title = trans('notification.user_get_proposal_add_title');
            $notifiObj->msg = str_replace(['__USERNAME','__PROPOSAL_NAME'],[$auth_user->full_name,$response->proposal_name],trans('notification.user_get_proposal_add_msg'));
            $notifiObj->url = '/proposal';
            $notifiModel->create_notification($notifiObj);
        }
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_proposal'):trans('msg.succ_added_proposal');
        return response()->json(['status' => 200, 'msg' => $resp_msg,'url' => $url], 200);
    }
    // SEARCH PROPOSAL
    function search_proposal(Request $request)
    {
        Log::info(71);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $proposalModel = new Proposal();
        $data['data'] = $proposalModel->search_proposal($request);
        return view('proposal.proposal_table',$data);
    }
    // GET SINGLE PROPOSAL FOR EDIT
    function single_proposal_detail(Request $request)
    {
        Log::info(73);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $proposalModel = new Proposal();
        $response = $proposalModel->get_single_proposal($request->proposal_id);
        $form_data['data'] = $response;
        return view('proposal.add_proposal_modal',$form_data);
    }
    // DELETE SINGLE PROPOSAL
    function delete_single_proposal(Request $request)
    {
        Log::info(75);
        Proposal::destroy($request->proposal_id);
        return response()->json(['status' => 200,'msg' => trans('msg.succ_delete_proposal')],200);
    }
    // SINGLE PROPOSAL VIEW
    function single_proposal_view(Request $request)
    {
        Log::info(76);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.proposal');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $proposalModel = new Proposal();
        $data['data'] = $proposalModel->get_single_proposal($request->id);
        $data['item_count'] = ProposalItem::where('proposal_id','=',$request->id)->sum('qty');
        $activityModel = new ProposalActivity();
        $activityData = $activityModel->get_proposal_activity($request->id);
        $data['proposal_activity_html'] = view('proposal.proposal_activity',['data' => $activityData])->render();
        $data['proposal_item_type_html'] = view('proposal.add_proposal_item_type')->render();
        return view('proposal.single_proposal',$data);
    }
    // GET PART LIST
    function get_part_list(Request $request)
    {
        Log::info(77);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $partModel = new Part();
        $data = $partModel->get_part_list_for_proposal($request);
        return response()->json($data);
    }
    // GET SINGLE PART/ITEM FOR ADD ITEM
    function single_item_detail(Request $request)
    {
        Log::info(79);
        $auth_user = auth()->user();
        $partModel = new Part();
        $response = $partModel->get_single_part_details($request->part_id);
        $form_data['data'] = $response;
        $form_data['part_id'] = $request->part_id;
        $form_data['proposal_id'] = $request->proposal_id;
        return view('proposal.add_proposal_item',$form_data);
    }
    // ADD-UPDATE PROPOSAL ITEM DETAILS
    function add_update_proposal_item(Request $request)
    {
        Log::info(80);
        $auth_user = auth()->user();
        $rules['part_id'] = 'required';
        $rules['qty'] = 'required';
        $rules['description'] = 'required';
        $msg['qty.required'] = trans('msg.proposal_item_qty_req');
        $msg['description.required'] = trans('msg.proposal_item_description_req');
        $this->validate($request,$rules,$msg);
        $itemModel = new ProposalItem();
        $response = $itemModel->add_update_item($request);
        $proposal_activity_html = "";
        if(!isset($request->id) || empty($request->id))
        {
            $proposalObj = (object)[];
            $proposalObj->created_by = $auth_user->id;
            $proposalObj->proposal_id = $response->proposal_id;
            $proposalObj->proposal_key = 'item_created';
            $proposalModel = new ProposalActivity();
            $proposalModel->create_activity($proposalObj);
            $activityModel = new ProposalActivity();
            $activityData = $activityModel->get_proposal_activity($response->proposal_id);
            $proposal_activity_html = view('proposal.proposal_activity',['data' => $activityData])->render();

        }
        $proposalData = Proposal::where('id','=',$response->proposal_id)->first();
        if(isset($proposalData->service_provider) && !empty($proposalData->service_provider))
        {
            $serviceProviderData = User::find($proposalData->service_provider);
        }
        else
        {
            $serviceProviderData = User::where('role_id','=',2)->first();
            if(isset($serviceProviderData->id) && !empty($serviceProviderData->id))
            {
                Proposal::where('id','=',$response->proposal_id)->update(['service_provider' => $serviceProviderData->id]);
            }
        }
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_proposal_item'):trans('msg.succ_added_proposal_item');
        $item_count = ProposalItem::where('proposal_id','=',$response->proposal_id)->sum('qty');
        $service_provider = $serviceProviderData->full_name;
        return response()->json(['status' => 200, 'msg' => $resp_msg,'item_count' => $item_count,'service_provider' => $service_provider,'proposal_activity_html' => $proposal_activity_html], 200);
    }
    // SEARCH PROPOSAL ITEM
    function search_proposal_item(Request $request)
    {
        Log::info(81);
      /*  $auth_user = auth()->user();
        $request->created_by = $auth_user->id;*/
        $data['proposal_data'] = (new Proposal())->get_single_proposal($request->proposal_id);
        $itemModel = new ProposalItem();
        $data['data'] = $itemModel->search_proposal_part($request);

        return view('proposal.proposal_part_table',$data);
    }
    // GET SINGLE PROPOSAL ITEM DETAIL FOR EDIT
    function single_proposal_item(Request $request)
    {
        Log::info(82);
        $itemModel = new ProposalItem();
        $response = $itemModel->get_single_proposal_item($request->id);
        $form_data['proposal_item_data'] = $response;
        $partModel = new Part();
        $form_data['data'] = $partModel->get_single_part_details($response->part_id);
        $form_data['part_id'] = $response->part_id;
        $form_data['proposal_id'] = $response->proposal_id;
        return view('proposal.add_proposal_item',$form_data);
    }
    // DELETE PROPOSAL ITEM
    function delete_proposal_item(Request $request)
    {
        Log::info(83);
        ProposalItem::destroy($request->id);
        $item_count = ProposalItem::where('proposal_id','=',$request->proposal_id)->sum('qty');
        return response()->json(['status' => 200,'item_count' => $item_count,'msg' => trans('msg.succ_delete_proposal_item')],200);
    }
    // UPDATE PROPOSAL  CLIENT WANT STATUS
    function update_client_want_status(Request $request)
    {
        Log::info(90);
        $auth_user = auth()->user();
        $proposalModel = new Proposal();
        $response = $proposalModel->update_proposal_client_want_status($request);

        $proposalObj = (object)[];
        $proposalObj->created_by = $auth_user->id;
        $proposalObj->proposal_id = $response->id;
        $proposalObj->proposal_key = 'rfq_sent';
        $proposalModel = new ProposalActivity();
        $proposalModel->create_activity($proposalObj);

        if(!empty($response->service_provider))
        {
            $notifiModel = new Notification();
            $notifiObj = (object)[];
            $notifiObj->sender_id = $auth_user->id;
            $notifiObj->receiver_id = $response->service_provider;
            $notifiObj->title = trans('notification.user_get_rfq_sent_title');
            $notifiObj->msg = str_replace(['__USERNAME','__PROPOSAL_NAME'],[$auth_user->full_name,$response->proposal_name],trans('notification.user_get_rfq_sent_msg'));
            $notifiObj->url = '/rfq/view/'.$response->id;
            $notifiModel->create_notification($notifiObj);
        }
        return response()->json(['status' => 200,'msg' => trans('msg.succ_rfq_sent')],200);
    }

}
