<?php

namespace App\Http\Controllers;

use App\Models\ContactPerson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ContactPersonController extends Controller
{
    // LOAD CONTACT PERSON VIEW
    function contact_person_view()
    {
        Log::info(100);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.contact_person');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $form_data = [];
        $data['html_form'] = view('contact_person.add_contact_person_modal',$form_data);
        return view('contact_person.contact_person_list',$data);
    }
    // ADD-UPDATE CONTACT PERSON
    function add_update_contact_person(Request $request)
    {
        Log::info(101);
        $rules['supplier'] = 'required|max:250';
        $rules['name'] = 'max:250';
        if(isset($request->email) && !empty($request->email))
        {
            if(isset($request->id) && !empty($request->id))
                $rules['email'] = 'email|unique:contact_person,email,'.$request->id;
            else
                $rules['email'] = 'email|unique:contact_person';
        }

        //$rules['phone_number'] = 'required';
        $rules['department'] = 'max:250';

        $msg['supplier.required'] = trans('msg.supplier_name_req');
        $msg['supplier.max'] = trans('msg.supplier_name_limit');
        $msg['name.required'] = trans('msg.contact_person_name_req');
        $msg['name.max'] = trans('msg.contact_person_name_limit');
        $msg['email.required'] = trans('msg.email_req');
        $msg['email.unique'] = trans('msg.email_unique');
        $msg['phone_number.required'] = trans('msg.contact_person_phone_number_req');
        $msg['department.required'] = trans('msg.contact_person_department_req');
        $msg['department.max'] = trans('msg.contact_person_department_limit');
        $this->validate($request,$rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $personModel = new ContactPerson();
        $response = $personModel->add_update_contact_person($request);
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_contact_person'):trans('msg.succ_added_contact_person');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // SEARCH CONTACT PERSON
    function search_contact_person(Request $request)
    {
        Log::info(103);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $personModel = new ContactPerson();
        $data['data'] = $personModel->search_contact_person($request);
        return view('contact_person.contact_person_table',$data);
    }
    // GET SINGLE CONTACT PERSON DETAIL
    function single_contact_person(Request $request)
    {
        Log::info(105);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $personModel = new ContactPerson();
        $response = $personModel->get_single_contact_person($request->person_id);
        $form_data['data'] = $response;
        return view('contact_person.add_contact_person_modal',$form_data);
    }
    // DELETE CONTACT PERSON
    function delete_contact_person(Request $request)
    {
        Log::info(107);
        ContactPerson::destroy($request->person_id);
        return response()->json(['status' => 200,'msg' => trans('msg.succ_delete_contact_person')],200);
    }
    // GET CONTACT PERSON LIST
    function get_contact_person_list(Request $request)
    {
        Log::info(110);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $data = (new ContactPerson())->get_contact_person_list($request);
        return response()->json($data);
    }
}
