<?php

namespace App\Http\Controllers;

use App\Exports\UserExport;
use App\Models\Notification;
use App\Models\Proposal;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Excel;
class UserController extends Controller
{
    // LOAD USERS VIEW
    function user_view()
    {
        Log::info(48);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.users');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $roleModule = new Role();
        $form_data['role_data'] = $roleModule->get_active_role_list();
        $data['html_form'] = view('users.add_user_modal',$form_data);
        return view('users.users_list',$data);
    }
    // ADD-UPDATE USER DETAILS
    function add_update_user(Request $request)
    {
        Log::info(49);
        $rules['first_name'] = 'required|string|max:20|regex:/^[a-zA-Z]*$/';
        $rules['last_name'] = 'required|string|max:20|regex:/^[a-zA-Z]*$/';
        if(isset($request->id) && !empty($request->id))
            $rules['email'] = 'required|email|unique:users,email,'.$request->id;
        else
            $rules['email'] = 'required|email|unique:users';
        $rules['role_id'] = 'required|min:1';
        if(!isset($request->id) || empty($request->id))
            $rules['password'] = 'required|min:6';
        elseif(isset($request->password) && !empty($request->password))
            $rules['password'] = 'required|min:6';
        if($request->hasFile('image'))
            $rules['image'] = 'required|mimes:jpeg,jpg,png,gif';

        $msg['first_name.required'] = trans('msg.first_name_req');
        $msg['last_name.required'] = trans('msg.last_name_req');
        $msg['email.required'] = trans('msg.email_req');
        $msg['email.unique'] = trans('msg.email_unique');
        $msg['role_id.required'] = trans('msg.role_id_req');
        $this->validate($request,$rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $userModel = new User();
        $response = $userModel->add_update_user($request);
        if(!isset($request->id) || empty($request->id))
        {
            $notifiModel = new Notification();
            $notifiObj = (object)[];
            $notifiObj->sender_id = $auth_user->id;
            $notifiObj->receiver_id = $response->id;
            $notifiObj->title = trans('notification.user_get_employee_add_title');
            $notifiObj->msg = trans('notification.user_get_employee_add_msg');
            $notifiObj->url = '/dashboard';
            $notifiModel->create_notification($notifiObj);
        }
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_employee'):trans('msg.succ_added_employee');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // SEARCH USER DETAILS
    function search_users(Request $request)
    {
        Log::info(53);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $userModel = new User();
        $data['data'] = $userModel->search_users($request);
        return view('users.users_table',$data);
    }
    // GET SINGLE USER DETAIL FOR EDIT USER DETAILS
    function single_user_detail(Request $request)
    {
        Log::info(55);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $userModel = new User();
        $response = $userModel->get_single_user($request->user_id);
        $roleModel = new Role();
        $form_data['role_data'] = $roleModel->get_active_role_list();
        $form_data['data'] = $response;
        return view('users.add_user_modal',$form_data);
    }
    // DELETE SINGLE USER
    function delete_single_user(Request $request)
    {
        Log::info(56);
        $checkProposalData = Proposal::where('service_provider','=',$request->user_id)->first();
        if(!empty($checkProposalData))
            return response()->json(['status' => 400,'msg' => trans('msg.user_exist_in_proposal')],400);

        $checkProposalData = Proposal::where('created_by','=',$request->user_id)->first();
        if(!empty($checkProposalData))
            return response()->json(['status' => 400,'msg' => trans('msg.user_exist_in_proposal')],400);


        $useModel = new User();
        $response = $useModel->delete_single_user($request->user_id);
        return response()->json(['status' => 200,'response' => $response,'msg' => trans('msg.succ_delete_user_details')],200);
    }
    // EXPORT USER DETAILS
    function user_export_data(Request $request)
    {
        Log::info(58);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $file_name = 'users_'.date('d_m_Y_h_i_s').'.xlsx';
        return Excel::download(new UserExport($request), $file_name);
    }
    // UPDATE USER STATUS
    function update_user_status(Request $request)
    {
        Log::info(59);
        $userModel = new User();
        $response = $userModel->update_user_status($request);
        return response()->json(['status' => 200],200);
    }
}
