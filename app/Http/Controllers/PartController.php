<?php

namespace App\Http\Controllers;

use App\Exports\PartExport;
use App\Imports\PartsImport;
use App\Models\Part;
use App\Models\Proposal;
use App\Models\ProposalItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PHPUnit\Exception;
use Validator;
use Excel;
class PartController extends Controller
{
    // LOAD PART VIEW
    function part_view()
    {
        Log::info(15);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.parts');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        if($authUser->role_id == 1)
            $data['parts_count'] = Part::where('status','=',1)->count();
        else
            $data['parts_count'] = Part::where('status','=',1)->where('created_by','=',$authUser->id)->count();
        return view('parts.part_list',$data);
    }
    // LOAD ADD PART VIEW
    function add_part_view(Request $request)
    {
        Log::info(18);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;

        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        if(!isset($request->proposal_id) || empty($request->proposal_id))
        {
            $data['page_title'] = trans('page_title.add_parts');
            $data['proposal_id'] = 0;
            if(!role('Parts','All','create_permission'))
                return redirect(url('/parts'));
        }
        else{
            $data['page_title'] = trans('page_title.add_new_item');
            $data['proposal_id'] = $request->proposal_id;
        }
        return view('parts.part_add',$data);
    }
    // ADD-UPDATE PARTS DETAILS
    function add_update_parts(Request $request)
    {
        Log::info(16);
        if(isset($request->id) && !empty($request->id))
            $rules['part_number'] = 'required|max:255|unique:parts,part_number,'.$request->id;
        else
            $rules['part_number'] = 'required|max:255|unique:parts';
        $rules['name'] = 'required|max:255';
        $rules['atl_pin'] = 'max:255';
        $rules['atl_2_pin'] = 'max:255';
        $rules['ui'] = 'max:255';
        $rules['nsn'] = 'max:255';
        $rules['nsn_2'] = 'max:255';
        $rules['oem'] = 'max:255';
        $rules['cage_code'] = 'max:255';
        $rules['manufacturer'] = 'max:255';
        $rules['niin'] = 'max:255';
        $rules['codification_country'] = 'max:255';
        $msg['part_number.required'] = trans('msg.part_number_req');
        $msg['part_number.unique'] = trans('msg.part_number_exist');
        $msg['name.required'] = trans('msg.part_name_req');
        $request->validate($rules,$msg);
        $request->flag = 0;
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $partModel = new Part();
        $response = $partModel->add_update_part_details($request);
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_part_details'):trans('msg.succ_add_part_details');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // LOAD EDIT PART VIEW
    function edit_part_view(Request $request)
    {
        Log::info(19);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.edit_parts');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        $partModel = new Part();
        $data['data'] = $partModel->get_single_part_details($request->part_id);
        return view('parts.part_add',$data);
    }
    // SEARCH PARTS DETAILS
    function search_parts(Request $request)
    {
        Log::info(22);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $partModel = new Part();
        $data['data'] = $partModel->search_parts($request);
        return view('parts.part_table',$data);
    }
    // DELETE PART DETAILS
    function delete_single_part(Request $request)
    {
        Log::info(25);
        $checkPartData = ProposalItem::where('part_id','=',$request->part_id)->first();
        if(!empty($checkPartData))
            return response()->json(['status' => 400,'msg' => trans('msg.part_assign_to_proposal')],400);
        $partModel = new Part();
        $response = $partModel->delete_single_part($request->part_id);
        return response()->json(['status' => 200,'response' => $response,'msg' => trans('msg.succ_delete_part_details')],200);
    }
    // IMPORT PART DATA
    function import_part_data(Request $request)
    {
        Log::info(23);
        $rules['part_file'] = 'required|mimes:xlsx';
        $msg = [];
        $request->validate($rules,$msg);
        try {
            $response =  Excel::import(new PartsImport, $request->file('part_file'));
            return back()->with('success', 'Import Successfully');
        }
        catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            \Session::flash('failures', $failures);
            return back();
        }
        catch (Exception $e) {
            $failures = $e->failures();
            \Session::flash('failures', $failures);
            return back();
        }
    }
    // IMPORT PART DATA
    function export_part_data(Request $request)
    {
        Log::info(26);
        $file_name = 'parts_'.date('d_m_Y_h_i_s').'.xlsx';
        return Excel::download(new PartExport($request), $file_name);
    }
    function get_part_list(Request $request)
    {
        Log::info(27);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $request->role_id = $auth_user->role_id;
        $partModel = new Part();
        $data = $partModel->get_part_list($request);
        return response()->json($data);
    }
}
