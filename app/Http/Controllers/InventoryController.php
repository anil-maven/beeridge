<?php

namespace App\Http\Controllers;

use App\Models\PartInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InventoryController extends Controller
{
    // LOAD PART INVENTORY VIEW
    function inventory_view()
    {
        Log::info(30);
        $authUser = auth()->user();
        $data['auth_user'] = $authUser;
        $data['page_title'] = trans('page_title.parts');
        $moduleData = check_module();
        if($moduleData['status'] == 400)
            return redirect($moduleData['url']);
        $data['module_data'] = $moduleData['module_data'];
        return view('inventory.inventory_list',$data);
    }
    // ADD PART INVENTORY DATA
    function add_part_inventory(Request $request)
    {
        Log::info(31);
        $rules['part_id'] = 'required|min:1';
        $rules['credit_qty'] = 'required|min:1';
        $msg['part_id.required'] = trans('msg.part_id_req');
        $msg['credit_qty.unique'] = trans('msg.credit_qty_req');
        $request->validate($rules,$msg);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $inventoryModel = new PartInventory();
        $response = $inventoryModel->add_part_inventory($request);
        $resp_msg = isset($request->id) && !empty($request->id)?trans('msg.succ_update_part_inventory'):trans('msg.succ_add_part_inventory');
        return response()->json(['status' => 200, 'msg' => $resp_msg], 200);
    }
    // SEARCH INVENTORY DATA
    function search_inventory(Request $request)
    {
        Log::info(33);
        $auth_user = auth()->user();
        $request->created_by = $auth_user->id;
        $inventoryModel = new PartInventory();
        $data['data'] = $inventoryModel->search_part_inventory($request);
        return view('inventory.inventory_table',$data);
    }
    // DELETE SINGLE INVENTORY
    function delete_single_inventory(Request $request)
    {
        Log::info(35);
        $inventoryModel = new PartInventory();
        $response = $inventoryModel->delete_single_inventory($request->inventory_id);
        return response()->json(['status' => 200,'response' => $response,'msg' => trans('msg.succ_delete_invantory_details')],200);

    }
}
