<?php

namespace App\Exports;

use App\Models\Part;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PartExport implements FromCollection,WithHeadings,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function collection()
    {
        $partModel = new Part();
        return $partModel->export_part_data($this->request);
    }
    public function headings(): array
    {
        return ['Serial Number','Part Number','Name','ATL PN','ATL 2 PN','Description','UI','NSN','NSN 2','OEM','Basic Detail','Cage Code','Manufacturer','NIIN','Codification Country','Created At'];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);
            },
        ];
    }
}
