<?php

namespace App\Exports;

use App\Models\ProposalItem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ClientQuotationExport implements  FromCollection,WithHeadings,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function collection()
    {
        $data = (new ProposalItem())->export_client_proposal($this->request);
        foreach($data AS $key => $value)
        {
            $data[$key]->total_price = convert($value->total_price).' '.$value->ui;
            $data[$key]->total_client_price = convert($value->total_client_price).' '.$value->ui;
            unset($data[$key]->ui);
        }
     
        return $data;
    }
    public function headings(): array
    {
        return ['Item Details','Serial No.','Quantity','Unit Price','Total Amount'];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);
            },
        ];
    }
}
