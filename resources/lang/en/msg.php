<?php

return [
    // PROFILE DETAILS
    'email_req' => 'Email is required.',
    'email_invalid' => 'Invalid email.',
    'password_req' => 'Password is required.',
    'credential_wrong' => 'Wrong email or password, Please use correct details.',
    'first_name_req' => 'First name is required.',
    'first_name_invalid' => 'Invalid first name.',
    'first_name_limit' => 'Allow only 20 characters.',
    'last_name_req' => 'Last name is required.',
    'last_name_invalid' => 'Invalid last name.',
    'last_name_limit' => 'Allow only 20 characters.',
    'succ_update_profile' => 'Profile updated successfully.',
    'email_not_register' => "We can't find a user with that e-mail address.",
    // CHANGE PASSWORD
    'old_password_req' => 'Old password is required.',
    'new_password_req' => 'New password is required.',
    'new_min_password' => 'New password must be at least 6 characters long.',
    'password_confirm_req' => 'Confirm password is required.',
    'same_confirm_password' => 'Please enter the same new password as above.',
    'wrong_old_password' => 'Old password does not match in our database.',
    'new_password_different' => 'The new password and old password must be different.',
    'succ_update_password' => 'Password updated successfully.',
    // ROLE AND PERMISSION
    'role_name_req' => 'Role name is required.',
    'role_name_invalid' => 'Invalid Role name.',
    'role_name_limit' => 'Allow only 20 characters.',
    'organization_id_req' => 'Organization is required.',
    'reporting_to_req' => 'Reporting to is required.',
    'succ_add_role_details' => 'Role details added successfully.',
    'succ_update_role_details' => 'Role details updated successfully.',
    'succ_delete_role_details' => 'Role details deleted successfully.',
    'employee_exist_under_role' => 'Please first delete all employees assigned in role.',
    // NOTIFICATION
    'want_delete_notification' => 'You want to delete notification!',
    'succ_delete_notification' => 'Notification deleted successfully.',
    // PARTS
    'part_number_req' => 'Part Number is required.',
    'part_number_exist' => 'Part Number already exist.',
    'part_name_req' => 'Name is required.',
    'succ_update_part_details' => 'Part details updated successfully.',
    'succ_add_part_details' => 'Part details added successfully.',
    'succ_delete_part_details' => 'Part Detail deleted successfully.',
    'want_delete_part' => 'You want to delete part details!',
    // PART INVENTORY
    'part_id_req' => 'Please select part.',
    'credit_qty_req' => 'Quantity is required.',
    'succ_update_part_inventory' => 'Inventory updated successfully.',
    'succ_add_part_inventory' => 'Inventory added successfully.',
    'want_delete_inventory' => 'You want to delete inventory!',
    'succ_delete_invantory_details' => 'Inventory deleted successfully.',
    // USERS
    'role_id_req' => 'User role is required.',
    'email_unique' => 'Email already exist.',
    'succ_update_employee' => 'User updated successfully.',
    'succ_added_employee' => 'User added successfully.',
    'succ_delete_user_details' => 'User deleted successfully.',
    'password_min' => 'Password must be at least 6 characters long.',
    'want_delete_employee' => 'You want to delete user!',
    // PROPOSAL
    'proposal_name_req' => 'Task name is required.',
    'succ_update_proposal' => 'Task updated successfully.',
    'succ_added_proposal' => 'Task added successfully.',
    'want_delete_proposal' => 'You want to delete task!',
    'succ_delete_proposal' => 'Task deleted successfully.',
    'succ_rfq_sent' => 'RFQ sent successfully.',
    'user_exist_in_proposal' => 'User exist in task.',
    // PROPOSAL ITEM
    'proposal_item_qty_req' => 'Quantity is required.',
    'proposal_item_description_req' => 'Description is required.',
    'succ_update_proposal_item' => 'Item updated successfully.',
    'succ_added_proposal_item' => 'Item added successfully.',
    'want_delete_proposal_item' => 'You want to delete item!',
    'succ_delete_proposal_item' => 'Item deleted successfully.',
    'part_assign_to_proposal' => 'Part exist in task.',
    'succ_update_client_price' => 'Price updated successfully.',
    // CONTACT PERSON
    'supplier_name_req' => 'Supplier is required.',
    'supplier_name_limit' => 'Allow only 250 characters.',
    'contact_person_name_req' => 'Name is required.',
    'contact_person_name_limit' => 'Allow only 250 characters.',
    'contact_person_phone_number_req' => 'Phone number is required.',
    'contact_person_department_req' => 'Department is required.',
    'contact_person_department_limit' => 'Allow only 250 characters.',
    'succ_update_contact_person' => 'Supplier updated successfully.',
    'succ_added_contact_person' => 'Supplier added successfully.',
    'want_delete_contact_person' => 'You want to delete Supplier!',
    'succ_delete_contact_person' => 'Supplier deleted successfully.',
    // QUOTATION DETAILS
    'succ_update_quotation' => 'Price details updated successfully.',
    'succ_added_quotation' => 'Price details added successfully.',
    'supplier_company_req' => 'Supplier company is required.',
    'quoted_part_number_req' => 'Quoted part number is required.',
    'unit_price_req' => 'Unit price is required.',
    'want_delete_quotation' => 'You want to delete quotation!',
    'want_delete_quotation_price' => 'You want to delete price details!',
    'succ_delete_quotation' => 'Price details deleted successfully.',
    'succ_update_quotation_flag' => 'Price details selected successfully.',
    'contact_person_id_req' => 'Supplier is required.',
    'want_to_submit_quotation' => 'You want to submit quotation!',
    'first_select_quotation' => 'First select quotation and update client price!',
    'succ_submit_quotation_to_client' => 'Quotation submitted successfully.',
    // PRICES
    'price_unit_price_req' => 'Price is required.',
    'price_unit_price_min' => 'Price must be at least 1.',
    'succ_added_part_price' => 'Price details added successfully.',
    'succ_updated_part_price' => 'Price details updated successfully.',
    'want_delete_price' => 'You want to delete price!',
    'succ_delete_part_price' => 'Price deleted successfully.',
    'wrong_price_format' => 'Wrong price format.'

];
