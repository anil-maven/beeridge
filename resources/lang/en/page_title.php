<?php

return [
    'login_view' => 'Login | Beeridge',
    'dashboard' => 'Dashboard | Beeridge',
    'forget_password' => 'Forget Password | Beeridge',
    'parts' => 'Parts | Beeridge',
    'add_parts' => 'Add Part | Beeridge',
    'edit_parts' => 'Edit Part | Beeridge',
    'role_and_permission' => 'Role & Permission | Beeridge',
    'users' => 'Users | Beeridge',
    'add_users' => 'Add User | Beeridge',
    'notification' => 'Notification | Beeridge',
    'proposal' => 'Proposal | Beeridge',
    'single_rfq' => 'RFQ | Beeridge',
    'rfq_detail' => 'RFQ Details | Beeridge',
    'add_new_item' => 'Add Item | Beeridge',
    'contact_person' => 'Supplier | Beeridge',
    'quotation_detail' => 'Quotation | Beeridge',
    'inventory' => 'Inventory | Beeridge',
    'rfq_price_detail' => 'Price Details | Beeridge',
    'price' => 'Price Details | Beeridge',


];
