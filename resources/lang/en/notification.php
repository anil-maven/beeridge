<?php
return [
    'user_get_employee_add_title' => 'New Registration',
    'user_get_employee_add_msg' => 'Welcome to '.env('APP_NAME'),
    'user_get_proposal_add_title' => 'Create Task',
    'user_get_proposal_add_msg' => '"__USERNAME" user create "__PROPOSAL_NAME" Task.',
    'user_get_rfq_sent_title' => 'RFQ Sent',
    'user_get_rfq_sent_msg' => '"__USERNAME" user sent "__PROPOSAL_NAME" RFQ.',
    'user_get_quotation_submitted_title' => 'Quotation Submitted',
    'user_get_quotation_submitted_msg' => '"__USERNAME" service provider submitted quotation for "__PROPOSAL_NAME" task.',
];
?>
