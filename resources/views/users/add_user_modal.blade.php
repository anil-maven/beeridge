@php $assets_url = asset('/assets') @endphp
<style>
    .form-control:disabled, .form-control[readonly] {
        background-color: #fff;
    }
</style>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">@if(isset($data->id) && !empty($data->id)) Edit @else Add @endif User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class="tabcontent_wrapper ">
            <form class="mt-4" id="addUpdateForm" action="{{ url('/users/add-update-user') }}" method="POST"  enctype="multipart/form-data">
              <div class="sty_inp_bg">
                <input type="hidden" name="id"  value="@if(isset($data->id) && !empty($data->id)){{ $data->id }}@else 0 @endif">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="img_picker_wrap">
                            <input type="file" class="image-file" accept="image/*" id="employee_image" name="image" placeholder=" ">
                            <label class="md-form-lable">User Image</label>
                            <label for="employee_image">
                                <img id="employee_image_preview" src="@if(isset($data->image) && !empty($data->image) && isset($data->full_image_url)){{ $data->full_image_url }}@else{{ asset('/default_icon/default_image.png') }}@endif" alt="">
                            </label>
                        </div>
                        <label  class="error mt-2 text-danger common-error" id="logo_error" for="logo"></label>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="first_name"
                               id="first_name"
                               value="@if(isset($data->first_name)){{ $data->first_name }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">First Name</label>
                        <label  class="error mt-1 text-danger common-error" id="first_name_error" for="first_name"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="last_name"
                               id="last_name"
                               value="@if(isset($data->last_name)){{ $data->last_name }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Last Name</label>
                        <label  class="error mt-1 text-danger common-error" id="last_name_error" for="last_name"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email"
                               class="form-control"
                               name="email"
                               id="email"
                               onfocus="this.removeAttribute('readonly');"
                               value="@if(isset($data->email)){{ $data->email }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Email</label>
                        <label  class="error mt-1 text-danger common-error" id="email_error" for="email"></label>
                    </div>
                    <div class="form-group col-md-6">

                        <select class="form-control" name="role_id" id="role_id">
                            <option value="">Select Role</option>
                            @if(isset($role_data) && count($role_data) != 0)
                                @foreach($role_data AS $key => $value)
                                    <option @if(isset($data->role_id) && $data->role_id == $value->id) selected @endif value="{{ $value->id }}">
                                        {{ $value->role_name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                      {{--  <label class="md-form-lable">User Role</label>--}}
                        <label  class="error mt-2 text-danger common-error" id="role_id_error" for="role_id_to"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="password"
                               class="form-control"
                               name="password"
                               id="password"
                               value=""
                               onfocus="this.removeAttribute('readonly');"
                               placeholder=" ">
                        <label class="md-form-lable">Password</label>
                        <label  class="error mt-1 text-danger common-error" id="password_error" for="password"></label>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="custom_checkbox">Active
                            <input type="checkbox"
                                   name="status"
                                   id="organization_status"
                                   value="1"
                                   @if(isset($data->status))
                                   @if($data->status == 1)
                                   checked
                                   @endif
                                   @else
                                   checked
                                @endif
                            >
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="form-group cta_btn">
                            <button type="submit" id="addUpdateForm_btn" class="btn search_btn">Save</button>
                            <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- modal-content -->


