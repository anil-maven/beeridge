@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @if(role('Users','All','amend_permission'))
        @php $edit_permission = 1 @endphp
    @endif
    @if(role('Users','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="icon_td">
                            <img src="{{ $value->full_image_url }}" alt="" style="width: 40px;">
                        </td>
                        <td class="created_td"> {{ $value->first_name }}</td>
                        <td class="created_td"> {{ $value->last_name }}</td>
                        <td class="created_td"> {{ $value->email }}</td>
                        <td class="created_td"> {{ $value->role_name }}</td>
                        <td class="icon_td sty_status">
                            <label class="switch">
                                <input type="checkbox"
                                       @if($value->status == 1) checked @endif
                                       id="user_checkbox_{{ $value->id }}"
                                       @if($edit_permission == 1)
                                       onclick="update_user_status('user_checkbox_{{ $value->id }}','{{ $value->id }}')"
                                       @else
                                       disabled
                                    @endif
                                >
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="created_td"> {{ $value->created_at }}</td>

                        <td class="icon_td sty_action">
                            @if($edit_permission == 1)
                                <button type="button" onclick="edit_user('{{ $value->id }}')" class="btn common_btn"><i class="fas fa-edit"></i></button>
                            @endif
                            @if($delete_permission == 1)
                                <button type="button" onclick="delete_user('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif



