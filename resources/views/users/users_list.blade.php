@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                @include('layouts.flash_msg')

                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span>Users</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item searh_w">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by user name, email and role"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                                @if(role('Users','All','export_permission'))
                                <li>
                                    <form action="{{ url('/users/export-users') }}" method="POST" target="_blank" >
                                        @csrf
                                        <input type="hidden" name="search_string" id="search_string_for_export">
                                        <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                    </form>
                                </li>
                                @endif
                                @if(role('Users','All','create_permission'))
                                    <li>
                                        <button class="btn new_jobs_btn" onclick="show_add_user_modal()">
                                            <i class="fas fa-plus"></i>
                                            <span>New User</span>
                                        </button>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->


                <!-- End Row list Box -->

                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->


            </div>

        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="add_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document" id="user_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')

    @if(isset($html_form))
        <script>
            function show_add_user_modal()
            {
                $('#user_modal_box').html(`{!! $html_form !!}`);
                $('#add_user_modal').modal('show');
               validateForm(true);
            }
        </script>
    @endif
    <script>
        function form_submit_by_ajax()
        {
            var url = $('#addUpdateForm').attr('action');
            add_update_details_with_image('addUpdateForm',url,'employee_image','image','add_user_modal','searchData');
        }
        function validateForm(password_req) {

            jQuery.validator.addMethod("valid_first_name", function (value, element) {
                if (/^[a-zA-Z]+$/i.test(value)) {
                    return true;
                } else {
                    return false;
                }
                ;
            }, "{{ trans('msg.first_name_invalid') }}");
            jQuery.validator.addMethod("valid_last_name", function (value, element) {
                if (/^[a-zA-Z]+$/i.test(value)) {
                    return true;
                } else {
                    return false;
                }
                ;
            }, "{{ trans('msg.last_name_invalid') }}");
            $("#addUpdateForm").validate({
                rules: {
                    first_name: {
                        required: true,
                        valid_first_name: true,
                        maxlength: 20,
                    },
                    last_name: {
                        required: true,
                        valid_last_name: true,
                        maxlength: 20,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    role_id: {
                        required: true,
                    },
                    password: {
                        required:password_req,
                        minlength: 6,
                    },
                },
                messages: {
                    first_name: {
                        required: "{{ trans('msg.first_name_req') }}",
                        maxlength: "{{ trans('msg.first_name_limit') }}",
                    },
                    last_name: {
                        required: "{{ trans('msg.last_name_req') }}",
                        maxlength: "{{ trans('msg.last_name_limit') }}",
                    },
                    email: {
                        required: "{{ trans('msg.email_req') }}",
                        email: "{{ trans('msg.email_invalid') }}"
                    },
                    role_id: {
                        required: "{{ trans('msg.role_id_req') }}",
                    },
                    password: {
                        required: "{{ trans('msg.password_req') }}",
                        minlength: "{{ trans('msg.password_min') }}",
                    }
                },
                errorPlacement: function (label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function (element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }, submitHandler: function (form, event) {
                    if (!this.beenSubmitted) {
                        event.preventDefault();
                        form_submit_by_ajax();
                    }
                }
            });
            $('#employee_image').on('change', function () {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#employee_image_preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            });
        }

    </script>

    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        var var_filter_status = 0;
        function searchData(page) {
            var search_string = $('#search_string').val();
            $('#search_string_for_export').val(search_string);
            $.ajax(
                {
                    url: '{{ url('/users/search-users') }}',
                    type: "POST",
                    data: {
                        search_string: search_string,
                        page: page,
                        var_filter_status:var_filter_status,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function edit_user(user_id)
        {
            $.ajax(
                {
                    url: '{{ url('/users/single-user-detail') }}/'+user_id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#user_modal_box').html(data);
                $('#add_user_modal').modal('show');
                validateForm();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function delete_user(user_id)
        {
            swal({
                title: "Are you sure ??",
                text: '{{ trans('msg.want_delete_employee') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/users/delete-single-user') }}',
                            type: "POST",
                            data: {
                                user_id: user_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
        function update_user_status(checkbox_id,user_id)
        {
            var value_check = 0;
            if($('#'+checkbox_id).prop("checked") == true){
                value_check = 1;
            }
            $.ajax(
                {
                    url: '{{ url('/users/update-user-status') }}',
                    type: "POST",
                    data: {
                        user_id: user_id,
                        status: value_check,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "json"
                }).done(function(data){
                console.log(data)
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });
        }
    </script>
@endsection
