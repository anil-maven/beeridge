@php $assets_url = asset('/assets') @endphp
<style>
    .form-control:disabled, .form-control[readonly] {
        background-color: #fff;
    }
</style>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">@if(isset($data->id) && !empty($data->id)) Edit @else Add @endif Supplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class="tabcontent_wrapper ">
            <form class="mt-4" id="addUpdateForm" action="{{ url('/contact-person/add-update-person') }}" method="POST">
              <div class="sty_inp_bg">
                <input type="hidden" name="id"  value="@if(isset($data->id) && !empty($data->id)){{ $data->id }}@else 0 @endif">
                @csrf
              <div class="form-row pt-3">
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="supplier"
                               id="supplier"
                               value="@if(isset($data->supplier)){{ $data->supplier }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Supplier</label>
                        <label  class="error mt-1 text-danger common-error" id="supplier_error" for="supplier_name"></label>
                    </div>
                  <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="name"
                               id="name"
                               value="@if(isset($data->name)){{ $data->name }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Name</label>
                        <label  class="error mt-1 text-danger common-error" id="name_error" for="name_name"></label>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="email"
                               class="form-control"
                               name="email"
                               id="email"

                               onfocus="this.removeAttribute('readonly');"
                               value="@if(isset($data->email)){{ $data->email }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Email</label>
                        <label  class="error mt-1 text-danger common-error" id="email_error" for="email"></label>
                    </div>
                  <div class="form-group col-md-6">
                      <input type="text"
                             class="form-control"
                             name="phone_number"
                             id="phone_number"
                             onInput="this.value = phoneFormat(this.value)"
                             value="@if(isset($data->phone_number)){{ $data->phone_number }}@endif"
                             placeholder=" ">
                      <label class="md-form-lable">Phone Number</label>
                      <label  class="error mt-1 text-danger common-error" id="phone_number_error" for="phone_number"></label>
                  </div>
                  <div class="form-group col-md-6">
                      <input type="text"
                             class="form-control"
                             name="department"
                             id="department"
                             value="@if(isset($data->department)){{ $data->department }}@endif"
                             placeholder=" ">
                      <label class="md-form-lable">Department</label>
                      <label  class="error mt-1 text-danger common-error" id="department_error" for="department"></label>
                  </div>

                </div>
                </div>

                    <div class="form-group">
                        <div class="form-group cta_btn">
                            <button type="submit" id="addUpdateForm_btn" class="btn search_btn">Save</button>
                            <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>

            </form>
        </div>
    </div>
</div><!-- modal-content -->


