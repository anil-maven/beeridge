@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @if(role('Supplier','All','amend_permission'))
        @php $edit_permission = 1 @endphp
    @endif
    @if(role('Supplier','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Supplier</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Department</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="created_td"> {{ $value->supplier }}</td>
                        <td class="created_td"> {{ ($value->name)?$value->name:"NA" }}</td>
                        <td class="created_td"> {{ ($value->email)?$value->email:"NA" }}</td>
                        <td class="created_td"> {{ ($value->phone_number)?$value->phone_number:"NA" }}</td>
                        <td class="created_td"> {{ ($value->department)?$value->department:"NA" }}</td>
                        <td class="created_td"> {{ $value->created_at }}</td>

                        <td class="icon_td sty_action">
                            @if($edit_permission == 1)
                                <button type="button" onclick="edit_contact_person('{{ $value->id }}')" class="btn common_btn"><i class="fas fa-edit"></i></button>
                            @endif
                            @if($delete_permission == 1)
                                <button type="button" onclick="delete_contact_person('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif



