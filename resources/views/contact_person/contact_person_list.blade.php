@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                @include('layouts.flash_msg')

                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span>Supplier</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item searh_w">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by supplier, name, email and department"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                               {{-- @if(role('Users','All','export_permission'))
                                    <li>
                                        <form action="{{ url('/users/export-users') }}" method="POST" target="_blank" >
                                            @csrf
                                            <input type="hidden" name="search_string" id="search_string_for_export">
                                            <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                        </form>
                                    </li>
                                @endif--}}
                                @if(role('Supplier','All','create_permission'))
                                    <li>
                                        <button class="btn new_jobs_btn" onclick="show_add_contact_person_modal()">
                                            <i class="fas fa-plus"></i>
                                            <span>New Supplier</span>
                                        </button>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->



                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->


            </div>

        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="add_contact_person_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document" id="contact_person_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')

    @if(isset($html_form))
        <script>
            function show_add_contact_person_modal()
            {
                $('#contact_person_modal_box').html(`{!! $html_form !!}`);
                $('#add_contact_person_modal').modal('show');
                validateForm(true);
            }
        </script>
    @endif
    <script>
        function form_submit_by_ajax()
        {
            var url = $('#addUpdateForm').attr('action');
            add_update_details('addUpdateForm',url,'add_contact_person_modal','searchData');
        }
        function phoneFormat(input) {//returns (###) ###-####
            input = input.replace(/\D/g,'').substring(0,10); //Strip everything but 1st 10 digits
            var size = input.length;
            if (size>0) {input=""+input}
            if (size>3) {input=input.slice(0,3)+"- "+input.slice(3)}
            if (size>6) {input=input.slice(0,8)+"-" +input.slice(8)}
            return input;
        }

        function validateForm() {


            $("#addUpdateForm").validate({
                rules: {
                    supplier: {
                        required: true,
                        maxlength: 250,
                    },
                    name: {
                        required: false,
                        maxlength: 250,
                    },
                    email: {
                        required: false,
                        email: true
                    },
                    phone_number: {
                        required: false,
                    },
                    department: {
                        required: false,
                        maxlength: 250,
                    },
                },
                messages: {
                    supplier: {
                        required: "{{ trans('msg.supplier_name_req') }}",
                        maxlength: "{{ trans('msg.supplier_name_limit') }}",
                    },
                    name: {
                        required: "{{ trans('msg.contact_person_name_req') }}",
                        maxlength: "{{ trans('msg.contact_person_name_limit') }}",
                    },
                    email: {
                        required: "{{ trans('msg.email_req') }}",
                        email: "{{ trans('msg.email_invalid') }}"
                    },
                    phone_number: {
                        required: "{{ trans('msg.contact_person_phone_number_req') }}",
                    },
                    department: {
                        required: "{{ trans('msg.contact_person_department_req') }}",
                        maxlength: "{{ trans('msg.contact_person_department_limit') }}",
                    },
                },
                errorPlacement: function (label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function (element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }, submitHandler: function (form, event) {
                    if (!this.beenSubmitted) {
                        event.preventDefault();
                        form_submit_by_ajax();
                    }
                }
            });

        }

    </script>

    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        var var_filter_status = 0;
        function searchData(page) {
            var search_string = $('#search_string').val();
            $('#search_string_for_export').val(search_string);
            $.ajax(
                {
                    url: '{{ url('/contact-person/search-person') }}',
                    type: "POST",
                    data: {
                        search_string: search_string,
                        page: page,
                        var_filter_status:var_filter_status,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function edit_contact_person(person_id)
        {
            $.ajax(
                {
                    url: '{{ url('/contact-person/single-contact-person') }}/'+person_id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#contact_person_modal_box').html(data);
                $('#add_contact_person_modal').modal('show');
                validateForm();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function delete_contact_person(person_id)
        {
            swal({
                title: "Are you sure ??",
                text: '{{ trans('msg.want_delete_contact_person') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/contact-person/delete-single-contact-person') }}',
                            type: "POST",
                            data: {
                                person_id: person_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }

    </script>
@endsection
