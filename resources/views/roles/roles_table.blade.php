@php $assets_url = asset('/assets') @endphp
@php $edit_permission = 1 @endphp
@php $delete_permission = 1 @endphp

@if(isset($data) && count($data) != 0)
    <div class="responsive_table">
        <table class="table timeoffpolicy_table sty_roles_table">
            <thead>
            <tr>
                <th  width="15%">
                    <label class="custom_checkbox" style="font-size: 14px">Role
                       {{-- <input type="checkbox">
                        <span class="checkmark"></span>--}}
                    </label>
                </th>
                <th width="75%">Module</th>
                <th width="10%">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($data AS $key => $value)
                @php $moduleArr = json_decode($value->modules); @endphp
                @if($value->id != 1)
                    <tr>
                        <td class="td_one">
                            <label class="custom_checkboxa">{{ $value->role_name }} @if(isset($value->organization_name) && !empty($value->organization_name)) <br><small>({{ $value->organization_name }})</small> @endif
                               {{-- <input type="checkbox">
                                <span class="checkmark"></span>--}}
                            </label>
                        </td>
                        <td class="td_two">
                            <div class="role_group">
                                <ul class="nav">
                                    @foreach($moduleArr AS $key2 => $value2)
                                        @php $key_without_space = str_replace([' ','&'],['',''],$key2) @endphp
                                        <li>
                                            <button type="button" class="btn role_btn common-role-section-class" id="role_btn_toggle_{{ $value->id.'_'.$key_without_space }}" onclick="open_close_roles_box('role_btn_toggle_{{ $value->id.'_'.$key_without_space }}','set_permission_tr_{{ $value->id.'_'.$key_without_space }}','{{ $value->id }}','{{ $key2 }}')">{{ $key2 }}
                                                <span class="plus_icon"><i class="fas fa-plus"></i></span>
                                            </button>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </td>
                        <td class="td_five">
                            @if($edit_permission == 1)
                                <a href="javascript:void(0)" onclick="edit_role_details('{{ $value->id }}')" class="common_btn">
                                    <i class="fas fa-edit"></i>
                                </a>
                            @endif
                            @if($delete_permission == 1)
                                @if($value->flag == 1)
                                    <a href="javascript:void(0)" onclick="delete_role_details('{{ $value->id }}')" class="common_btn">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr class="tr-spacer"></tr>
                    @foreach($moduleArr AS $key2 => $value2)
                        @php $key_without_space = str_replace([' ','&'],['',''],$key2) @endphp
                        <tr class="set_permission_tr mt-2 common-role-section-class-box" id="set_permission_tr_{{ $value->id.'_'.$key_without_space }}">
                        </tr>
                    @endforeach
                    <tr class="tr-spacer"></tr>
                @endif
            @endforeach

            </tbody>
        </table>
    </div>
    {{ $data->render("layouts.pagination") }}
@else
    <div>
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif
