@php $savedModuleData = "" @endphp
@if(isset($data->modules) && !empty($data->modules))
    @php $savedModuleData = json_decode($data->modules) @endphp
@endif
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">@if(isset($data->id) && !empty($data->id)) Edit @else Add @endif Role</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class="tabcontent_wrapper ">
            <form id="roleDataForm" action="{{ url('/role-and-permission/add-update-roles') }}" method="POST"  enctype="multipart/form-data">
                <input type="hidden" name="id" id="role_id" value="@if(isset($data->id) && !empty($data->id)){{ $data->id }}@else 0 @endif">
                @csrf
<!-- 
                <div class="sty_inp_bg">
                    <div class="form-row">
                        <div class="form-group col-md-12 mb-0">
                                <input type="text"
                                       class="form-control"
                                       name="proposal_name"
                                       id="proposal_name"
                                       value="@if(isset($data->proposal_name)){{ $data->proposal_name }}@endif"
                                       placeholder=" ">
                                <label class="md-form-lable">Proposal Name</label>
                                <label  class="error mt-1 text-danger common-error" id="proposal_name_error" for="proposal_name"></label>
                            </div>
                        </div>
                    </div> -->

               <div class="sty_inp_bg mt-4">  
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="form-group mt-2">
                            <input type="text"
                                   class="form-control"
                                   name="role_name"
                                   id="role_name"
                                   value="@if(!empty(old('role_name'))){{ old('role_name') }}@elseif(isset($data->role_name)){{ $data->role_name }}@endif"
                                   placeholder=" ">
                            <label for="role_name" class="md-form-lable">Role Name</label>
                            <label  class="error mt-2 text-danger common-error" id="role_name_error" for="role_name"></label>
                            </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <h6 class="h6-title2 sty_mdl_title_2">Set Module</h6>
                    </div>

                    @if(isset($module_with_sub_data) && !empty($module_with_sub_data))
                        @foreach($module_with_sub_data AS $key => $value)
                            @php $module_name  = $value->module_name @endphp
                            <div class="col-md-6 col-xs-6">
                                <h6 class="h6-title2">{{ $value->module_name }}</h6>
                            </div>
                            <div class="col-md-6 col-xs-6 text-right">
                                <label class="switch">
                                    <input type="checkbox" class="common-role-modules" onclick="check_module_status('module_name_{{ $value->module_name }}')" id="module_name_{{ $value->module_name }}" name="module_name[]" value="{{ $value->module_name }}" @if(!empty($savedModuleData))@if(!empty($savedModuleData->$module_name)) checked @endif @else checked @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            @foreach($value->child_module AS $key2 => $value2)
                                @php $subModuleArr = [] @endphp
                                @if(isset($savedModuleData->$module_name) && !empty($savedModuleData->$module_name))
                                    @php $subModuleArr = $savedModuleData->$module_name @endphp
                                @endif
                                <div class="col-md-6 col-xs-6" style="padding-left: 40px;">
                                    <p class="para-desc2">{{ $value2->module_name }}</p>
                                </div>
                                <div class="col-md-6 col-xs-6 text-right">
                                    <label class="switch">
                                        <input type="checkbox"
                                               class="common-role-modules module_name_{{ $value->module_name }}"
                                               onclick="check_sub_module_status('module_name_{{ $value2->module_name }}','module_name_{{ $value->module_name }}')"
                                               id="module_name_{{ $value2->module_name }}"
                                               name="sub_module_name[{{ $value->module_name }}][]"
                                               value="{{ $value2->module_name }}"
                                               @if(isset($data->id) && !empty($data->id))
                                               @if(in_array($value2->module_name,$subModuleArr))
                                               checked
                                               @endif
                                               @else
                                               checked
                                            @endif
                                        >
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            @endforeach
                        @endforeach
                    @endif

                    <div class="col-md-12">
                        <div class="form-group cta_btn">
                            <button type="submit" id="roleDataForm_btn" class="btn search_btn">Save</button>
                            <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- modal-content -->

