@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
    <style>
        .permission_box .custom_checkbox {
            padding-left: 30px;
            font-size: 14px;
          /*  color: #AAAAAA !important;*/
            font-weight: 500;
        }
        .permission_box .custom_checkbox .checkmark {
            top: 1px;
        }
        .permission_box .custom_checkbox input:checked ~ .checkmark {
            border: 1.5px solid #347AE2;
            background-color: #347AE2;
        }
        .permission_box .custom_checkbox .checkmark:after {
            left: 5px;
            top: 0px;
            width: 6px;
            height: 12px;
            border: solid #fff;
            border-width: 0 2px 2px 0;
        }

        button.btn.role_btn {
            border: 1px solid #EAEAEE;
            border-radius: 8px;
            background-color: transparent;
            margin-right: 10px;
            font-size: 16px;
            font-weight: 500;
            line-height: 26px;
            color: #002B1A;
            padding: 12px 10px;
            margin-bottom: 10px;
        }
        button.btn.role_btn span {
            color: #347AE2;
            margin-left: 4px;
            font-size: 14px;
            transition: all 0.5s;
        }
        button.btn.role_btn.open{
            background-color: #347AE2;
            color: #fff;
            border: 1px solid #347AE2;
        }
        button.btn.role_btn.open span.plus_icon i{
            color: #fff;
            transform: rotate(45deg);
        }
        .permission_td .para-desc2 {
            font-weight: 700;
            line-height: 26px;
            color: #263A70;

        }
        .permission_box ul li{
            margin-right: 55px;
            margin-bottom: 10px;
        }
        .set_permission_tr{
            display: none;
        }
        button.btn.role_btn {
            border: 1px solid #EAEAEE;
            border-radius: 8px;
            background-color: transparent;
            margin-right: 20px;
            font-size: 14px;
            font-weight: 400;
            line-height: 26px;
            color: #61729A;
            padding: 12px 10px;
        }
        input:checked + .slider {
            background-color: #347AE2;
        }
        .h6-title2 {
            font-size: 14px;
            color: #000;
            font-weight: 500;
        }
        ul#tabs-nava {
            list-style: none;
            margin: 0;
            display: inline-block;
        }
        .td_one label.custom_checkboxa {
            font-size: 14px;
        }

    </style>
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span> Role & Permission</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10">
                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item w-25">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by role name"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                                {{--<li style="cursor: pointer;" onclick="show_part_filter_modal()">
                                    <p class="mb-0">
                                        <i class="fas fa-filter"></i>
                                        <span> Filters</span>
                                    </p>
                                </li>
                                <li>
                                    <a target="_blank" href="{{ url('/public/my_assets/parts_file.xlsx') }}">
                                        <button class="btn new_jobs_btn"><i class="fas fa-file-download"></i> <span>Download</span></button>
                                    </a>
                                </li>--}}
                                {{--<li>

                                    <form enctype="multipart/form-data" action="{{ url('/parts/import-parts') }}" method="POST">
                                        @csrf
                                        <span class="file-wrapper">
                                         <input type="file" onchange="this.form.submit();" class="form-control" name="part_file">
                                          <span class="button"><i class="fas fa-file-import"></i> <span>Import</span></span>
                                        </span>
                                        @if($errors->has('part_file'))
                                            <label  class="error mt-2 text-danger" for="part_file">{{ $errors->first('part_file') }}</label>
                                        @endif
                                    </form>

                                </li>
                                <li>

                                    <form action="{{ url('/parts/export-parts') }}" method="POST" target="_blank" >
                                        @csrf
                                        <input type="hidden" name="search_by_serial_no" id="search_by_serial_no_for_export">
                                        <input type="hidden" name="search_by_part_number" id="search_by_part_number_for_export">
                                        <input type="hidden" name="search_by_name" id="search_by_name_for_export">
                                        <input type="hidden" name="search_by_atl_pn" id="search_by_atl_pn_for_export">
                                        <input type="hidden" name="search_by_atl_2_pn" id="search_by_atl_2_pn_for_export">
                                        <input type="hidden" name="search_by_oem" id="search_by_oem_for_export">
                                        <input type="hidden" name="search_by_manufacturer" id="search_by_manufacturer_for_export">
                                        <input type="hidden" name="search_by_codification_country" id="search_by_codification_country_for_export">
                                        <input type="hidden" name="search_string" id="search_string_for_export">
                                        <input type="hidden" name="var_filter_status" id="var_filter_status">
                                        <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                    </form>

                                </li>--}}

                                <li>
                                    <button class="btn new_jobs_btn" onclick="show_add_role_modal()">
                                        <i class="fas fa-plus"></i>
                                        <span>New Role</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row list Box -->
                <div id="search_list_box">

                </div>
                <!-- End Table Row -->
            </div>

        </div>
    </section>
    <!-- main content section end -->
    <div class="modal right fade" id="add_role_modal" tabindex="-1" role="dialog" aria-labelledby="role_modal">
        <div class="modal-dialog modal-sm" role="document" id="role_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')
    @if(isset($html_form))
        <script>
            function show_add_role_modal()
            {
                $('#role_modal_box').html(`{!! $html_form !!}`);
                $('#add_role_modal').modal('show');
                validateForm();
            }
            function check_module_status(checkbox_id)
            {
                if($('#'+checkbox_id).prop("checked") == false){
                    $('.'+checkbox_id).prop('checked',false);
                }
                else {
                    $('.'+checkbox_id).prop('checked',true);
                }
            }
            function check_sub_module_status(sub_module_checkbox_id,module_checkbox_id)
            {
                if($('#'+sub_module_checkbox_id).prop("checked") == true){
                    $('#'+module_checkbox_id).prop('checked',true);
                }
            }
        </script>
    @endif
    <script>
        function form_submit_by_ajax()
        {
            var url = $('#roleDataForm').attr('action');
            add_update_details('roleDataForm',url,'add_role_modal','searchData');
        }
        function validateForm()
        {
            $("#roleDataForm").validate({
                rules: {
                    role_name: {
                        required: true,
                        maxlength: 20,
                    },
                    organization_id: {
                        required: true,
                    },
                    reporting_to: {
                        required: true,
                    },
                },
                messages: {
                    role_name: {
                        required: "{{ trans('msg.role_name_req') }}",
                        maxlength:"{{ trans('msg.role_name_limit') }}"
                    },
                    organization_id: {
                        required: "{{ trans('msg.organization_id_req') }}",
                    },
                    reporting_to: {
                        required: "{{ trans('msg.reporting_to_req') }}",
                    },
                },
                errorPlacement: function (label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function (element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }, submitHandler: function (form,event) {
                    if (!this.beenSubmitted)
                    {
                        event.preventDefault();
                        form_submit_by_ajax();
                        /*this.beenSubmitted = true;*/
                        /*form.submit();*/
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });

        function searchData(page) {

              var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/role-and-permission/search-roles') }}',
                    type: "POST",
                    data: {
                        search_string:search_string,
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
                /*  var myNewURL = "{{ url('/role-and-permission/search-roles') }}";
                window.history.pushState({}, document.title, myNewURL );*/
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
    </script>
    <script>
        function open_close_roles_box(module_class,module_box_class,role_id,module_name)
        {
            if(!$('#'+module_class).hasClass('open'))
            {
                $('.common-role-section-class').removeClass('open');
                $('.common-role-section-class-box').hide();
            }
            $("#"+module_box_class).fadeToggle("slow");
            $('#'+module_class).toggleClass("open");

            //$('#'+module_class).addClass("open");

            ajax_get_role_permission(module_box_class,role_id,module_name);
        }
        function ajax_get_role_permission(module_box_class,role_id,module_name)
        {
            $.ajax(
                {
                    url: '{{ url('/role-and-permission/get-role-permission') }}',
                    type: "POST",
                    data: {
                        role_id: role_id,
                        module_name: module_name,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function(data){
                $("#"+module_box_class).empty().html(data);
                // Show the first tab and hide the rest
                $('#tabs-nav li:first-child').addClass('active');
                $('.tab-content').hide();
                $('.tab-content:first').show();

                // Click function
                $('#tabs-nav li').click(function(){
                    $('#tabs-nav li').removeClass('active');
                    $(this).addClass('active');
                    $('.tab-content').hide();

                    var activeTab = $(this).find('a').attr('href');
                    $(activeTab).fadeIn();
                    return false;
                });
                $('.tab-content:first-child').show();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                console.log('No response from server');
            });
        }
        function update_role_permission(permission_column_name,checkbox_id,permission_id)
        {
            var value_check = 0;
            if($('#'+checkbox_id).prop("checked") == true){
                value_check = 1;
            }
            $.ajax(
                {
                    url: '{{ url('/role-and-permission/update-role-permission') }}',
                    type: "POST",
                    data: {
                        permission_id: permission_id,
                        column_name:permission_column_name,
                        checkbox_value: value_check,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "json"
                }).done(function(data){
                console.log(data)
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });

        }

        function edit_role_details(role_id)
        {
            $('#role_name').val('');
            $('#reporting_to').val(0);
            $('#role_id').val(0);
            $('#role_title').text('Add Role');
            $('.common-role-modules').prop('checked',false);
            $.ajax(
                {
                    url: '{{ url('/role-and-permission/single-role-detail') }}/'+role_id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#role_modal_box').html(data);
                $('#add_role_modal').modal('show');
                validateForm();

            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function delete_role_details(role_id)
        {
            swal({
                title: "Are you sure ??",
                text: 'You want to delete role!',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/role-and-permission/delete-single-role') }}',
                            type: "POST",
                            data: {
                                role_id: role_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        swal(data.msg,'','success');
                        searchData(1);

                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
    </script>
    <script>
        function get_organization_role()
        {
            var organization_id = $('#organization_id :selected').val();
            $.ajax(
                {
                    url: '{{ url('/role-and-permission/get-organization-roles') }}/'+organization_id,
                    type: "GET",
                    datatype: "json"
                }).done(function(data) {
                $('#reporting_to').empty();
                $('#reporting_to').append(`<option value="">Reporting to*</option>`);
                data.response.forEach(function (item,value)
                {
                    $('#reporting_to').append(`<option value="`+item.id+`">`+item.role_name+`</option>`);
                });
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
    </script>

    <style type="text/css">
        /* Tabs */

        /* ul#tabs-nav {
             list-style: none;
             display: flex;
             justify-content: space-between;
             display: flex;
             justify-content: space-between;
             margin: 0;
             flex-wrap: wrap;
         }*/
        ul#tabs-nav li {
            display: inline-block;
            margin: 0 11px;
        }
        ul#tabs-nav {
            list-style: none;
            margin: 0;
            display: inline-block;
        }
        ul#tabs-nav li {
            cursor: pointer;
        }
        ul#tabs-nav li:hover a,
        ul#tabs-nav li.active a {
            color: #085D29 !important;
            position: relative;;

        }
        ul#tabs-nav li.active {
            position: relative;
        }
        #tabs-nav li a {
            text-decoration: none;
            color: #a0a0a0 !important;
            font-weight: 500;
        }
        li.active:before {
            content: '';
            border: 1px solid #3abc6d;
            position: absolute;
            z-index: 9999;
            width: 50%;
            bottom: -10px;
            left: 0;
            right: 0;
            margin: 0 auto;
        }
        .row_navhead {
            border-bottom: 2px solid #ddd;
            margin-bottom: 25px;
        }
    </style>
@endsection
