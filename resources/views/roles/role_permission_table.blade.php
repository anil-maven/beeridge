<td colspan="3" class="permission_td text-left">
    <!-- Tbas start HRM -->
    <div class="row align-itmes-center row_navhead">
        <div class="col-xs-4 col-md-4 col-lg-4">
            <p class="para-desc2 mb-0">Set Permission @if(isset($data[0]->module_name)) ({{ $data[0]->module_name }}) @endif</p>
        </div>
        <div class="col-xs-8 col-md-8 col-lg-8 text-lg-right">
            @if(count($data) != 0)
                <ul id="tabs-nava">
                    @foreach($data AS $key => $value)
                      {{--  <li><a href="#tab{{ $key.'_'.$value->id }}">{{ $value->sub_module_name }}</a></li>--}}
                       {{-- <li><a href="javascript:void(0)">{{ $value->sub_module_name }}</a></li>--}}
                    @endforeach
                </ul> <!-- END tabs-nav -->
            @endif
        </div>
    </div>

    <div class="permission_box">
        <div id="tabs-content">
            @if(count($data) != 0)
                @foreach($data AS $key => $value)
                    <div id="tab{{ $key.'_'.$value->id }}" class="tab-content">
                        <ul class="nav">
                            <li>
                                <label class="custom_checkbox">Select
                                    <input  type="checkbox"
                                            id="select_permission_{{ $value->id }}"
                                            onclick="update_role_permission('select_permission','select_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->select_permission) && $value->select_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Read
                                    <input  type="checkbox"
                                            id="read_permission_{{ $value->id }}"
                                            onclick="update_role_permission('read_permission','read_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->read_permission) && $value->read_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Write
                                    <input  type="checkbox"
                                            id="write_permission_{{ $value->id }}"
                                            onclick="update_role_permission('write_permission','write_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->write_permission) && $value->write_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Create
                                    <input  type="checkbox"
                                            id="create_permission_{{ $value->id }}"
                                            onclick="update_role_permission('create_permission','create_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->create_permission) && $value->create_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Delete
                                    <input  type="checkbox"
                                            id="delete_permission_{{ $value->id }}"
                                            onclick="update_role_permission('delete_permission','delete_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->delete_permission) && $value->delete_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Submit
                                    <input  type="checkbox"
                                            id="submit_permission_{{ $value->id }}"
                                            onclick="update_role_permission('submit_permission','submit_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->submit_permission) && $value->submit_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Cancel
                                    <input  type="checkbox"
                                            id="cancel_permission_{{ $value->id }}"
                                            onclick="update_role_permission('cancel_permission','cancel_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->cancel_permission) && $value->cancel_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Amend
                                    <input  type="checkbox"
                                            id="amend_permission_{{ $value->id }}"
                                            onclick="update_role_permission('amend_permission','amend_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->amend_permission) && $value->amend_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Print
                                    <input  type="checkbox"
                                            id="print_permission_{{ $value->id }}"
                                            onclick="update_role_permission('print_permission','print_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->print_permission) && $value->print_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Email
                                    <input  type="checkbox"
                                            id="email_permission_{{ $value->id }}"
                                            onclick="update_role_permission('email_permission','email_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->email_permission) && $value->email_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Report
                                    <input  type="checkbox"
                                            id="report_permission_{{ $value->id }}"
                                            onclick="update_role_permission('report_permission','report_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->report_permission) && $value->report_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Import
                                    <input  type="checkbox"
                                            id="import_permission_{{ $value->id }}"
                                            onclick="update_role_permission('import_permission','import_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->import_permission) && $value->import_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Export
                                    <input  type="checkbox"
                                            id="export_permission_{{ $value->id }}"
                                            onclick="update_role_permission('export_permission','export_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->export_permission) && $value->export_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Share
                                    <input  type="checkbox"
                                            id="share_permission_{{ $value->id }}"
                                            onclick="update_role_permission('share_permission','share_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->share_permission) && $value->share_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="custom_checkbox">Set User Permissions
                                    <input  type="checkbox"
                                            id="set_user_permission_{{ $value->id }}"
                                            onclick="update_role_permission('set_user_permission','set_user_permission_{{ $value->id }}','{{ $value->id }}')"
                                            @if(isset($value->set_user_permission) && $value->set_user_permission == 1)
                                            checked
                                        @endif
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                @endforeach
            @endif
        </div> <!-- END tabs-content -->
    </div>
    <!-- END tabs  HRM-->
</td>
