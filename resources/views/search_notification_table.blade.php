@if(isset($data) && count($data) != 0)
    @php $delete_permission = 0 @endphp
    @if(role('Inventory','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    <div class="col-md-12 pl-0">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Sender</th>
                    <th>Title</th>
                    <th>Message</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="icon_td">
                            @if(isset($value->sender_name) && !empty($value->sender_name))
                               {{ $value->sender_name }}
                                @if(isset($value->role_name) && !empty($value->role_name))
                                   <br> ({{ $value->role_name }})
                                @endif
                            @endif

                        </td>
                        <td class="icon_td">
                            {{ $value->title }}
                        </td>
                        <td class="icon_td"> {{ $value->msg }}</td>
                        <td class="created_td"> {{ $value->created_at }}</td>

                        <td class="icon_td sty_action">
                            @if(isset($value->url) && !empty($value->url))
                                <button type="button" onclick="window.location.href='{{ url($value->url) }}'" class="btn common_btn"><i class="far fa-eye"></i></button>
                            @endif
                            <button type="button" onclick="delete_notification('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div>
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif




