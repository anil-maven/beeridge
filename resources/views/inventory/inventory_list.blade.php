@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span>Inventory</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item searh_w">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by serial number, part number and name"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                              {{--  <li style="cursor: pointer;" onclick="show_part_filter_modal()">
                                    <p class="mb-0">
                                        <i class="fas fa-filter"></i>
                                        <span> Filters</span>
                                    </p>
                                </li>
                                <li>
                                    <a target="_blank" href="{{ url('/public/my_assets/parts_file.xlsx') }}">
                                        <button class="btn new_jobs_btn"><i class="fas fa-file-download"></i> <span>Download</span></button>
                                    </a>
                                </li>
                                <li>

                                    <form enctype="multipart/form-data" action="{{ url('/parts/import-parts') }}" method="POST">
                                        @csrf
                                        <span class="file-wrapper">
                                         <input type="file" onchange="this.form.submit();" class="form-control" name="part_file">
                                          <span class="button"><i class="fas fa-file-import"></i> <span>Import</span></span>
                                        </span>
                                        @if($errors->has('part_file'))
                                            <label  class="error mt-2 text-danger" for="part_file">{{ $errors->first('part_file') }}</label>
                                        @endif
                                    </form>

                                </li>
                                <li>

                                    <form action="{{ url('/parts/export-parts') }}" method="POST" target="_blank" >
                                        @csrf
                                        <input type="hidden" name="search_by_serial_no" id="search_by_serial_no_for_export">
                                        <input type="hidden" name="search_by_part_number" id="search_by_part_number_for_export">
                                        <input type="hidden" name="search_by_name" id="search_by_name_for_export">
                                        <input type="hidden" name="search_by_atl_pn" id="search_by_atl_pn_for_export">
                                        <input type="hidden" name="search_by_atl_2_pn" id="search_by_atl_2_pn_for_export">
                                        <input type="hidden" name="search_by_oem" id="search_by_oem_for_export">
                                        <input type="hidden" name="search_by_manufacturer" id="search_by_manufacturer_for_export">
                                        <input type="hidden" name="search_by_codification_country" id="search_by_codification_country_for_export">
                                        <input type="hidden" name="search_string" id="search_string_for_export">
                                        <input type="hidden" name="var_filter_status" id="var_filter_status">
                                        <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                    </form>

                                </li>--}}
                                @if(role('Inventory','All','create_permission'))

                                    <li>
                                        <button class="btn new_jobs_btn" onclick="show_add_inventory_modal()">
                                            <i class="fas fa-plus"></i>
                                            <span>New Inventory</span>
                                        </button>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->

                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->
            </div>

        </div>
    </section>
    <!-- Start Edit Profile Modal -->
    <div class="modal right fade" id="addInventoryModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Inventory</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form id="partInventoryForm" action="{{ url('/inventory/add-inventory') }}" method="POST"  >
                        @csrf
                        <div class="tabcontent_wrapper">
                             <div class="sty_inp_bg mt-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mt-2">
                                            <select name="part_id" id="part_id" class="form-control">

                                            </select>
                                            {{--  <label class="md-form-lable">Search part</label>--}}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mt-2">
                                            <input type="number"
                                                   class="form-control"
                                                   id="credit_qty"
                                                   name="credit_qty"
                                                   value=""
                                                   min="1"
                                                   oninput="this.value = Math.abs(this.value)"
                                                   placeholder="Quantity">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <div class="">
                                        <div class="form-group cta_btn">
                                            <button type="submit" id="partInventoryForm_btn" class="btn search_btn">Sumbit</button>
                                            <button type="button" class="btn clear_btn"  data-dismiss="modal" aria-label="Close">Clear</button>
                                        </div>
                                    </div>

                        </div>
                    </form>

                </div>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <!-- End Edit Profile Modal -->
@endsection
@section('script')

    <style>
        .select2-container .selection {
            width: 100% !important;
        }
        .select2-container {
            width: 100% !important;
        }
        .select2-selection
        {
            height: 45px !important;
        }
    </style>
    <link href="{{ asset('/my_assets') }}/select2.min.css" rel="stylesheet" />
    <script src="{{ asset('/my_assets') }}/select2.min.js"></script>
    <script>
        $(function() {
            $("#part_id").select2({
                placeholder: "Search by part number",
                allowClear: true,
                ajax: {
                    url: '{{ url('/inventory/get-parts-list') }}',
                    dataType: 'json',
                    type: "POST",
                    data: function (term) {
                        return {
                            term: term,
                            _token: $("input[name=_token]").val()
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.part_number,
                                    id: item.id,
                                }
                            })
                        };
                    }

                }
            });
        });
        $('#part_id')
            .select2()
            .on('select2:open', () => {
                $(".select2-results:not(:has(a))").append('<a href="{{ url('/parts/add') }}" style="padding: 6px;height: 20px;display: inline-table;">Create new part</a>');
            })

        $("#partInventoryForm").validate({
            rules: {
                part_id: {
                    required: true,
                    maxlength: 250,
                },
                credit_qty: {
                    required: true,
                },

            },
            messages: {
                part_id: {
                    required: '{{ trans('msg.part_id_req') }}'
                },
                credit_qty: {
                    required: '{{ trans('msg.credit_qty_req') }}'
                },


            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }, submitHandler: function (form,event) {
                if (!this.beenSubmitted)
                {
                    event.preventDefault();
                    form_submit_by_ajax();
                }
            }
        });
        function form_submit_by_ajax()
        {
            var url = $('#partInventoryForm').attr('action');
            add_update_details('partInventoryForm',url,'addInventoryModal','searchData');
        }
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page)
        {
            var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/inventory/search-inventory') }}',
                    type: "POST",
                    data: {
                        search_string: search_string,
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function show_add_inventory_modal()
        {
            $('#part_id').val('');
            $('#part_id').trigger('change');
            $('#credit_qty').val('');
            $('#addInventoryModal').modal('show');
        }
        function delete_inventory_details(inventory_id)
        {
            swal({
                title: "Are you sure ?",
                text: '{{ trans('msg.want_delete_inventory') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/inventory/delete-single-inventory') }}',
                            type: "POST",
                            data: {
                                inventory_id: inventory_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }

    </script>

@endsection
