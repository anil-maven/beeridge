@if(isset($data) && count($data) != 0)
    @php $delete_permission = 0 @endphp
    @if(role('Inventory','All','delete_permission'))
        @php $delete_permission = 1 @endphp
     @endif
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Serial Number</th>
                    <th>Part Number</th>
                    <th>Part Name</th>
                    <th>Quantity</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="icon_td">{{ $value->serial_no }}</td>
                        <td class="icon_td">{{ $value->part_number }}</td>
                        <td class="icon_td">{{ $value->name }}</td>
                        <td class="created_td"> {{ $value->credit_qty }}</td>
                        <td class="created_td"> {{ $value->created_at }}</td>

                        <td class="icon_td sty_action">
                            @if($delete_permission == 1)
                                <button type="button" onclick="delete_inventory_details('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


