
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">@if(isset($data->id) && !empty($data->id)) Edit @else Add @endif Proposal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class="tabcontent_wrapper ">
            <form class="mt-4" id="addUpdateForm" action="{{ url('/proposal/add-update-proposal') }}" method="POST" >
                <input type="hidden" name="id" value="@if(isset($data->id) && !empty($data->id)){{ $data->id }}@else 0 @endif">
                @csrf

                <div class="sty_inp_bg">
                <div class="form-row">
                    <div class="form-group col-md-12 mb-0">
                            <input type="text"
                                   class="form-control"
                                   name="proposal_name"
                                   id="proposal_name"
                                   value="@if(isset($data->proposal_name)){{ $data->proposal_name }}@endif"
                                   placeholder=" ">
                            <label class="md-form-lable">Proposal Name</label>
                            <label  class="error mt-1 text-danger common-error" id="proposal_name_error" for="proposal_name"></label>
                        </div>
                    </div>
                    </div>

                    <div class="form-group col-md-12 px-0">
                        <div class="form-group cta_btn">
                            <button type="submit" id="addUpdateForm_btn" class="btn search_btn">Save</button>
                            <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                
            </form>
        </div>
    </div>
</div><!-- modal-content -->


