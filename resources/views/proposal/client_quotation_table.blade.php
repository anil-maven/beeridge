@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @php $read_permission = 0 @endphp
    {{-- @if(role('Proposal','All','amend_permission'))
         @php $edit_permission = 1 @endphp
     @endif
     @if(role('Proposal','All','delete_permission'))
         @php $delete_permission = 1 @endphp
     @endif
     @if(role('Proposal','All','read_permission'))
         @php $read_permission = 1 @endphp
     @endif--}}
    <div class="col-md-12">
        <div class="responsive_table">
            <table class="table mb-0">
                <thead>
                <tr>
                    <th>Item Details</th>
                    <th>Serial No.</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Amount</th>
                    @if(isset($quotation_status) && !empty($quotation_status))
                        <th>New Unit Price</th>
                        <th>New Total Amount</th>
                    @elseif(isset($data[0]->new_client_price) && $data[0]->new_client_price > 0)
                        <th>New Unit Price</th>
                        <th>New Total Amount</th>
                    @endif
                    <th class="text-lg-right">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && count($data) != 0)
                    @php $total_amount = 0 @endphp
                    @foreach($data AS $key => $value)
                        @php $unit_price = 0 @endphp
                        @if(isset($value->quotation_unit_price) && !empty($value->quotation_unit_price))
                            @php $unit_price = $value->quotation_unit_price @endphp
                        @endif
                        <tr>
                            <td class="common_td">{{ $value->part_number }}</td>
                            <td class="common_td">{{ $value->serial_no }}</td>
                             <td class="common_td text-lg-right">{{ $value->qty }}</td>

                            <td class="common_td text-lg-right">
                                {{ convert($value->total_client_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif
                            </td>
                            <td class="common_td text-lg-right">
                                {{ convert($value->qty * $value->total_client_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif
                            </td>
                            @if(isset($quotation_status) && !empty($quotation_status))
                                <td class="common_td text-lg-right">
                                   <input type="number"
                                          onkeypress="if(this.value.length>10) return false;else return event.charCode >= 48 && event.charCode <= 57;"
                                          onkeyup="checkDec(this);"
                                          id="quotation_client_price_{{ $value->id }}"
                                          data-quotation-item-id="{{ $value->id }}"
                                          name="client_new_price[]"
                                          min="1"
                                          step="0.1"
                                          @if(isset($value->new_client_price) && !empty($value->new_client_price))
                                          value="{{ $value->new_client_price }}"
                                          @endif
                                          class="form-control number_validation client_new_price">
                                </td>
                                <td class="common_td text-lg-right">
                                    <span id="quotation_total_client_price_{{ $value->id }}">
                                        @if(isset($value->new_client_price) && !empty($value->new_client_price))
                                            {{ convert($value->new_client_price*$value->qty) }}
                                        @endif
                                    </span>
                                </td>
                            @elseif(isset($data[0]->new_client_price) && $data[0]->new_client_price > 0)
                                <td>{{ convert($value->new_client_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif </td>
                                <td>{{ convert($value->new_client_price*$value->qty) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif</td>
                            @endif
                            <td class="text-center" style="padding: inherit;">
                                <a type="button" href="javascript:void(0)" onclick="view_proposal_item_modal('{{ $value->id }}')" class="btn common_btn pr-1"><i class="fas fa-eye"></i></a>
                            </td>
                        </tr>
                        @php $total_amount += ($value->qty * $value->total_client_price)  @endphp
                    @endforeach
                   {{-- <tr>
                        <td class="common_td"></td>
                        <td class="common_td"></td>
                        <td class="common_td text-lg-right"></td>
                        <td class="common_td text-lg-right">Total</td>
                        <td class="common_td text-lg-right">{{ convert($total_amount) }}</td>
                        <td class="text-center" style="padding: inherit;"></td>
                    </tr>--}}
                @endif

                </tbody>
            </table>
        </div>
    </div>
    {{ $data->render("layouts.pagination") }}



@endif


