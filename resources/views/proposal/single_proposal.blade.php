@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
    <style>
        .select2-container .selection {
            width: 100% !important;
        }
        .select2-container {
            width: 100% !important;
        }
        .select2-selection
        {
            height: 45px !important;
        }
    </style>
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp
    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">

                <div class="row mb-3">
                    <div class="col-xl-4 col-lg-4">
                        <h6 class="h6-title mt-1">
                            <span><a href="{{ url('/proposal') }}">Proposal</a></span>
                            <img src="{{ $assets_url }}/img/right_arrow.svg" class="{{ $assets_url }}/img-fluid">
                            @if(isset($data->proposal_name) && !empty($data->proposal_name))
                                <a href="javascript:void(0)"> <span>{{ $data->proposal_name }}</span></a>
                            @endif
                        </h6>
                    </div>
                    <div class="col-xl-8 col-lg-8 pl-xs-0">
                        <div class="manage_jobs_right_wrapper mng_contacts sty_btngrup2" id="what_do_you_want_box" @if(empty($data->service_provider)) style="display: none;" @endif>
                            <ul class="nav justify-content-end align-items-center">
                                @if($data->client_want_status != 0)
                                    <li>
                                        <a href="#" class="btn sty_btn_2" type="button">Request Quotation</a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn sty_btn_2" type="button">Start Contract</a>
                                    </li>
                                @else
                                    <li>What Do You Want To Do?</li>
                                    <li>
                                        <a  href="javascript:void(0)" onclick="update_proposal_want_status('{{ $data->id }}','1')" class="btn sty_btn_2">Pricing</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" onclick="update_proposal_want_status('{{ $data->id }}','2')" class="btn sty_btn_2" type="button">Purchase</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" onclick="update_proposal_want_status('{{ $data->id }}','3')" class="btn sty_btn_2" type="button">Research</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" onclick="update_proposal_want_status('{{ $data->id }}','4')" class="btn sty_btn_2" type="button">other</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End  -->


                <div class="card border-0">
                    <div class="card-body">
                        <div class="row prop_details">
                            <div class="col-lg-6 ">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/proname.png" class="img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Proposal Name</h5>
                                                @if(isset($data->proposal_name) && !empty($data->proposal_name))
                                                    <p>{{ $data->proposal_name }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/date.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Date Created</h5>
                                                @if(isset($data->created_at) && !empty($data->created_at))
                                                    <p>{{ $data->created_at }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/items.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">No. Of Items</h5>
                                                <p id="no_of_item_count">{{ $item_count }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/user.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Service Provider</h5>
                                                <p id="service_provider_name_box">
                                                    @if(isset($data->service_provider_name) && !empty($data->service_provider_name))
                                                        {{ $data->service_provider_name }}
                                                    @else
                                                    Not Available
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/qustion.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Quotation</h5>
                                                <p>
                                                    @if($data->quotation_status == 0)
                                                        Not Available
                                                    @else
                                                        <a href="{{ url('/proposal/quotation/'.$data->id) }}">View Quotation</a>
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Row -->

                                <!-- <p class="stylable">Items Details</p> -->
                                <div class="form-group" id="add_proposal_item_box"  @if($item_count != 0) style="display: none;"  @endif>
                                    <a class="" href="javascript:void(0)" onclick="show_proposal_item_modal()" >
                                        <div class="add_item_box">
                                            <div>
                                               <!--  <i class="fa fa-plus fa-lg fa-fw" aria-hidden="true"></i> -->
                                                 <img class="w-100" src="{{ $assets_url }}/img/plus_icon.svg" class="{{ $assets_url }}/img-fluid">
                                                <p> Add Items</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="row sty_items_table" id="search_list_box">

                                </div>

                            </div>
                            <div class="col-lg-3">

                                <div class="media mb-4">
                                    <img class="mr-3" src="{{ $assets_url }}/img/timeline.png" class="img-fluid">
                                    <div class="media-body">
                                        <h5 class="mt-0">Proposal Timeline</h5>
                                    </div>
                                </div>

                                <div class="sty_timelins" id="proposal_activity_box">
                                    @if(isset($proposal_activity_html) && !empty($proposal_activity_html))
                                        {!! $proposal_activity_html !!}
                                        @endif
                                </div>

                            </div>
                            <!-- Chatbox -->
                                <div class="col-lg-3">
                                    <p>  <img class="mr-1" src="{{ $assets_url }}/img/proname.png" class="img-fluid">Message to Service Provider</p>
                                    <div class="wrapper  chatbox">
                                        <div class="main">
                                            <div class="px-2 scroll">
                                              <div class="text-center chat_icon">
                                                  <img src="{{ $assets_url }}/img/chectimg.png" class="img-fluid">
                                              </div>
                                                <div> 
                                                    <div class="chet_cmt">
                                                        <p>No Service Provider Assigned Yet</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <nav class="navbar navbar-expand-sm d-flex justify-content-between"> 
                                                <a href="#"><img src="{{ $assets_url }}/img/clip1.svg" class="img-fluid"></a>
                                                <input type="text number" name="text" class="form-control" placeholder="Type a message here...">
                                               <a href="#"> 
                                                <img src="{{ $assets_url }}/img/send.svg" class="img-fluid" style="width: 33px">
                                               </a>
                                                
                                               <!--  <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2"> <i class="fa fa-paperclip icon1"></i> <i class="fa fa-arrow-circle-right icon2"></i> </div> -->
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                <!--End  Chatbox -->
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </section>
    <!-- main content section end -->

    <!-- Edit Add item Modal -->
    <div class="modal right fade PropaslModal" id="ProposalModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" id="proposal_item_type">


            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="modal right fade" id="ProposalItemModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper py-5" id="show_proposal_item_box">


                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

     <div class="modal right fade" id="part_price_history_modal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Part Price History</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper py-5" id="part_price_history_box">


                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


@endsection
@section('script')

    <script>

        $('#colorselector').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption"){
                $('#PropaslModal_2').modal("show");
            }

        });

        $('#colorselector2').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption2"){
                $('#Services_Modal_3').modal("show");
            }
        });

        $('#colorselector3').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption3"){
                $('#Services_Modal_4').modal("show");
            }
        });

        $('#colorselector4').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption4"){
                $('#Services_Modal_5').modal("show");
            }
        });


        // For quantity
        $(document).ready(function(){
            $('.count').prop('disabled', true);
            $(document).on('click','.plus',function(){
                $('.count').val(parseInt($('.count').val()) + 1 );
            });
            $(document).on('click','.minus',function(){
                $('.count').val(parseInt($('.count').val()) - 1 );
                if ($('.count').val() == 0) {
                    $('.count').val(1);
                }
            });
        });
    </script>
    <link href="{{ asset('/my_assets') }}/select2.min.css" rel="stylesheet" />
    <script src="{{ asset('/my_assets') }}/select2.min.js"></script>
    <script>
       function search_part_item()
       {

           $("#part_id").select2({
               placeholder: "Search by item number",
               allowClear: true,
               ajax: {
                   url: '{{ url('/proposal/get-parts-list') }}',
                   dataType: 'json',
                   type: "POST",
                   data: function (term) {
                       return {
                           term: term,
                           _token: $("input[name=_token]").val()
                       };
                   },
                   processResults: function (data) {
                       return {
                           results: $.map(data, function (item) {
                               return {
                                   text: item.part_number,
                                   id: item.id,
                               }
                           })
                       };
                   }

               }
           }) .on('select2:open', () =>
           {
               $(".select2-results:not(:has(a))").append('<a href="{{ url('/proposal/new-item-add/'.$data->id) }}" style="padding: 6px;height: 20px;display: inline-table;">Create new item</a>');
           });

           $('#part_id').on('select2:select', function (e) {
               var data = e.params.data;
               if(data.id)
               {
                   $.ajax(
                       {
                           url: '{{ url('/proposal/single-item-detail') }}/'+data.id+'/{{ $data->id }}',
                           type: "GET",
                           datatype: "html"
                       }).done(function(data){
                       $('#show_item_box').html(data);
                       $('#show_item_box').show();
                       validateForm('ProposalModal');
                   }).fail(function(jqXHR, ajaxOptions, thrownError){
                       var errorData = JSON.parse(jqXHR.responseText);
                       error_msg(errorData.msg);
                   });
               }

           });

          /* $('#part_id')
               .select2()
               .on('select2:open', () =>
               {
                   $(".select2-results:not(:has(a))").append('<a href="{{ url('/proposal/new-item-add/'.$data->id) }}" style="padding: 6px;height: 20px;display: inline-table;">Create new item</a>');
               });*/

       }





        function validateForm(modal_name = 'ProposalModal')
        {
            $("#proposalItemForm").validate({
                rules: {
                    qty: {
                        required: true,
                        min:1,
                    },
                    description: {
                        required: true,
                    },
                },
                messages: {
                    qty: {
                        required: '{{ trans('msg.proposal_item_qty_req') }}',
                    /*    min: '{{ trans('msg.proposal_item_qty_min') }}',*/
                    },
                    description: {
                        required: '{{ trans('msg.proposal_item_description_req') }}',
                    },
                },
                errorPlacement: function (label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function (element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }, submitHandler: function (form,event) {
                    if (!this.beenSubmitted)
                    {
                        event.preventDefault();
                        form_submit_by_ajax(modal_name);
                    }
                }
            });

        }
       function validateForm2(modal_name = 'ProposalModal')
       {
           $("#editProposalItemForm").validate({
               rules: {
                   qty: {
                       required: true,
                       min:1,
                   },
                   description: {
                       required: true,
                   },
               },
               messages: {
                   qty: {
                       required: '{{ trans('msg.proposal_item_qty_req') }}',
                       /*    min: '{{ trans('msg.proposal_item_qty_min') }}',*/
                   },
                   description: {
                       required: '{{ trans('msg.proposal_item_description_req') }}',
                   },
               },
               errorPlacement: function (label, element) {
                   label.addClass('mt-2 text-danger');
                   label.insertAfter(element);
               },
               highlight: function (element, errorClass) {
                   $(element).parent().addClass('has-danger')
                   $(element).addClass('form-control-danger')
               }, submitHandler: function (form,event) {
                   if (!this.beenSubmitted)
                   {
                       event.preventDefault();
                       form_submit_by_ajax2(modal_name);
                   }
               }
           });

       }
       function form_submit_by_ajax(modal_name)
       {
           var url = $('#proposalItemForm').attr('action');
           add_update_details('proposalItemForm',url,modal_name,'searchData');
       }
        function form_submit_by_ajax2(modal_name)
        {
            var url = $('#editProposalItemForm').attr('action');
            add_update_details('editProposalItemForm',url,modal_name,'searchData');
        }

        </script>
        <script>
            $(document).ready(function () {
                $(document).on('click', '.pagination a', function (event) {
                    event.preventDefault();

                    $('li').removeClass('active');
                    $(this).parent('li').addClass('active');

                    var myurl = $(this).attr('href');
                    var page = $(this).attr('href').split('page=')[1];

                    searchData(page);
                });

            });
            function searchData(page)
            {
              //  var search_string = $('#search_string').val();
                $.ajax(
                    {
                        url: '{{ url('/proposal/search-proposal-item') }}',
                        type: "POST",
                        data: {
                           // search_string: search_string,
                           proposal_id:'{{ $data->id }}',
                            page: page,
                            _token: $("input[name=_token]").val()
                        },
                        datatype: "html"
                    }).done(function (data) {
                    $("#search_list_box").empty().html(data);
                    history.pushState("", document.title, window.location.pathname);
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    alert('No response from server');
                });
            }
            searchData(1);

            function edit_proposal_item_modal(id)
            {
                $.ajax(
                    {
                        url: '{{ url('/proposal/single-proposal-item') }}/'+id,
                        type: "GET",
                        datatype: "html"
                    }).done(function(data){
                    $('#show_proposal_item_box').html(data);
                    $('#ProposalItemModal').modal('show');
                    validateForm2('ProposalItemModal');
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    var errorData = JSON.parse(jqXHR.responseText);
                    error_msg(errorData.msg);
                });
            }
            function delete_proposal_item(id,proposal_id)
            {
                swal({
                    title: "Are you sure ??",
                    text: '{{ trans('msg.want_delete_proposal_item') }}',
                    icon: "warning",
                    buttons: true,
                }).then((willConfirm) =>
                {
                    if (willConfirm)
                    {
                        $.ajax(
                            {
                                url: '{{ url('/proposal/delete-proposal-item') }}',
                                type: "POST",
                                data: {
                                    id: id,
                                    proposal_id: proposal_id,
                                    _token: $("input[name=_token]").val()
                                },
                                datatype: "json"
                            }).done(function (data) {
                            success_msg(data.msg);
                            searchData(1);
                            $('#no_of_item_count').text(data.item_count);
                            if(data.item_count == 0)
                            {
                                $('#add_proposal_item_box').show();
                            }
                            else{
                                $('#add_proposal_item_box').hide();
                            }
                        }).fail(function (jqXHR, ajaxOptions, thrownError) {
                            var errorData = JSON.parse(jqXHR.responseText);
                            error_msg(errorData.msg);
                        });
                    } else {
                        swal.close()
                    }
                });
            }
            function update_proposal_want_status(proposal_id,client_want_status)
            {
                $.ajax(
                    {
                        url: '{{ url('/proposal/update-client-want-status') }}',
                        type: "POST",
                        data: {
                            proposal_id: proposal_id,
                            client_want_status: client_want_status,
                            _token: $("input[name=_token]").val()
                        },
                        datatype: "json"
                    }).done(function (data) {
                    success_msg(data.msg);
                    setTimeout(function () {
                        location.reload(true);
                    }, 3000);
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    var errorData = JSON.parse(jqXHR.responseText);
                    error_msg(errorData.msg);
                });
            }
        </script>
    @if(isset($proposal_item_type_html))
        <script>
            function show_proposal_item_modal()
            {
                $('#proposal_item_type').html(`{!! $proposal_item_type_html !!}`)
                $('#ProposalModal').modal('show');
                search_part_item();
            }
        </script>
    @endif
    <script>
        $(document).ready(function() {
            search_part_item();
        });
    </script>
    <script>
        function show_part_price_history(part_id)
        {
            $.ajax(
                    {
                        url: '{{ route('part_price_history') }}',
                        type: "POST",
                        data: {
                            part_id: part_id,
                            _token: $("input[name=_token]").val()
                        },
                        datatype: "html"
                    }).done(function(data){
                    $('#part_price_history_box').html(data);
                    $('#part_price_history_modal').modal('show');
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    var errorData = JSON.parse(jqXHR.responseText);
                    error_msg(errorData.msg);
                });
        }
    </script>
@endsection
