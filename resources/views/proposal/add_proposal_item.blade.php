@php $assets_url = asset('/assets') @endphp
<form action="{{ url('/proposal/add-item') }}" method="POST" @if(isset($proposal_item_data->id) && !empty($proposal_item_data->id)) id="editProposalItemForm" @else id="proposalItemForm" @endif class="">
    <div class="tabcontent_wrapper">
    @csrf
        @if(isset($proposal_item_data->id) && !empty($proposal_item_data->id))
    <input type="hidden" name="id" value="{{ $proposal_item_data->id }}">
        @endif
    <input type="hidden" name="part_id" value="{{ $part_id }}">
    <input type="hidden" name="proposal_id" value="{{ $proposal_id }}">
    <div class="form-group ">
        <input type="number"
               class="form-control"
               placeholder=" "
               name="qty"
               min="1"
               oninput="this.value = Math.abs(this.value)"
               value="@if(isset($proposal_item_data->qty) && !empty($proposal_item_data->qty)){{ $proposal_item_data->qty }}@endif"
               id="qty">
        <label for="qty" class="md-form-lable">Quantity</label>
    </div>
    <div class="form-group">

        <textarea type="text"
                  rows="5"
                  class="form-control"
                  placeholder=" "
                  name="description"
                  id="description">@if(isset($proposal_item_data->description) && !empty($proposal_item_data->description)){{ $proposal_item_data->description }}@endif</textarea>
        <label for="description" class="md-form-lable">Add Description</label>
    </div>

        <div class="form-group cta_btn mt-4 mb-5">
            <button type="submit" @if(isset($proposal_item_data->id) && !empty($proposal_item_data->id)) id="editProposalItemForm" @else id="proposalItemForm_btn" @endif class="btn search_btn text">Add</button>
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn clear_btn">Cancel</button>
        </div>
    </div>
</form>

<div class="item_details">
    <h5 class="sty_title">Item Details</h5>

    <div class="row">
        <div class="col-md-4">
            <label>Part Name</label>
            <p>
                @if(isset($data->name) && !empty($data->name))
                    {{ $data->name }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>Part No.</label>
            <p>
                @if(isset($data->part_number) && !empty($data->part_number))
                    {{ $data->part_number }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>ATL PN</label>
            <p>
                @if(isset($data->atl_pn) && !empty($data->atl_pn))
                    {{ $data->atl_pn }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>ATL 2 PN</label>
            <p>
                @if(isset($data->atl_2_pn) && !empty($data->atl_2_pn))
                    {{ $data->atl_2_pn }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>ATL 2 PN</label>
            <p>
                @if(isset($data->atl_2_pn) && !empty($data->atl_2_pn))
                    {{ $data->atl_2_pn }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>UI</label>
            <p>
                @if(isset($data->ui) && !empty($data->ui))
                    {{ $data->ui }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>NSN</label>
            <p>
                @if(isset($data->nsn) && !empty($data->nsn))
                    {{ $data->nsn }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>NSN 2</label>
            <p>
                @if(isset($data->nsn_2) && !empty($data->nsn_2))
                    {{ $data->nsn_2 }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>Cage Code</label>
            <p>
                @if(isset($data->cage_code) && !empty($data->cage_code))
                    {{ $data->cage_code }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>Manufacturer</label>
            <p>
                @if(isset($data->manufacturer) && !empty($data->manufacturer))
                    {{ $data->manufacturer }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>NIIN</label>
            <p>
                @if(isset($data->niin) && !empty($data->niin))
                    {{ $data->niin }}
                @else
                    NA
                @endif
            </p>
        </div>
        <div class="col-md-4">
            <label>Codification Country</label>
            <p>
                @if(isset($data->codification_country) && !empty($data->codification_country))
                    {{ $data->codification_country }}
                @else
                    NA
                @endif
            </p>
        </div>
    </div>
    <div class="row">
        @if(isset($data->basic_details) && !empty($data->basic_details))
            <div class="col-md-6">
                <label>Basic Details</label>
                <p>{{ $data->basic_details }}</p>
            </div>
        @endif
        @if(isset($data->description) && !empty($data->description))
            <div class="col-md-6">
                <label>Description</label>
                <p>{{ $data->description }}</p>
            </div>
        @endif
    </div>
    <div class="row mt-5">
        @if(isset($data->part_image_url) && !empty($data->part_image_url))
        <div class="col-md-6">
            <label>Image</label>
            <img src="{{ $data->part_image_url }}"   height="300"   class="img-fluid">
        </div>
        @endif
        @if(isset($data->part_video_url) && !empty($data->part_video_url))
            <div class="col-md-6">
                <label class="w-100">Video</label>
                <video height="300" controls>
                    <source src="{{ $data->part_video_url }}" type="video/mp4">
                    <source src="{{ $data->part_video_url }}" type="video/ogg">
                    Your browser does not support the video tag.
                </video>
            </div>
        @endif
       </div>
    </div>
</div>
