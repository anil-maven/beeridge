@if(isset($data) && count($data) != 0)
    @foreach($data AS $key => $value)
        <div class="media">
            @if(!empty($value->icon))
            <img class="mr-3" src="{{ asset('/assets') }}/img/{{ $value->icon }}" class="img-fluid">
            @endif
            <div class="media-body draft_box">
                <p class="mb-0">{{ Date('Y-m-d', strtotime($value->created_at)) }} |  {{ Date('g:i a', strtotime($value->created_at)) }}</p>
                <h5 class="mt-0">{{ $value->title }}</h5>
                @if($value->icon == 'QuotationSaved.png')
                   <a href="{{ url('/proposal/quotation/'.$value->proposal_id) }}">View Quotation</a>
                @endif
            </div>
        </div>
    @endforeach
@endif
