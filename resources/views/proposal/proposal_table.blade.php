@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @php $read_permission = 0 @endphp
    @if(role('Proposal','All','amend_permission'))
        @php $edit_permission = 1 @endphp
    @endif
    @if(role('Proposal','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    @if(role('Proposal','All','read_permission'))
        @php $read_permission = 1 @endphp
    @endif
    <div class="col-md-12 pl-xs-0">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Proposal Name</th>
                    <th># of Items</th>
                    <th>Quotation</th>
                    <th>Created</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="icon_td">{{ $value->proposal_name }}</td>
                        <td class="icon_td">
                            @if(!empty($value->total_item))
                                {{ $value->total_item }}
                            @else
                                NA
                            @endif
                        </td>
                        <td class="icon_td">
                            @if($value->quotation_status == 0)
                            NA
                            @else
                            <a href="{{ url('/proposal/quotation/'.$value->id) }}">View Quotation</a>
                            @endif
                        </td>
                        <td class="created_td"> {{ $value->created_at }}</td>
                        <td class="icon_td sty_status">
                            {!! get_client_proposal_status($value->status)['btn'] !!}
                        </td>

                        <td class="icon_td sty_action">
                            @if($read_permission == 1)
                                <button type="button" onclick="window.location.href='{{ url('/proposal/view/'.$value->id) }}'" class="btn common_btn"><i class="far fa-eye"></i></button>
                            @endif
                            @if($value->quotation_status == 0)
                                @if($edit_permission == 1)
                                    <button type="button" onclick="edit_proposal('{{ $value->id }}')" class="btn common_btn"><i class="fas fa-edit"></i></button>
                                @endif
                                @if($delete_permission == 1)
                                    <button type="button" onclick="delete_proposal('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


