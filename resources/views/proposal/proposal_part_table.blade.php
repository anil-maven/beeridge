@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @php $read_permission = 0 @endphp
   {{-- @if(role('Proposal','All','amend_permission'))
        @php $edit_permission = 1 @endphp
    @endif
    @if(role('Proposal','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    @if(role('Proposal','All','read_permission'))
        @php $read_permission = 1 @endphp
    @endif--}}
    <div class="col-md-12 pl-xs-0">
        <div class="responsive_table">
            <table class="table mb-0">
                <thead>
                <tr>
                    <th>Item details</th>
                    <th>Quantity</th>
                    <th>Price History</th>
                    @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 0)
                    <th class="text-lg-right">
                        <a type="button" href="javascript:void(0)" onclick="show_proposal_item_modal()" class="btn common_btn text-white p-0"><i class="fas fa-plus"></i> Add</a>
                    </th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && count($data) != 0)
                    @foreach($data AS $key => $value)
                        <tr>
                            <td class="common_td">
                                {{ $value->part_number }}
                            </td>
                            <td class="common_td">{{ $value->qty }}</td>
                            <td class="sty_action">
                                <a type="button" href="javascript:void(0)" onclick="show_part_price_history('{{ $value->part_id }}')" class="btn common_btn p-0"><i class="fas fa-eye"></i></a>
                            </td>
                            @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 0)
                            <td class="sty_action">
                                <a type="button" href="javascript:void(0)" onclick="edit_proposal_item_modal('{{ $value->id }}')" class="btn common_btn p-0"><i class="fas fa-edit"></i></a>
                                <a type="button" href="javascript:void(0)" onclick="delete_proposal_item('{{ $value->id }}','{{ $value->proposal_id }}')" class="btn common_btn p-0"><i class="fas fa-trash"></i></a>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
    </div>
    {{ $data->render("layouts.pagination") }}



@endif


