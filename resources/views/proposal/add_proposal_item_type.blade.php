<div class="modal-header">
    <h4 class="modal-title">Add item</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
    <div class="tabcontent_wrapper py-4" style="padding-bottom: 0px !important;">
        <p>What Do you want to add</p>
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Items</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Service</a>
            </li>
        </ul>
        <form action="#" method="get" class="demo">

            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show proposalform" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <!--  <input type="text" class="form-control" placeholder=" "  id="cc_name">
                                 <label for="cc_name" class="md-form-lable">Propasl Name</label> -->
                                <Select id="part_id" name="part_id" class="form-control">

                                </Select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade proposalform" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                Service Not Available
                                <!--      <input type="text" class="form-control" placeholder=" "  id="cc_name">
                                     <label for="cc_name" class="md-form-lable">Search to add Services</label> -->
                                {{--  <Select id="colorselector2" class="form-control">
                                      <option value="secondoption2">1</option>
                                      <option value="secondoption2">2</option>
                                      <option value="secondoption2">3</option>
                                  </Select>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="tabcontent_wrapper py-5" id="show_item_box" style="padding-top: 0px !important;display: none;">


    </div>
</div>
