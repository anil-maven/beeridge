@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                @include('layouts.flash_msg')
                @if(Session::has('failures'))
                    <div class="alert alert-danger common-alert" role="alert">
                        <strong>Errors:</strong>

                        <ul>
                            @foreach(Session::get('failures') as $failure)
                                @foreach ($failure->errors() as $error)
                                    <li>{{ $error }} At {{ $failure->row() }} Row</li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row mb-4">
                    <div class="col-xl-2 col-lg-2 pl-xs-0">
                        <h6 class="h6-title mt-1"><span>Proposal</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10 pl-xs-0">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item w-25">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by proposal name"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                              {{--  <li style="cursor: pointer;" onclick="show_part_filter_modal()">
                                    <p class="mb-0">
                                        <i class="fas fa-filter"></i>
                                        <span> Filters</span>
                                    </p>
                                </li>--}}
                                @if(role('Proposal','All','import_permission'))
                                   {{-- <li>
                                        <a target="_blank" href="{{ url('/public/my_assets/parts_file.xlsx') }}">
                                            <button class="btn new_jobs_btn"><i class="fas fa-file-download"></i> <span>Download</span></button>
                                        </a>
                                    </li>
                                    <li>
                                        <form enctype="multipart/form-data" action="{{ url('/parts/import-parts') }}" method="POST">
                                            @csrf
                                            <span class="file-wrapper">
                                         <input type="file" onchange="this.form.submit();" class="form-control" name="part_file">
                                          <span class="button"><i class="fas fa-file-import"></i> <span>Import</span></span>
                                        </span>
                                            @if($errors->has('part_file'))
                                                <label  class="error mt-2 text-danger" for="part_file">{{ $errors->first('part_file') }}</label>
                                            @endif
                                        </form>
                                    </li>--}}
                                @endif
                                @if(role('Proposal','All','export_permission'))
                                   {{-- <li>
                                        <form action="{{ url('/parts/export-parts') }}" method="POST" target="_blank" >
                                            @csrf
                                            <input type="hidden" name="search_by_serial_no" id="search_by_serial_no_for_export">
                                            <input type="hidden" name="search_by_part_number" id="search_by_part_number_for_export">
                                            <input type="hidden" name="search_by_name" id="search_by_name_for_export">
                                            <input type="hidden" name="search_by_atl_pn" id="search_by_atl_pn_for_export">
                                            <input type="hidden" name="search_by_atl_2_pn" id="search_by_atl_2_pn_for_export">
                                            <input type="hidden" name="search_by_oem" id="search_by_oem_for_export">
                                            <input type="hidden" name="search_by_manufacturer" id="search_by_manufacturer_for_export">
                                            <input type="hidden" name="search_by_codification_country" id="search_by_codification_country_for_export">
                                            <input type="hidden" name="search_string" id="search_string_for_export">
                                            <input type="hidden" name="var_filter_status" id="var_filter_status">
                                            <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                        </form>
                                    </li>--}}
                                @endif
                                @if(role('Proposal','All','create_permission'))
                                    <li>
                                        <button class="btn new_jobs_btn" onclick="show_add_proposal_modal()">
                                            <i class="fas fa-plus"></i>
                                            <span>New Proposal</span>
                                        </button>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->

                <div class="row mb-4">
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color1">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/not.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>@if(isset($proposal_count)){{ $proposal_count }}@else 0 @endif</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color2">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/contracts.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color3">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/paid.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color4">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/recived.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row list Box -->

                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->


            </div>

        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="add_proposal_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document" id="proposal_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')
    @if(isset($html_form))
        <script>
            function show_add_proposal_modal()
            {
                $('#proposal_modal_box').html(`{!! $html_form !!}`);
                $('#add_proposal_modal').modal('show');
                validateForm();
            }
            function validateForm()
            {
                $("#addUpdateForm").validate({
                    rules: {
                        proposal_name: {
                            required: true,
                            maxlength: 250,
                        },
                    },
                    messages: {
                        proposal_name: {
                            required: '{{ trans('msg.proposal_name_req') }}',
                        },
                    },
                    errorPlacement: function (label, element) {
                        label.addClass('mt-2 text-danger');
                        label.insertAfter(element);
                    },
                    highlight: function (element, errorClass) {
                        $(element).parent().addClass('has-danger')
                        $(element).addClass('form-control-danger')
                    }, submitHandler: function (form,event) {
                        if (!this.beenSubmitted)
                        {
                            event.preventDefault();
                            form_submit_by_ajax();
                        }
                    }
                });
                function form_submit_by_ajax()
                {
                    var url = $('#addUpdateForm').attr('action');
                    add_update_details('addUpdateForm',url,'add_proposal_modal','searchData');
                }
            }
        </script>
    @endif
    <script>
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page)
        {
            var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/proposal/search-proposal') }}',
                    type: "POST",
                    data: {
                        search_string: search_string,
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function edit_proposal(proposal_id)
        {
            $.ajax(
                {
                    url: '{{ url('/proposal/single-proposal-detail') }}/'+proposal_id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#proposal_modal_box').html(data);
                $('#add_proposal_modal').modal('show');
                validateForm();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function delete_proposal(proposal_id)
        {
            swal({
                title: "Are you sure ??",
                text: '{{ trans('msg.want_delete_proposal') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/proposal/delete-single-proposal') }}',
                            type: "POST",
                            data: {
                                proposal_id: proposal_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
    </script>

@endsection
