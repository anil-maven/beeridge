<div class="modal-header">
    <h4 class="modal-title"></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
    <div class="tabcontent_wrapper">
        <h4 class="mdl_title_2 my-4">Please provide following information to add item</h4>
        <!-- form -->


        <form id="partForm" action="{{ url('/parts/add-update-part') }}" method="POST"  enctype="multipart/form-data">
            @csrf
            @if(isset($data->id) && !empty($data->id))
                <input type="hidden" name="id" value="{{ $data->id }}">
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               name="part_number"
                               id="part_number"
                               value="@if(isset($data->part_number)){{ $data->part_number }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Part No. (Required)</label>
                        <label  class="error mt-1 text-danger common-error" id="part_number_error" for="part_number"></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               name="name"
                               id="name"
                               value="@if(isset($data->name)){{ $data->name }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Name (Required)</label>
                        <label  class="error mt-1 text-danger common-error" id="name_error" for="name"></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               name="atl_pn"
                               id="atl_pn"
                               value="@if(isset($data->atl_pn)){{ $data->atl_pn }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">ATL PN</label>
                        <label  class="error mt-1 text-danger common-error" id="atl_pn_error" for="atl_pn"></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select name="ui" id="ui" class="form-control">
                            @foreach(get_part_units() AS $key => $value)
                                <option
                                    @if(isset($data->ui) && $value['unit'] == $data->ui)
                                    selected
                                    @endif
                                    value="{{ $value['unit'] }}">
                                    {{ $value['unit'] }}
                                </option>
                            @endforeach
                        </select>

                        <label class="md-form-lable">U/I (Units)</label>
                        <label  class="error mt-1 text-danger common-error" id="ui_error" for="ui"></label>
                    </div>
                </div> <div class="col-md-6">
                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               name="nsn"
                               id="nsn"
                               value="@if(isset($data->nsn)){{ $data->nsn }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">NSN</label>
                        <label  class="error mt-1 text-danger common-error" id="nsn_error" for="nsn"></label>
                    </div>
                </div> <div class="col-md-6">
                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               name="oem"
                               id="oem"
                               value="@if(isset($data->oem)){{ $data->oem }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">OEM</label>
                        <label  class="error mt-1 text-danger common-error" id="oem_error" for="oem"></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               name="manufacturer"
                               id="manufacturer"
                               value="@if(isset($data->manufacturer)){{ $data->manufacturer }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Manufacturer</label>
                        <label  class="error mt-1 text-danger common-error" id="manufacturer_error" for="manufacturer"></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="number"
                               class="form-control"
                               placeholder=" "
                               id="credit_qty"
                               name="credit_qty"
                               value=""
                               min="1"
                               oninput="this.value = Math.abs(this.value)"
                        >
                        <label for="Quantity" class="md-form-lable">Quantity</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                          <textarea class="form-control"
                                    rows="5"
                                    name="description"
                                    id="add_part_description"
                                    placeholder=" ">@if(isset($data->description)){{ $data->description }}@endif</textarea>
                        <label class="md-form-lable" for="add_part_description">Description</label>
                        {{-- <label  class="error mt-1 text-danger common-error" id="description_error" for="description"></label>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group cta_btn">
                        <button type="submit" class="btn st_n_btn AddPart" id="partForm_btn">Add Part</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- form -->
    </div>
</div>
