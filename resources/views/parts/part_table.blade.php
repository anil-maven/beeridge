@if(isset($data) && count($data) != 0)

    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @if(role('Parts','All','amend_permission'))
        @php $edit_permission = 1 @endphp
    @endif
    @if(role('Parts','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                  {{--  <th>
                        <label class="custom_checkbox">Proposal Name
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </th>--}}
                    <th>Serial No.</th>
                    <th>Part Number</th>
                    <th>Name</th>
                    <th>Created By</th>
                   {{-- <th>Quantity</th>--}}
                    <th>ATL PN</th>
                    <th>ATL 2 PN</th>
                    <th>OEM</th>
                    <th>Manufacturer</th>
                    <th>Codification Country</th>
                   {{-- <th>Status</th>--}}
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data AS $key => $value)
                        <tr>
                            <td class="icon_td">{{ $value->serial_no }}</td>
                            <td class="created_td"> {{ ($value->part_number)?$value->part_number:"NA" }}</td>
                            <td class="created_td"> {{ ($value->name)?$value->name:"NA" }}</td>
                            <td class="created_td"> {{ $value->created_by_name }}</td>

                            <td class="created_td">
                                @if(!empty($value->atl_pn))
                                    {{ $value->atl_pn }}
                                @else
                                    NA
                                @endif
                            </td>
                            <td class="created_td">
                                @if(!empty($value->atl_2_pn))
                                    {{ $value->atl_2_pn }}
                                @else
                                    NA
                                @endif
                            </td>
                            <td class="created_td">
                                @if(!empty($value->oem))
                                    {{ $value->oem }}
                                @else
                                    NA
                                @endif
                            </td>
                            <td class="created_td">
                                @if(!empty($value->manufacturer))
                                    {{ $value->manufacturer }}
                                @else
                                    NA
                                @endif
                            </td>
                            <td class="created_td">
                                @if(!empty($value->codification_country))
                                    {{ $value->codification_country }}
                                @else
                                    NA
                                @endif
                            </td>

                          {{--  <td class="icon_td sty_status">
                                <button type="button" class="btn btn_draft btn-sm">Drafted</button>
                            </td>--}}
                            <td class="icon_td sty_action">
                                @if($edit_permission == 1)
                                    <button type="button" onclick="window.location.href='{{ url('/parts/edit/'.$value->id) }}'" class="btn common_btn"><i class="fas fa-edit"></i></button>
                                @endif
                                @if($delete_permission == 1)
                                    <button type="button" onclick="delete_part_details('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                                @endif
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


