@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                @include('layouts.flash_msg')
                @if(Session::has('failures'))
                    <div class="alert alert-danger common-alert" role="alert">
                        <strong>Errors:</strong>

                        <ul>
                            @foreach(Session::get('failures') as $failure)
                                @foreach ($failure->errors() as $error)
                                    <li>{{ $error }} At {{ $failure->row() }} Row</li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span>Parts</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10 ">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item searh_w">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by serial number, part number, name, ATL PN, ATL 2 PN, OEM and Manufacture"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                                <li style="cursor: pointer;" onclick="show_part_filter_modal()">
                                    <p class="mb-0">
                                        <i class="fas fa-filter"></i>
                                        <span> Filters</span>
                                    </p>
                                </li>
                                @if(role('Parts','All','import_permission'))
                                <li>
                                    <a target="_blank" href="{{ url('/public/my_assets/parts_file.xlsx') }}">
                                        <button class="btn new_jobs_btn"><i class="fas fa-file-download"></i> <span>Download</span></button>
                                    </a>
                                </li>
                                <li>
                                    <form enctype="multipart/form-data" action="{{ url('/parts/import-parts') }}" method="POST">
                                        @csrf
                                        <span class="file-wrapper">
                                         <input type="file" onchange="this.form.submit();" class="form-control" name="part_file">
                                          <span class="button"><i class="fas fa-file-import"></i> <span>Import</span></span>
                                        </span>
                                        @if($errors->has('part_file'))
                                            <label  class="error mt-2 text-danger" for="part_file">{{ $errors->first('part_file') }}</label>
                                        @endif
                                    </form>
                                </li>
                                @endif
                                @if(role('Parts','All','export_permission'))
                                <li>
                                    <form action="{{ url('/parts/export-parts') }}" method="POST" target="_blank" >
                                        @csrf
                                        <input type="hidden" name="search_by_serial_no" id="search_by_serial_no_for_export">
                                        <input type="hidden" name="search_by_part_number" id="search_by_part_number_for_export">
                                        <input type="hidden" name="search_by_name" id="search_by_name_for_export">
                                        <input type="hidden" name="search_by_atl_pn" id="search_by_atl_pn_for_export">
                                        <input type="hidden" name="search_by_atl_2_pn" id="search_by_atl_2_pn_for_export">
                                        <input type="hidden" name="search_by_oem" id="search_by_oem_for_export">
                                        <input type="hidden" name="search_by_manufacturer" id="search_by_manufacturer_for_export">
                                        <input type="hidden" name="search_by_codification_country" id="search_by_codification_country_for_export">
                                        <input type="hidden" name="search_string" id="search_string_for_export">
                                        <input type="hidden" name="var_filter_status" id="var_filter_status">
                                        <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                    </form>
                                </li>
                                @endif
                                @if(role('Parts','All','create_permission'))
                                    <li>
                                        <button class="btn new_jobs_btn" onclick="window.location.href='{{ url('/parts/add') }}'">
                                            <i class="fas fa-plus"></i>
                                            <span>New Part</span>
                                        </button>
                                    </li>
                                 @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->

                <div class="row mb-4">
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color1">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/not.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>@if(isset($parts_count)){{ $parts_count }}@else 0 @endif</h5>
                                <p>Parts Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color2">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/contracts.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Parts Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color3">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/paid.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Parts Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color4">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/recived.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Parts Created</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row list Box -->

                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->


            </div>

        </div>
    </section>
    <!-- Start Edit Profile Modal -->
    <div class="modal right fade" id="partFilterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filter</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper">
                            <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_serial_no"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by serial no</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_part_number"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by part number</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_name"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by name</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_atl_pn"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by ATL PN</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_atl_2_pn"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by ATL 2 PN</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_oem"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by OEM</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_manufacturer"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by manufacturer</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           id="search_by_codification_country"
                                           value=""
                                           placeholder=" ">
                                    <label class="md-form-lable">Search by codification country</label>
                                </div>
                            </div>

                                <div class="col-md-12">
                                    <div class="form-group cta_btn">
                                        <button type="submit" onclick="filter_part_data()" class="btn search_btn">Sumbit</button>
                                        <button type="button" class="btn clear_btn" onclick="create_part_filter()">Clear</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <!-- End Edit Profile Modal -->
@endsection
@section('script')
    <script>
       $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        var var_filter_status = 0;
        function searchData(page) {
            var search_by_serial_no = $('#search_by_serial_no').val();
            var search_by_part_number = $('#search_by_part_number').val();
            var search_by_name = $('#search_by_name').val();
            var search_by_atl_pn = $('#search_by_atl_pn').val();
            var search_by_atl_2_pn = $('#search_by_atl_2_pn').val();
            var search_by_oem = $('#search_by_oem').val();
            var search_by_manufacturer = $('#search_by_manufacturer').val();
            var search_by_codification_country = $('#search_by_codification_country').val();
            var search_string = $('#search_string').val();
            $('#search_by_serial_no_for_export').val(search_by_serial_no);
            $('#search_by_part_number_for_export').val(search_by_part_number);
            $('#search_by_name_for_export').val(search_by_name);
            $('#search_by_atl_pn_for_export').val(search_by_atl_pn);
            $('#search_by_atl_2_pn_for_export').val(search_by_atl_2_pn);
            $('#search_by_oem_for_export').val(search_by_oem);
            $('#search_by_manufacturer_for_export').val(search_by_manufacturer);
            $('#search_by_codification_country_for_export').val(search_by_codification_country);
            $('#search_string_for_export').val(search_string);
            $('#var_filter_status').val(var_filter_status);
            $.ajax(
                {
                    url: '{{ url('/parts/search-parts') }}',
                    type: "POST",
                    data: {
                        search_by_serial_no: search_by_serial_no,
                        search_by_part_number: search_by_part_number,
                        search_by_name: search_by_name,
                        search_by_atl_pn: search_by_atl_pn,
                        search_by_atl_2_pn: search_by_atl_2_pn,
                        search_by_oem: search_by_oem,
                        search_by_manufacturer: search_by_manufacturer,
                        search_by_codification_country: search_by_codification_country,
                        search_string: search_string,
                        page: page,
                        var_filter_status:var_filter_status,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
              }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function delete_part_details(part_id)
        {
            swal({
                title: "Are you sure ?",
                text: '{{ trans('msg.want_delete_part') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/parts/delete-single-part') }}',
                            type: "POST",
                            data: {
                                part_id: part_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
        function show_part_filter_modal()
        {
            $('#partFilterModal').modal('show');
        }
        function filter_part_data()
        {
            var_filter_status = 1;
            searchData(1);
            $('#partFilterModal').modal('hide');
        }
        function create_part_filter()
        {
            $('#search_by_serial_no').val('');
            $('#search_by_part_number').val('');
            $('#search_by_name').val('');
            $('#search_by_atl_pn').val('');
            $('#search_by_atl_2_pn').val('');
            $('#search_by_oem').val('');
            $('#search_by_manufacturer').val('');
            $('#search_by_codification_country').val('');
            $('#search_string').val('');
            var_filter_status = 0;
            searchData(1);
            $('#partFilterModal').modal('hide');
        }
    </script>
@endsection
