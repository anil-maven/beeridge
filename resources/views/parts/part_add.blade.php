@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp
    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-xl-6 col-lg-3">
                        @if(isset($proposal_id) && !empty($proposal_id))
                            <h6 class="h6-title mt-1"> 
                              <span class="sty_text_blue" onclick="window.location.href='{{ route('proposal_view',[$proposal_id]) }}'" style="cursor: pointer;">Task </span> 
                              <img src="{{ $assets_url }}/img/right_arrow.svg"  class="arrow_rtl"> @if(isset($data->id) && !empty($data->id)) Update @else Create New @endif Item</h6>
                        @else
                            <h6 class="h6-title mt-1"> 
                              <span class="sty_text_blue" onclick="window.location.href='{{ url('/parts') }}'" style="cursor: pointer;">Parts </span> 
                              <img src="{{ $assets_url }}/img/right_arrow.svg"  class="arrow_rtl"> @if(isset($data->id) && !empty($data->id)) Update @else Create New @endif Part</h6>
                        @endif
                    </div>
                </div>
                <!-- End Row -->
                <form id="partForm" action="{{ url('/parts/add-update-part') }}" method="POST"  enctype="multipart/form-data">
                    @csrf
                    @if(isset($data->id) && !empty($data->id))
                        <input type="hidden" name="id" value="{{ $data->id }}">
                    @endif
                    <div class="row">
                    <div class="col-md-12">
                        <div class="aggregators_configure_wrapper">
                            <h6 class="conf-title">Please Provide Following Information</h6>

                            <div class="form-row sty_form_row">
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="part_number"
                                           id="part_number"
                                           value="@if(isset($data->part_number)){{ $data->part_number }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">Part No. (Required)</label>
                                    <label  class="error mt-1 text-danger common-error" id="part_number_error" for="part_number"></label>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="name"
                                           id="name"
                                           value="@if(isset($data->name)){{ $data->name }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">Name (Required)</label>
                                    <label  class="error mt-1 text-danger common-error" id="name_error" for="name"></label>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="atl_pn"
                                           id="atl_pn"
                                           value="@if(isset($data->atl_pn)){{ $data->atl_pn }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">ATL PN</label>
                                    <label  class="error mt-1 text-danger common-error" id="atl_pn_error" for="atl_pn"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="atl_2_pn"
                                           id="atl_2_pn"
                                           value="@if(isset($data->atl_2_pn)){{ $data->atl_2_pn }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">ATL 2 PN</label>
                                    <label  class="error mt-1 text-danger common-error" id="atl_2_pn_error" for="atl_2_pn"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <select name="ui" id="ui" class="form-control">
                                        @foreach(get_part_units() AS $key => $value)
                                        <option
                                        @if(isset($data->ui) && $value['unit'] == $data->ui)
                                            selected
                                        @endif
                                        value="{{ $value['unit'] }}">
                                            {{ $value['unit'] }}
                                        </option>
                                        @endforeach
                                    </select>
                                    <!-- <input type="text"
                                           class="form-control"
                                           name="ui"
                                           id="ui"
                                           value="@if(isset($data->ui)){{ $data->ui }}@endif"
                                           placeholder=" "> -->
                                    <label class="md-form-lable">U/I (Units)</label>
                                    <label  class="error mt-1 text-danger common-error" id="ui_error" for="ui"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="nsn"
                                           id="nsn"
                                           value="@if(isset($data->nsn)){{ $data->nsn }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">NSN</label>
                                    <label  class="error mt-1 text-danger common-error" id="nsn_error" for="nsn"></label>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="nsn_2"
                                           id="nsn_2"
                                           value="@if(isset($data->nsn_2)){{ $data->nsn_2 }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">NSN 2</label>
                                    <label  class="error mt-1 text-danger common-error" id="nsn_2_error" for="nsn_2"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="oem"
                                           id="oem"
                                           value="@if(isset($data->oem)){{ $data->oem }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">OEM</label>
                                    <label  class="error mt-1 text-danger common-error" id="oem_error" for="oem"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="cage_code"
                                           id="cage_code"
                                           value="@if(isset($data->cage_code)){{ $data->cage_code }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">Cage Code</label>
                                    <label  class="error mt-1 text-danger common-error" id="cage_code_error" for="cage_code"></label>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="manufacturer"
                                           id="manufacturer"
                                           value="@if(isset($data->manufacturer)){{ $data->manufacturer }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">Manufacturer</label>
                                    <label  class="error mt-1 text-danger common-error" id="manufacturer_error" for="manufacturer"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="niin"
                                           id="niin"
                                           value="@if(isset($data->niin)){{ $data->niin }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">NIIN</label>
                                    <label  class="error mt-1 text-danger common-error" id="niin_error" for="niin"></label>

                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text"
                                           class="form-control"
                                           name="codification_country"
                                           id="codification_country"
                                           value="@if(isset($data->codification_country)){{ $data->codification_country }}@endif"
                                           placeholder=" ">
                                    <label class="md-form-lable">Codification Country</label>
                                    <label  class="error mt-1 text-danger common-error" id="codification_country_error" for="codification_country"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control"
                                                  rows="5"
                                                  name="description"
                                                  id="description"
                                                  placeholder=" ">@if(isset($data->description)){{ $data->description }}@endif</textarea>
                                        <label class="md-form-lable">Description</label>
                                       {{-- <label  class="error mt-1 text-danger common-error" id="description_error" for="description"></label>--}}
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                     <textarea class="form-control"
                                               rows="5"
                                               name="basic_details"
                                               id="basic_details"
                                               placeholder=" ">@if(isset($data->basic_details)){{ $data->basic_details }}@endif</textarea>
                                    <label class="md-form-lable">Details</label>
                                    <label  class="error mt-1 text-danger common-error" id="basic_details_error" for="basic_details"></label>

                                </div>
                                <div class="form-group col-md-6">
                                    <div class="img_upload">
                                        <div>
                                            <img id="part_image_preview"
                                                 @if(isset($data->part_image_url) && !empty($data->part_image_url))
                                                    src="{{ $data->part_image_url }}"
                                                 @else
                                                    src="{{ asset('/default_icon/default_image.png') }}"
                                                 @endif
                                                 class="img-uploaded"
                                                 alt="your image" />
                                        </div>
                                        <div class=" text-center">
                                            <h6 class="m-0">Drag & Drop Image Here</h6>
                                            <p class="m-0 p-0">or</p>
                                            <span class="file-wrapper">
                                                  <input type="file" accept="image/*" name="part_image" id="part_image" class="uploader" />

                                                <span class="btn btn-large btn-alpha">Upload</span>
                                                </span>
                                        </div>
                                    </div>
                                    <label  class="error mt-1 text-danger common-error" id="part_image_error" for="part_image"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="img_upload">
                                        <div id="part_video_box">
                                            @if(isset($data->part_video_url) && !empty($data->part_video_url))
                                            <video width="250" height="100" controls>
                                                <source src="{{ $data->part_video_url }}" type="video/mp4">
                                                <source src="{{ $data->part_video_url }}" type="video/ogg">
                                                Your browser does not support the video tag.
                                            </video>
                                            @else
                                                <img id="" src="{{ asset('/default_icon/video.png') }}" class="img-uploaded" alt="your image" />
                                            @endif

                                        </div>
                                        <div class=" text-center">
                                            <h6 class="m-0">Drag & Drop Video Here</h6>
                                            <p class="m-0 p-0">or</p>
                                            <span class="file-wrapper">
                                                       <input type="file" accept="video/mp4,video/x-m4v,video/*" name="part_video" id="part_video" class="uploader" />
                                                       <span class="btn btn-large btn-alpha">Upload</span>
                                                     </span>
                                        </div>
                                    </div>
                                    <label  class="error mt-1 text-danger common-error" id="part_video_error" for="part_image"></label>
                                </div>
                                <div class="form-group cta_btn mb-0 ml-3">
                                    <button type="submit" id="partForm_btn" class="btn search_btn">Save</button>
                                    <button type="button" onclick="window.location.href = '{{ url('/parts') }}'" class="btn btn-danger">Cancel</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </form>
            </div>

        </div>
    </section>
    <!-- main content section end -->
@endsection
@section('script')
    <script>
        $("#partForm").validate({
            rules: {
                part_number: {
                    required: true,
                    maxlength: 250,
                },
                name: {
                    required: true,
                    maxlength: 250,
                },
                atl_pn: {
                    maxlength: 250,
                },
                atl_2_pn: {
                    maxlength: 250,
                },
                ui: {
                    maxlength: 250,
                },
                nsn: {
                    maxlength: 250,
                },
                nsn_2: {
                    maxlength: 250,
                },
                oem: {
                    maxlength: 250,
                },
                cage_code: {
                    maxlength: 250,
                },
                manufacturer: {
                    maxlength: 250,
                },
                niin: {
                    maxlength: 250,
                },
                codification_country: {
                    maxlength: 250,
                },
            },
            messages: {
                part_number: {
                    required: '{{ trans('msg.part_number_req') }}'
                },
                name: {
                    required: '{{ trans('msg.part_name_req') }}'
                },


            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }, submitHandler: function (form,event) {
                if (!this.beenSubmitted)
                {
                    event.preventDefault();
                    form_submit_by_ajax();
                }
            }
        });
        function form_submit_by_ajax()
        {
            var url = $('#partForm').attr('action');
            add_update_part('partForm',url,'');
        }
        function add_update_part(form_id,url)
        {
            $('#'+form_id+'_btn').prop('disabled', true);
            $('.common-error').hide();
            var formData = $('#' + form_id).serializeArray();
            var fd = new FormData();
            var image = document.getElementById('part_image');
            fd.append('part_image', image.files[0]);
            var video = document.getElementById('part_video');
            fd.append('part_video', video.files[0]);
            formData.forEach(function (item,key)
            {
                fd.append(item.name, item.value);
            });
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                data: fd,
                success: function (response)
                {
                    $('#'+form_id+'_btn').prop('disabled', false);
                    swal({
                        title: "Success",
                        text: response.msg,
                        type: "success"
                    }).then(function() {
                        window.location = "<?php if(isset($proposal_id) && !empty($proposal_id)){ echo route('proposal_view',$proposal_id); }else{ echo url('/parts'); } ?>";
                    });
                   /* success_msg(response.msg);*/
                },
                error:function (reject) {
                    if (reject.status === 422) {
                        var errors = $.parseJSON(reject.responseText);
                        $('.common-error').show();
                        $.each(errors.errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                    } else {
                        var responseJSON = JSON.parse(reject.responseJSON);
                        error_msg(responseJSON.msg);
                    }
                    $('#'+form_id+'_btn').prop('disabled', false);
                }
            });
        }

        part_image.onchange = function(e){
            e.preventDefault();
            var file = this.files[0];
            if(file.type.indexOf('image/') !== 0) {
                this.value = null;
                $('#part_image_error').text('Allow only image');
            }
            else {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#part_image_preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }
        }
        part_video.onchange = function(e){
            e.preventDefault();
            var file = this.files[0];
            if(file.type.indexOf('video/') !== 0) {
                this.value = null;
                $('#part_video_error').text('Allow only video');
            }
            else {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#part_video_box').empty();
                        $('#part_video_box').html(` <video width="250" height="100" controls>
                                                <source src="`+e.target.result+`" type="video/mp4">
                                                <source src="`+e.target.result+`" type="video/ogg">
                                                Your browser does not support the video tag.
                                            </video>`);

                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }
        }
    </script>
@endsection
