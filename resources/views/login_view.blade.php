@extends('layouts.front_master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('section')
    @php $assets_url = asset('/assets/') @endphp
    <section class="login_section" style="background-image: url('{{ $assets_url }}/img/login_bg.png');background-size: 100% 100%;background-repeat: no-repeat;background-position: center;position: relative;">
        <div class="d-none d-lg-block">
            <img  class="left_lng_img" src="{{ $assets_url }}/img/left_lng_img.png">
            <img  class="right_lng_img" src="{{ $assets_url }}/img/right_lng_img.png">
        </div>
        <div class="container">
            <div class="col-md-12 col-lg-9 m-auto text-center login_main_header">
                <h1>Extend Your Reach</h1>
                <p>Phosfluorescently predominate leveraged architectures and vertical catalysts for <br> ange. Energistically conceptualize standardized  onverg whereas stand-alone.</p>
            </div>

            <div class="col-md-12 col-lg-12 col-xl-5 m-auto">
                <div class="login_wrapper my-4">
                    <div>
                        <div class="py-md-4 py-lg-4 px-md-5 px-lg-5">
                            <div class="login_head">
                                <img src="{{ $assets_url }}/img/beeridge_logo.png" alt="">
                                <h5 class="h5-title">Login Into Your Account</h5>
                            </div>
                            <div class="login_body">
                                <form id="loginForm" action="{{ route('login') }}" method="post">
                                   @csrf
                                    @include('layouts.flash_msg')
                                    <div class="form-group">
                                        <input type="text"
                                               class="form-control"
                                               placeholder="Enter your email address"
                                               value="{{ old('email') }}"
                                               name="email"
                                               autocomplete="off"
                                               readonly
                                               onfocus="this.removeAttribute('readonly');"
                                               id="email">
                                       {{-- <label for="login_email" class="lg_form_lable">Enter your email address</label>--}}
                                        <img src="{{ $assets_url }}/img/Email.svg" alt="">
                                        @if($errors->has('email'))
                                            <label  class="error mt-2 text-danger" for="email">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input type="password"
                                               class="form-control"
                                               placeholder="Enter your password"
                                               name="password"
                                               readonly
                                               onfocus="this.removeAttribute('readonly');"
                                               autocomplete="new-password"
                                               id="password">
                                       {{-- <label for="login_pwd" class="lg_form_lable">Enter your password</label>--}}
                                        <img src="{{ $assets_url }}/img/lock.svg" alt="">
                                        <img onclick="show_hide_password('password')" class="eye_img" src="{{ $assets_url }}/img/eye-icon.svg" alt="">
                                        <!-- <i class="fas fa-eye-slash"></i> -->
                                        @if($errors->has('password'))
                                            <label  class="error mt-2 text-danger" for="password">{{ $errors->first('password') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group mb-1">
                                        <div class="row">
                                            <div class="col-md-7 col-sm-12">
                                                <div class="toggle_box">
                                                    <label class="switch">
                                                        <input type="checkbox"
                                                               name="remember"
                                                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <p class="span_txt">Remember me</p>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-12 text-lg-right">
                                                <a href="{{ url('/forget-password') }}" class="forgot_pwd_link">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn login_btn">Sign In</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

              {{--  <div class="text-center sinup_link">
                   Don't have an account? <a href="#">Sign Up</a>
                </div>--}}
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            var some_id = $('#email');
            some_id.prop('type', 'text');
            some_id.removeAttr('autocomplete');
        });
        function show_hide_password(id)
        {
            var x = document.getElementById(id);
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
        $("#loginForm").validate({
            rules: {
                email: {
                    required: true,
                    email:true
                },
                password: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "{{ trans('msg.email_req') }}",
                    email: "{{ trans('msg.email_invalid') }}"
                },
                password: {
                    required: "{{ trans('msg.password_req') }}",
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            },submitHandler: function (form) {
                if (!this.beenSubmitted) {
                    this.beenSubmitted = true;
                    form.submit();
                }
            }
        });

    </script>
@endsection
