@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 col-lg-9 col-md-12 pd-xs-0">
                    <div class="dashboard_graph_wrapper">
                        <div class="row">
                            <div class="col-md-5">
                                <h6 class="h6-title">Monthly Recieved Applications</h6>
                            </div>
                            <div class="col-md-7">
                                <ul class="nav justify-content-end">
                                    <li class="d-flex">
                                        <div class="form-group mb-0 select_icon">
                                            <select class="form-control">
                                                <option value="0">May</option>
                                                <option value="1">June</option>
                                                <option value="2">July</option>
                                            </select>
                                        </div>
                                        <!-- </li>
                                        <li> -->
                                        <div class="form-group mb-0 select_icon">
                                            <select class="form-control" style="border-right:0px">
                                                <option value="0">2021</option>
                                                <option value="1">2022</option>
                                                <option value="2">2023</option>
                                            </select>
                                        </div>
                                        <button type="button" class="btn show_btn">Show</button>
                                    </li>
                                    <!-- <li>
                                        <button type="button" class="btn show_btn">Show</button>
                                    </li> -->
                                    <li class="dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop2" data-toggle="dropdown">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">
                                                <img src="{{ $assets_url }}/img/maximize-icon.png" alt="">
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="{{ $assets_url }}/img/printer.png" alt="">
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="{{ $assets_url }}/img/png-icon.png" alt="">
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="{{ $assets_url }}/img/jpg-icon.png" alt="">
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="{{ $assets_url }}/img/pdf-icon.png" alt="">
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="{{ $assets_url }}/img/svg-icon.png" alt="">
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!--  <img src="{{ $assets_url }}/img/graph-img.jpg" alt=""> -->
                                <figure class="highcharts-figure">
                                    <div id="container"></div>

                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-2 col-lg-3 col-md-6">
                    <div class="dashboard_app_wrapper">
                        <div class="head_wrap">
                            <h6 class="app_title">Received Applications</h6>
                            <p class="app_desc">11 April 2021</p>
                        </div>
                        <div class="body_wrap">
                            <div class="body_inner_wrap row">
                                <div class="img_wrap">
                                    <img src="{{ $assets_url }}/img/profile-img.jpg" alt="">
                                </div>
                                <div class="content_wrap">
                                    <h6 class="app_title">Sarah Hosten</h6>
                                    <p class="app_desc">5d ago</p>
                                </div>
                                <div class="icon_wrap">
                                    <a class="icon_link" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="body_inner_wrap row">
                                <div class="img_wrap">
                                    <img src="{{ $assets_url }}/img/profile-img.jpg" alt="">
                                </div>
                                <div class="content_wrap">
                                    <h6 class="app_title">Mike Loke</h6>
                                    <p class="app_desc">6d ago</p>
                                </div>
                                <div class="icon_wrap">
                                    <a class="icon_link" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="body_inner_wrap row">
                                <div class="img_wrap">
                                    <img src="{{ $assets_url }}/img/profile-img.jpg" alt="">
                                </div>
                                <div class="content_wrap">
                                    <h6 class="app_title">Dena Thompson</h6>
                                    <p class="app_desc">5d ago</p>
                                </div>
                                <div class="icon_wrap">
                                    <a class="icon_link" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="body_inner_wrap row">
                                <div class="img_wrap">
                                    <img src="{{ $assets_url }}/img/profile-img.jpg" alt="">
                                </div>
                                <div class="content_wrap">
                                    <h6 class="app_title">Dena Thompson</h6>
                                    <p class="app_desc">5d ago</p>
                                </div>
                                <div class="icon_wrap">
                                    <a class="icon_link" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="body_inner_wrap row">
                                <div class="img_wrap">
                                    <img src="{{ $assets_url }}/img/profile-img.jpg" alt="">
                                </div>
                                <div class="content_wrap">
                                    <h6 class="app_title">Dena Thompson</h6>
                                    <p class="app_desc">5d ago</p>
                                </div>
                                <div class="icon_wrap">
                                    <a class="icon_link" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                </div>
                            </div>

                            <div class="body_inner_wrap2">
                                <a class="view_all_link" href="#">View All</a>
                            </div>
                        </div>
                        <div class="foot_wrap">
                            <div class="foot_wrap_innner dashbg_blue">
                                <img src="{{ $assets_url }}/img/dash1.png">
                                <h6>376</h6>
                                <p>Applications</p>
                            </div>
                            <div class="foot_wrap_innner dashbg_yellow">
                                <img src="{{ $assets_url }}/img/dash2.png">
                                <h6>5859</h6>
                                <p>Clicks</p>
                            </div>
                            <div class="foot_wrap_innner dashbg_red foot_wrap_full">
                                <img src="{{ $assets_url }}/img/dasg3.png">
                                <div class="foot_wrap_full_contaent">
                                    <h6>0</h6>
                                    <p>Conversion Rate</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- main content section end -->
@endsection
@section('script')
    <!--high chart js-->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script type="text/javascript">
        Highcharts.chart('container', {
            chart: {
                type: 'spline'
            },
            /*title: {
              text: 'Monthly Average Temperature'
            },
            subtitle: {
              text: 'Source: WorldClimate.com'
            },*/
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'No. of Application'
                },
                labels: {
                    formatter: function () {
                        return this.value + '';
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: 'red',
                        lineWidth: 0.5
                    }
                }
            },
            series: [ {
                name: 'Application',
                marker: {
                    symbol: 'circle'
                },
                color: {
                    radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 },
                    stops: [
                        [0, 'red'],
                        [1, 'red'],
                    ]
                },
                data: [{
                    y: 3.9,
                    /*marker: {
                      symbol: 'url(https://www.highcharts.com/samples/graphics/snow.png)'
                    }*/
                }, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
    </script>


@endsection
