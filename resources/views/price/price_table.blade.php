@if(isset($data) && count($data) != 0)
    @php $delete_permission = 0 @endphp
    @php $edit_permission = 0 @endphp
    @php $import_permission = 0 @endphp
    @php $read_permission = 0 @endphp
   @if(role('Prices','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    @if(role('Prices','All','read_permission'))
        @php $edit_permission = 1 @endphp
    @endif
    @if(role('Prices','All','import_permission'))
        @php $import_permission = 1 @endphp
    @endif
    @if(role('Prices','All','read_permission'))
        @php $read_permission = 1 @endphp
    @endif
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Serial Number</th>
                    <th>Part Number</th>
                    <th>Price</th>
                    <!-- @if($import_permission == 1)
                    <th style="width:10%">Import</th>
                    @endif -->
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="icon_td">{{ (@$value->part_data->serial_no)?$value->part_data->serial_no:'NA' }}</td>
                        <td class="icon_td">{{ (@$value->part_data->part_number)?$value->part_data->part_number:'NA' }}</td>
                        <td class="icon_td">{{ convert($value->unit_price) }} {{ (@$value->part_data->ui)?$value->part_data->ui:'' }}</td>
                       
                        <td class="icon_td sty_action">
                        @if($read_permission == 1)
                                <button type="button" onclick="show_price_detail_model('{{ $value->id }}')" class="btn common_btn"><i class="far fa-eye"></i></button>
                            @endif
                            @if($edit_permission == 1)
                                <button type="button" onclick="edit_price('{{ $value->id }}')" class="btn common_btn"><i class="fas fa-edit"></i></button>
                            @endif
                             @if($delete_permission == 1)
                                 <button type="button" onclick="delete_price('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                             @endif
                            
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


