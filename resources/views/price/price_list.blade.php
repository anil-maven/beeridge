@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
    <style>
        .select2-container .selection {
            width: 100% !important;
        }
        .select2-container {
            width: 100% !important;
        }
        .select2-selection
        {
            height: 45px !important;
        }
        #part_id-error
        {
            width: 100%;
            position: absolute;
            top: 43px;
        }
    </style>
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                @include('layouts.flash_msg')

                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span>Price</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item searh_w">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search by serial number, part number"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                               {{-- @if(role('Users','All','export_permission'))
                                    <li>
                                        <form action="{{ url('/users/export-users') }}" method="POST" target="_blank" >
                                            @csrf
                                            <input type="hidden" name="search_string" id="search_string_for_export">
                                            <button type="submit" class="btn new_jobs_btn"><i class="fas fa-file-export"></i> <span>Export</span></button>
                                        </form>
                                    </li>
                                @endif--}}
                                @if(role('Prices','All','import_permission'))
                                <li>
                                    <a target="_blank" href="{{ url('/public/my_assets/price_sample_file.xlsx') }}">
                                        <button class="btn new_jobs_btn"><i class="fas fa-file-download"></i> <span>Download</span></button>
                                    </a>
                                </li>
                                <li>
                                    <span class="file-wrapper" onclick="show_price_import_data()">
                                        <span class="button"><i class="fas fa-file-import"></i> <span>Import</span></span>
                                    </span>
                                </li>
                                @endif
                                @if(role('Prices','All','create_permission'))
                                    <li>
                                        <button class="btn new_jobs_btn" onclick="show_add_price_modal()">
                                            <i class="fas fa-plus"></i>
                                            <span>New Price</span>
                                        </button>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->



                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->


            </div>

        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="add_price_modal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document" id="price_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
      <!-- Add Employee Modal -->
      <div class="modal right fade" id="ProposalItemModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Price Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper py-4" id="show_proposal_item_box">


                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')
    
    @if(isset($html_form))
        <script>
            function show_add_price_modal()
            {
                $('#price_modal_box').html(`{!! $html_form !!}`);
                $('#add_price_modal').modal('show');
                validateForm(true);
                search_part_item();
            }
        </script>
    @endif
    <link href="{{ asset('/my_assets') }}/select2.min.css" rel="stylesheet" />
    <script src="{{ asset('/my_assets') }}/select2.min.js"></script>
    <script>
       function search_part_item()
       {

           $("#part_id").select2({
               placeholder: "Search by item number",
               allowClear: true,
               ajax: {
                   url: '{{ url('/proposal/get-parts-list') }}',
                   dataType: 'json',
                   type: "POST",
                   data: function (term) {
                       return {
                           term: term,
                           _token: $("input[name=_token]").val()
                       };
                   },
                   processResults: function (data) {
                       return {
                           results: $.map(data, function (item) {
                               return {
                                   text: item.part_number,
                                   id: item.id,
                               }
                           })
                       };
                   }

               }
           });

           $(function() {
                $("#contact_person_id").select2({
                    placeholder: "Search supplier",
                    allowClear: true,
                    ajax: {
                        url: '{{ url('/rfq/quotation/get-contact-person-list') }}',
                        dataType: 'json',
                        type: "POST",
                        data: function (term) {
                            return {
                                term: term,
                                _token: $("input[name=_token]").val()
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                   if(item.email)
                                   {
                                       return {
                                           text: item.supplier+' ('+item.email+')',
                                           id: item.id,
                                       }
                                   }
                                   else{
                                       return {
                                           text: item.supplier,
                                           id: item.id,
                                       }
                                   }

                                })
                            };
                        }

                    }
                });
            });
        }
    
  
        function form_submit_by_ajax()
        {
            var url = $('#addUpdateForm').attr('action');
            add_update_details('addUpdateForm',url,'add_price_modal','searchData');
        }
        

        function validateForm() 
        {
            
            // $('#contact_person_id')
            //     .select2()
            //     .on('select2:open', () => {
            //         $(".select2-results:not(:has(a))").append('<a href="javascript:void(0)" onclick="show_add_contact_person_modal()" style="padding: 6px;height: 20px;display: inline-table;">Create new supplier</a>');
            //     })
            

            $("#addUpdateForm").validate({
                rules: {
                    part_id: {
                        required: true,
                    },
                    unit_price: {
                        required: false,
                    },
                   
                },
                messages: {
                    part_id: {
                        required: "{{ trans('msg.part_id_req') }}",
                      
                    },
                    
                },
                errorPlacement: function (label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function (element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }, submitHandler: function (form, event) {
                    if (!this.beenSubmitted) {
                        event.preventDefault();
                        form_submit_by_ajax();
                    }
                }
            });

        }

    </script>

    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
     
        function searchData(page) {
            var search_string = $('#search_string').val();
            $('#search_string_for_export').val(search_string);
            $.ajax(
                {
                    url: '{{ route('search_price_data') }}',
                    type: "POST",
                    data: {
                        search_string: search_string,
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function edit_price(id)
        {
            var url = "{{ route('edit_part_price', ":id") }}";
            url = url.replace(':id', id);

            $.ajax(
                {
                    url: url,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#price_modal_box').html(data);
                $('#add_price_modal').modal('show');
                validateForm();
                search_part_item();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function delete_price(id)
        {
            swal({
                title: "Are you sure ??",
                text: '{{ trans('msg.want_delete_price') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: "{{ route('delete_part_price') }}",
                            type: "POST",
                            data: {
                                id: id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
        function show_price_detail_model(price_id)
        {
            var url = "{{ route('show_part_price', ":price_id") }}";
            url = url.replace(':price_id', price_id);
            $.ajax(
                {
                    url: url,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#show_proposal_item_box').html(data);
                $('#ProposalItemModal').modal('show');
                //validateForm('ProposalItemModal');
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function show_price_import_data() 
        {
            $('#price_modal_box').html(`{!! $import_html_form !!}`);
            $('#add_price_modal').modal('show');
            search_part_item();
            validateImportForm();
        }
        function validateImportForm() 
        {
            $("#addUpdateFormImport").validate({
                rules: {
                    part_id: {
                        required: true,
                    },
                    price_import: {
                        required: true,
                    },
                   
                },
                messages: {
                    part_id: {
                        required: "{{ trans('msg.part_id_req') }}",
                      
                    },
                    price_import: {
                        required: "File is required.",
                      
                    },
                    
                },
                errorPlacement: function (label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function (element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }, submitHandler: function (form, event) {
                    if (!this.beenSubmitted) {
                        event.preventDefault();
                        import_form_submit_by_ajax();
                    }
                }
            });

        }
        function import_form_submit_by_ajax() 
        {
            var url = $('#addUpdateFormImport').attr('action');
            add_update_details_with_image('addUpdateFormImport',url,'price_import','price_import','add_price_modal','searchData');
       
        }
    </script>
   
@endsection
