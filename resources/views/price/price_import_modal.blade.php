@php $assets_url = asset('/assets') @endphp
<style>
    .form-control:disabled, .form-control[readonly] {
        background-color: #fff;
    }
</style>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Import Price details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class="tabcontent_wrapper ">
            <form class="mt-4" id="addUpdateFormImport" action="{{ route('import_prices') }}" method="POST">
              <div class="sty_inp_bg">
               
                @csrf
              <div class="form-row pt-3">
                <div class="form-group col-md-6">
                    <Select id="part_id" name="part_id" class="form-control">
                        @if(isset($data->part_data->id) && !empty($data->part_data->id))
                            <option value="{{ $data->part_data->id }}">{{ $data->part_data->part_number }}</option>
                        @endif
                    </Select>   
                </div>
                <div class="form-group col-md-6">
                    <select name="contact_person_id" id="contact_person_id" class="form-control">
                        @if(isset($data->contact_person->id) && !empty($data->contact_person->id))
                            <option value="{{ $data->contact_person->id }}">{{ $data->contact_person->name }} @if(!empty($data->contact_person->email)) ({{ $data->contact_person->email }}) @endif</option>
                        @endif
                    </select>
                    <label  class="error mt-1 text-danger common-error" id="contact_person_id_error" for="contact_person_id_name"></label>
                </div>
                <div class="form-group col-md-6 mt-3">
                    <input type="file" name="price_import" id="price_import" accept=".xlsx" placeholder=""></input>
                    <label  class="error mt-1 text-danger common-error" id="price_import_error" for="price_import"></label>
                </div>
                </div>
                

                    <div class="form-group">
                        <div class="form-group cta_btn">
                            <button type="submit" id="addUpdateFormImport_btn" class="btn search_btn">Save</button>
                            <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>

            </form>
        </div>
    </div>
</div><!-- modal-content -->


