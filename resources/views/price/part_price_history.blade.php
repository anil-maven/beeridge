@if(isset($data) && count($data) != 0)
  
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Serial Number</th>
                    <th>Part Number</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                    
                    <td class="icon_td">{{ ($value->part_data->serial_no)?$value->part_data->serial_no:"NA" }}</td>
                      <td class="icon_td">{{ ($value->part_data->part_number)?$value->part_data->part_number:"NA" }}</td>
                    <td class="icon_td">{{ convert($value->unit_price) }} {{ ($value->part_data->ui)?$value->part_data->ui:" " }}</td>
                    
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

 


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


