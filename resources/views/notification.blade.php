@extends('layouts.master')
@section('page_title')
    @if(isset($page_title))
        {{ $page_title }}
    @else
        {{ env('APP_NAME') }}
    @endif
@endsection
@section('style')

@endsection
@section('inner_header')

@endsection
@php $assets_url = asset('/assets') @endphp
@section('section')
    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2 pl-0">
                        <h6 class="h6-title mt-1"><span>Notification</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10 pl-xs-0">


                    </div>
                </div>
                <!-- End Row Filter -->

                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->
            </div>

        </div>
    </section>
    <!-- Start Edit Profile Modal -->
@endsection
@section('script')
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    searchData(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page) {
            /*    var filter_order_by = $('#filter_order_by').val();
                var search_string = $('#search_string').val();*/
            $.ajax(
                {
                    url: '{{ url('/notification/search-notification') }}',
                    type: "POST",
                    data: {
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                /* location.hash = page;*/
                history.pushState("", document.title, window.location.pathname);
                /*  var myNewURL = "{{ url('/hrm/organization') }}";
                window.history.pushState({}, document.title, myNewURL );*/
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function delete_notification(notification_id)
        {
            swal({
                title: "Are you sure ?",
                text: '{{ trans('msg.want_delete_notification') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/notification/delete-single-notification') }}',
                            type: "POST",
                            data: {
                                notification_id: notification_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
    </script>
@endsection
