<script type="text/javascript">
    $('#profile_image').on('change', function () {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile_image_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    });
    jQuery.validator.addMethod("valid_first_name", function (value, element) {
        if(/^[a-zA-Z]+$/i.test(value)) {
            return true;
        } else {
            return false;
        };
    }, "{{ trans('msg.first_name_invalid') }}");
    jQuery.validator.addMethod("valid_last_name", function (value, element) {
        if(/^[a-zA-Z]+$/i.test(value)) {
            return true;
        } else {
            return false;
        };
    }, "{{ trans('msg.last_name_invalid') }}");
    $("#profileForm").validate({
        rules: {
            first_name: {
                required: true,
                valid_first_name:true,
                maxlength: 20,
            },
            last_name: {
                required: true,
                valid_last_name:true,
                maxlength: 20,
            },

        },
        messages: {
            first_name: {
                required: "{{ trans('msg.first_name_req') }}",
                maxlength: "{{ trans('msg.first_name_limit') }}",
            },
            last_name: {
                required: "{{ trans('msg.last_name_req') }}",
                maxlength: "{{ trans('msg.last_name_limit') }}",
            },
            job_title: {
                maxlength: "{{ trans('msg.job_title_limit') }}",
            },
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        }, submitHandler: function (form,event) {
            if (!this.beenSubmitted)
            {
                event.preventDefault();
                profile_form_submit_by_ajax();
            }
        }
    });
    function profile_form_submit_by_ajax()
    {
        var url = $('#profileForm').attr('action');
        add_update_details_with_image('profileForm',url,'profile_image','image','EditProfileModal','');
    }
</script>

<script>
    $("#changePasswordForm").validate({
        rules: {
            old_password: {
                required: true
            },
            password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                equalTo: "#new_password"
            },
        },
        messages: {
            old_password: {
                required: '{{ trans('msg.old_password_req') }}'
            },
            password: {
                required: '{{ trans('msg.new_password_req') }}',
                minlength: '{{ trans('msg.new_min_password') }}'
            },
            confirm_password: {
                required: '{{ trans('msg.password_confirm_req') }}',
                equalTo: "{{ trans('msg.same_confirm_password') }}"
            },
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        }, submitHandler: function (form,event) {
            if (!this.beenSubmitted)
            {
                event.preventDefault();
                password_form_submit_by_ajax();
            }
        }
    });

    function password_form_submit_by_ajax()
    {
        var url = $('#changePasswordForm').attr('action');
        add_update_details('changePasswordForm',url,'change_password_modal','');
    }
    function show_hide_password(id)
    {
        var x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
<script>
    function show_change_password_modal()
    {
        $('#old_password').val('');
        $('#new_password').val('');
        $('#confirm_password').val('');
        $('#change_password_modal').modal('show');
    }
</script>
