@if ($paginator->hasPages())
    <div class="col-md-12">
        <div class="pagination_wrapper">
            <ul class="pagination justify-content-center">
                @if($paginator->onFirstPage())
                    <li class="page-item disabled">
                        <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                            <span aria-hidden="true">
                                <img src="{{ asset('/assets') }}/img/right-arrow.png" alt="">
                            </span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous">
                            <span aria-hidden="true">
                                <img src="{{ asset('/assets') }}/img/right-arrow.png" alt="">
                            </span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="page-item disabled">
                            <a class="page-link" href="javascript:void(0)">
                                <span>{{ $element }}</span>
                            </a>
                        </li>

                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="page-item active disabled">
                                    <a class="page-link" href="javascript:void(0)">
                                        {{ $page }}
                                    </a>
                                </li>
                            @else
                                <li class="page-item">
                                    <a class="page-link" href="{{ $url }}">
                                        {{ $page }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach



                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->nextPageUrl() }}">
                            <img src="{{ asset('/assets') }}/img/right-arrow.png" alt="">
                        </a>
                    </li>

                @else
                    <li class="page-item disabled">
                        <a class="page-link" href="javascript:void(0)">
                            <img src="{{ asset('/assets') }}/img/right-arrow.png" alt="">
                        </a>
                    </li>

                @endif
            </ul>
        </div>
    </div>
@endif
