@php $segment1 = Request::segment(1) @endphp
<aside class="admin-sidebar">
    <form class=" serch_bar">
        <input  type="search" id="search_sidebar_input" onkeyup="search_sidebar()" placeholder="Search" aria-label="Search">
        <button class="serch_btn" type="submit">
            <img src="{{ asset('/') }}assets/img/search-icon.png" alt="">
        </button>
    </form>
    <a href="#" class="closebtn mob_closebtn">×</a>

    <div class="admin-sidebar-brand">
        <button type="button" class="btn toggle_btn sidebar_add_btn">
      <!--       <div class="lineOne"></div>
            <div class="lineTwo"></div>
            <div class="lineThree"></div> -->
            <img src="{{ asset('/') }}assets/img/toglebar_icon.svg" alt="">
        </button>
    </div>
    <div class="sidebar-main-wrapper">
        <div class="admin-sidebar-wrapper">
            <ul class="menu" id="sidebar_search_box">
                <li class="menu-item @if($segment1 == 'dashboard') active @endif">
                    <a href="{{ url('/dashboard') }}" class="menu-link">
                        <span class="menu-label">
                            <img src="{{ asset('/') }}assets/img/dashboard_icon.svg" alt="">
                        </span>
                        <span class="menu-icon">
                            Dashboard
                        </span>
                    </a>
                </li>
                @if(isset($module_data) && count($module_data) != 0)
                    @foreach($module_data AS $key => $value)
                        @php $url_explode = explode('/',$value->url); @endphp
                        @php $active_class = 0; @endphp
                        @if(isset($url_explode[1]) && $segment1 == $url_explode[1])
                            @php $active_class = 1; @endphp
                        @endif


                        <li class="menu-item  @if($active_class == 1)  active @endif" @if($active_class == 1)  id="sidebarActiveId" @endif>
                            <a href="{{ url($value->url) }}" class="menu-link">
                                <span class="menu-label">
                                    <img src="{{ url($value->icon) }}" alt="">
                                </span>
                                <span class="menu-icon">{{ $value->module_name }}</span>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</aside>
