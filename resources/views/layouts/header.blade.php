@php $segment1 = Request::segment(1) @endphp
<header class="home-header main_header">
    <div class="container-fluid">
        <div class="sty_header_inner">
            <div class="row">
                <div class="col-md-7 dp_col">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @if(isset($module_data) && count($module_data) != 0)
                            @foreach($module_data AS $key => $value)
                                @php $url_explode = explode('/',$value->url); @endphp
                                @if(isset($url_explode[1]) && $segment1 == $url_explode[1])
                                    <li class="nav-item" onclick="window.location.href='{{ url($value->url) }}'" style="cursor: pointer;">
                                       <div class="d-flex align-items-center">
                                           <a href="javascript:void(0)" class="back_btn"><i class="fas fa-chevron-left"></i></a>
                                           <img src="{{ url($value->icon) }}"  alt="">
                                           <span> {{ $value->module_name }}</span>
                                       </div>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="col-md-5">
                    <nav class="navbar navbar-expand-sm navbar-dark">
                        <button type="button" class="btn toggle_btn sidebarclose_btn">
                            <div class="lineOne"></div>
                            <div class="lineTwo"></div>
                            <div class="lineThree"></div>
                        </button>
                        <!-- Links -->
                        <ul class="navbar-nav justify-content-end">
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <img class="setting_img" src="{{ asset('/') }}assets/img/setting_icon.svg" alt="">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/logout') }}">
                                    <img class="logout_img" src="{{ asset('/') }}assets/img/logout_icon.svg" alt="">
                                </a>
                            </li>
                            <!-- Dropdown -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                                    @if(isset($auth_user->full_image_url))
                                        <img src="{{ $auth_user->full_image_url }}" id="user_full_image_url_preview" class="pro-img" alt="">
                                    @endif
                                    <span id="user_full_name_text">@if(isset($auth_user->full_name)){{ $auth_user->full_name }}@endif</span>
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="javascript:void(0)" type="button" data-toggle="modal" data-target="#EditProfileModal">Edit Profile</a>
                                    <a class="dropdown-item" href="javascript:void(0)" onclick="show_change_password_modal()">Change Password</a>
                                    <a class="dropdown-item" href="{{ url('/notification') }}">Notification</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-7 mb_col">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <img src="{{ asset('/') }}assets/img/proposal.png" alt="">
                            <span>Proposal</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <div>


</header>{{--
<header class="home-header main_header no-border-bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-sm navbar-dark">
                    <button type="button" class="btn toggle_btn sidebarclose_btn">
                        <div class="lineOne"></div>
                        <div class="lineTwo"></div>
                        <div class="lineThree"></div>
                    </button>

                    <!-- Links -->
                    <ul class="navbar-nav justify-content-end">
                        <li class="nav-item search_box">
                            <form method="get" action="#" class="header_search">
                                <div class="form-group">
                                    <input type="text" class="form-control" autocomplete="off" name="" placeholder="" required="">
                                    <img class="search_img" src="{{ asset('/') }}assets/img/search-icon.png" alt="">
                                    <button type="submit" class="btn btn-sm search_btn"><img src="{{ asset('/') }}assets/img/right-arrow.png" alt=""></button>
                                </div>
                            </form>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img class="setting_img" src="{{ asset('/') }}assets/img/setting_icon.svg" alt="">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/logout') }}">
                                <img class="logout_img" src="{{ asset('/') }}assets/img/logout_icon.svg" alt="">
                            </a>
                        </li>
                        <!-- Dropdown -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                                @if(isset($auth_user->full_image_url))
                                    <img src="{{ $auth_user->full_image_url }}" id="user_full_image_url_preview" class="pro-img" alt="">
                                @endif
                                <span id="user_full_name_text">@if(isset($auth_user->full_name)){{ $auth_user->full_name }}@endif</span>
                             --}}{{--   <img src="{{ asset('/') }}assets/img/profile-img.jpg" class="pro-img" alt="">
                                    John Doe--}}{{--
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="javascript:void(0)" type="button" data-toggle="modal" data-target="#EditProfileModal">Edit Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)" onclick="show_change_password_modal()">Change Password</a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>--}}

