<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('page_title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.min.css.map">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.css.map">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.min.css">

    <!-- Your custom styles -->
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/custom.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/login.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/form.css">
        <link rel="stylesheet" href="{{ asset('/my_assets') }}/fonts/stylesheet.css">
    @yield('style')
    <style>
        .form-control:disabled, .form-control[readonly] {
            background-color: unset;
        }
        .loader {
            margin-top: 10%;
            margin-left: 50%;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #037ecc;
            border-bottom: 16px solid #037ecc;
            border-right: 16px solid #1381d5;
            border-left: 16px solid #1381d5;
            width: 100px;
            height: 100px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes    spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .loader-box
        {
            display: block;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            opacity: 1;
            background-color: #fff;
            z-index: 999999999999;
            text-align: center;

        }
        .loader-box .flex-center{
            height: 100%;
            align-items: center;
            justify-content: center;
            display: flex;
            width: 100%;
            padding: 10px;
        }

    </style>
</head>
<body>
<div class="loader-box"  id="loader">
    <div class="flex-center">
        <img style="height: 50%;width: auto;" src="{{ asset('/default_icon/loader-logo3.gif') }}"/>
    </div>
</div>
    @yield('section')

    <script type="text/javascript" src="{{ asset('/') }}assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}assets/js/popper.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset('/my_assets') }}/jquery.validate.min.js"></script>
    @yield('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.common-alert').delay(3000).fadeOut();
        });
        $(document).ready(function() {
            $('#loader').fadeOut('slow');
        });
    </script>
</body>
</html>
