@if(Session::has('success'))
    <p class="alert alert-success common-alert">
        {{ Session::get('success') }}
    </p>
@endif
@if(Session::has('error'))
    <p class="alert alert-danger common-alert">
        {{ Session::get('error') }}
    </p>
@endif
