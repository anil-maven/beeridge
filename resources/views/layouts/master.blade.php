<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('page_title')</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.min.css.map">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.css.map">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.min.css">
    <!-- Your custom styles -->
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/table.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/layout.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/form.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/custom.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/modal.css">
    <link rel="stylesheet" href="{{ asset('/my_assets') }}/common.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/css/n_modal.css">
    

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/my_assets') }}/fonts/stylesheet.css">

    @yield('style')
</head>
<body>

@include('layouts.header')
@include('layouts.sidebar')
<div class="loader-box"  id="loader">
    <div class="flex-center">
        <img style="height: 50%;width: auto;" src="{{ asset('/default_icon/loader-logo3.gif') }}"/>
    </div>
</div>
@yield('section')
<!-- Start Edit Profile Modal -->
<div class="modal right fade" id="EditProfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Profile @if(isset($auth_user->user_role_name) && !empty($auth_user->user_role_name))<small>({{ $auth_user->user_role_name }})</small>@endif</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="tabcontent_wrapper">
                    <form id="profileForm" action="{{ url('/update-profile') }}" method="POST"  enctype="multipart/form-data">
                        <div class="sty_inp_bg">
                        <input type="hidden" name="profile_btn"  value="1">
                        @csrf
                        @include('layouts.flash_msg')
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="form-group col-md-4">
                                         <label for="profile_image" class="md-form-lable"> Profile Image</label>
                                    <div class="img_picker_wrap">
                                        <input type="file" class="image-file" accept="image/*" id="profile_image" name="image" placeholder=" ">
                                        <label for="profile_image">
                                            <img id="profile_image_preview" src="@if(isset($auth_user->full_image_url)){{ $auth_user->full_image_url }}@else{{ $assets_url }}/img/camera.png @endif" alt="">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           placeholder=" "
                                           value="@if(!empty(old('first_name'))){{ old('first_name') }}@elseif(isset($auth_user->first_name)){{ $auth_user->first_name }}@endif"
                                           name="first_name"
                                           id="first_name">
                                          <label for="first_name" class="md-form-lable">First Name</label>
                                    @if($errors->has('first_name'))
                                        <label  class="error mt-2 text-danger" for="first_name">{{ $errors->first('first_name') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <input type="text"
                                           class="form-control"
                                           placeholder=" "
                                           value="@if(!empty(old('last_name'))){{ old('last_name') }}@elseif(isset($auth_user->last_name)){{ $auth_user->last_name }}@endif"
                                           name="last_name"
                                           id="last_name">
                                    <label for="last_name" class="md-form-lable">Last Name</label>
                                    @if($errors->has('last_name'))
                                        <label  class="error mt-2 text-danger" for="last_name">{{ $errors->first('last_name') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="text"
                                       class="form-control"
                                       readonly
                                       value="@if(isset($auth_user->email)){{ $auth_user->email }}@endif"
                                       placeholder=" ">
                                <label for="Email" class="md-form-lable">Email</label>
                            </div>
                        </div>
                        </div>
                            <div class="">
                                <div class="form-group cta_btn">
                                    <button type="submit" id="profileForm_btn" class="btn search_btn">Save</button>
                                    <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<!-- End Edit Profile Modal -->
<!-- Start Edit Profile Modal -->
<div class="modal right fade" id="change_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="tabcontent_wrapper ">
                    <form class="mt-4" id="changePasswordForm" action="{{ url('/change-password') }}" method="POST"  >
                        <div class="sty_inp_bg">
                        @csrf
                        @include('layouts.flash_msg')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-2">
                                    <label for="old_password" class="form_lable">Old Password</label>
                                    <input type="password"
                                           class="form-control"
                                           name="old_password"
                                           id="old_password"
                                           value="@if(old('old_password')){{ old('old_password') }}@endif"
                                           placeholder="Old Password">
                                    <img onclick="show_hide_password('old_password')" class="eye_img" src="{{ $assets_url }}/img/eye-icon.png" alt="">
                                    <label  class="error mt-2 text-danger common-error" id="old_password_error" for="old_password"></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mt-2">
                                    <label for="new_password" class="form_lable">New Password</label>
                                    <input type="password"
                                           class="form-control"
                                           name="password"
                                           id="new_password"
                                           value="@if(old('password')){{ old('password') }}@endif"
                                           placeholder="New Password">
                                    <img onclick="show_hide_password('new_password')" class="eye_img" src="{{ $assets_url }}/img/eye-icon.png" alt="">
                                    <label  class="error mt-2 text-danger common-error" id="password_error" for="password"></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mt-2">
                                    <label for="confirm_password" class="form_lable">Confirm Password</label>
                                    <input type="password"
                                           class="form-control"
                                           name="confirm_password"
                                           id="confirm_password"
                                           value="@if(old('confirm_password')){{ old('confirm_password') }}@endif"
                                           placeholder="Confirm Password">
                                    <img onclick="show_hide_password('confirm_password')" class="eye_img" src="{{ $assets_url }}/img/eye-icon.png" alt="">
                                    <label  class="error mt-2 text-danger common-error" id="confirm_password_error" for="confirm_password"></label>
                                </div>
                            </div>
                        </div>
                        </div>
                            <div>
                                <div class="form-group cta_btn">
                                    <button type="submit" id="changePasswordForm_btn" class="btn search_btn">Save</button>
                                    <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<!-- End Edit Profile Modal -->
<script type="text/javascript" src="{{ asset('/') }}assets/js/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}assets/js/popper.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}assets/js/custom.js"></script>
<script type="text/javascript" src="{{ asset('/my_assets') }}/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ asset('/my_assets') }}/common.js?v={{ time() }}"></script>
<script src="{{ asset('/my_assets') }}/sweetalert.min.js"></script>


<!-- lef Menu bar -->
<script type="text/javascript">
    $(document).ready(function () {
        $('.common-alert').delay(6000).fadeOut();
    });
    $(document).ready(function() {
        $('#loader').fadeOut('slow');
    });
</script>
<script>

    $(document).ready(function(){
        $('.admin-sidebar-brand').on('click', function () {
            $('.admin-sidebar').css("transform", "translateX(0)").css("width", "200px");
            $('.serch_bar').show();
            $('.admin-sidebar').addClass('aside_sty');
            $('.closebtn').show();
            $('.admin-sidebar-brand').hide();
            $('.admin-sidebar .menu').filter(function(){
                return this.childNodes.length > 6
            // }).css("columns", "2");
            }).addClass("sty_two_col");
            $('.main-content').css("margin-left", "120px");
        });

        $('.closebtn').on('click', function () {
            $('.admin-sidebar').css("transform", "translateX(-96px)").css("width", "105px");
            $('.serch_bar').hide();
            $('.closebtn').hide();
            $('.admin-sidebar').removeClass('aside_sty');
            $('.admin-sidebar-brand').show();
            $('.admin-sidebar .menu').filter(function(){
                return this.childNodes.length > 6
            // }).css("columns", "1");
             }).removeClass("sty_two_col");
        });

        $('.sidebarclose_btn').on('click', function () {
            $('.serch_bar').show();
            $('.closebtn').show();
            $('.admin-sidebar').addClass('aside_sty');
            $('.admin-sidebar-brand').hide();

            if (window.matchMedia("(max-width:767px)").matches) {
                $(".admin-sidebar").css("transform", "translateX(0)").css("width", "200px");
                $('.admin-sidebar .menu').filter(function(){
                    return this.childNodes.length > 6
                // }).css("columns", "2");
                 }).addClass("sty_two_col");
            }else{
                $('.admin-sidebar-brand').show();
            }
        });
    });
  /*  $('.sidebar-main-wrapper').animate({
        scrollTop: $("#sidebarActiveId").offset().top
    }, 100);*/
    var $container = $(".sidebar-main-wrapper");
    var $scrollTo = $('#sidebarActiveId');

    $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},300);
    function search_sidebar()
    {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("search_sidebar_input");
        filter = input.value.toUpperCase();
        ul = document.getElementById("sidebar_search_box");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++)
        {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
</script>
@include('layouts.profile_details_js')
@yield('script')
</body>
</html>
