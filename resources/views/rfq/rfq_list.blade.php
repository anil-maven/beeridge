@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp

    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                @include('layouts.flash_msg')
                @if(Session::has('failures'))
                    <div class="alert alert-danger common-alert" role="alert">
                        <strong>Errors:</strong>

                        <ul>
                            @foreach(Session::get('failures') as $failure)
                                @foreach ($failure->errors() as $error)
                                    <li>{{ $error }} At {{ $failure->row() }} Row</li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row mb-3">
                    <div class="col-xl-2 col-lg-2">
                        <h6 class="h6-title mt-1"><span>RFQ</span> </h6>
                    </div>
                    <div class="col-xl-10 col-lg-10">

                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li class="nav-item w-25">
                                    <div  class="header_search">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   id="search_string"
                                                   onkeyup="searchData(1)"
                                                   placeholder="Search FAQ"
                                                   required="">
                                            <button type="button" class="btn btn-sm search_btn">
                                                <img class="search_img" src="{{ $assets_url }}/img/search-icon.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </li>
                                {{--  <li style="cursor: pointer;" onclick="show_part_filter_modal()">
                                      <p class="mb-0">
                                          <i class="fas fa-filter"></i>
                                          <span> Filters</span>
                                      </p>
                                  </li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Row Filter -->

                <div class="row mb-4">
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color1">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/not.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>@if(isset($proposal_count)){{ $proposal_count }}@else 0 @endif</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color2">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/contracts.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color3">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/paid.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card sty_card_box bg_color4">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <img src="{{ $assets_url }}/img/recived.png" >
                                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                </div>
                                <h5>0</h5>
                                <p>Proposal Created</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row list Box -->

                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->


            </div>

        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="add_proposal_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document" id="proposal_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')

    <script>
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page)
        {
            var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/rfq/search-rfq') }}',
                    type: "POST",
                    data: {
                        search_string: search_string,
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);


    </script>

@endsection
