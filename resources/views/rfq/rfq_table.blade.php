@if(isset($data) && count($data) != 0)
    @php $delete_permission = 0 @endphp
    @php $read_permission = 0 @endphp
    @if(role('RFQ','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    @if(role('RFQ','All','read_permission'))
        @php $read_permission = 1 @endphp
    @endif
    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>RFQ Name</th>
                    <th>Date Received</th>
                    <th># of Items</th>
                    <th>Operation</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="icon_td">{{ $value->proposal_name }}</td>
                        <td class="icon_td">{{ $value->faq_receive_date }}</td>
                        <td class="icon_td">
                            @if(!empty($value->total_item))
                                {{ $value->total_item }}
                            @else
                                NA
                            @endif
                        </td>

                        <td class="icon_td sty_status">
                            @if($value->client_want_status == 1)
                                Pricing
                            @elseif($value->client_want_status == 2)
                                Purchase
                            @elseif($value->client_want_status == 3)
                                Research
                            @elseif($value->client_want_status == 4)
                                Other
                            @elseif($value->client_want_status == 4)
                                NA
                            @endif
                        </td>
                        <td class="icon_td sty_status">
                            {!! get_sp_proposal_status($value->status)['btn'] !!}
                        </td>

                        <td class="icon_td sty_action">
                            @if($read_permission == 1)
                                <button type="button" onclick="window.location.href='{{ url('/rfq/view/'.$value->id) }}'" class="btn common_btn"><i class="far fa-eye"></i></button>
                            @endif
                           {{-- @if($delete_permission == 1)
                                <button type="button" onclick="delete_proposal('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                            @endif--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


