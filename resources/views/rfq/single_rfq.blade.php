@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
    <style>
        .select2-container .selection {
            width: 100% !important;
        }
        .select2-container {
            width: 100% !important;
        }
        .select2-selection
        {
            height: 40px !important;
        }
    </style>
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp
    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">

                <div class="row mb-3">
                    <div class="col-xl-4 col-lg-4">
                        <h6 class="h6-title mt-1">
                            <span><a href="{{ url('/rfq') }}">RFQ</a></span>
                            <img src="{{ $assets_url }}/img/right_arrow.svg" class="{{ $assets_url }}/img-fluid">
                            @if(isset($data->proposal_name) && !empty($data->proposal_name))
                                <a href="javascript:void(0)"> <span>{{ $data->proposal_name }}</span></a>
                            @endif
                        </h6>
                    </div>
                    <div class="col-xl-8 col-lg-8 pl-xs-0">
                        <div class="manage_jobs_right_wrapper mng_contacts sty_btngrup2">
                            <ul class="nav justify-content-end align-items-center">
                                <li>
                                    <a href="{{ url('/rfq/details/'.$data->id) }}" class="btn sty_btn_2" type="button">View RFQ Item details</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End  -->


                <div class="card border-0">
                    <div class="card-body">
                        <div class="row prop_details">
                            <div class="col-lg-6 ">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/proname.png" class="img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">RFQ Name:</h5>
                                                @if(isset($data->proposal_name) && !empty($data->proposal_name))
                                                    <p>{{ $data->proposal_name }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/date.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Date Received:</h5>
                                                @if(isset($data->faq_receive_date) && !empty($data->faq_receive_date))
                                                    <p>{{ $data->faq_receive_date }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/items.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">No. Of Items:</h5>
                                                <p id="no_of_item_count">{{ $item_count }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/user.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Buyer:</h5>
                                                <p id="service_provider_name_box">
                                                    @if(isset($data->created_by_name) && !empty($data->created_by_name))
                                                        {{ $data->created_by_name }}
                                                    @else
                                                        Not Available
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="col-lg-6">
                                        <div class="media">
                                            <img class="mr-3" src="{{ $assets_url }}/img/qustion.png" class="{{ $assets_url }}/img-fluid">
                                            <div class="media-body">
                                                <h5 class="mt-0">Qustions</h5>
                                                <p>Not Available</p>
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                                <!-- End Row -->

                                <!-- <p class="stylable">Items Details</p> -->
                                <div class="row sty_items_table" id="search_list_box">

                                </div>

                            </div>
                            <div class="col-lg-3">

                                <div class="media mb-4">
                                    <img class="mr-3" src="{{ $assets_url }}/img/timeline.png" class="img-fluid">
                                    <div class="media-body">
                                        <h5 class="mt-0">Proposal Timeline</h5>
                                    </div>
                                </div>

                                <div class="sty_timelins" id="proposal_activity_box">
                                    @if(isset($proposal_activity_html) && !empty($proposal_activity_html))
                                        {!! $proposal_activity_html !!}
                                    @endif
                                </div>

                            </div>
                            <!-- Chatbox -->
                                <div class="col-lg-3">
                                    <p>  <img class="mr-1" src="{{ $assets_url }}/img/proname.png" class="img-fluid">Message to Client</p>
                                    <div class="wrapper  chatbox">
                                        <div class="main">
                                            <div class="px-2 scroll">
                                              <div class="text-center chat_icon">
                                                  <img src="{{ $assets_url }}/img/chectimg.png" class="img-fluid">
                                              </div>
                                                <div> 
                                                    <div class="chet_cmt">
                                                        <p>Client Not Available </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <nav class="navbar navbar-expand-sm d-flex justify-content-between"> 
                                                <a href="#"><img src="{{ $assets_url }}/img/clip1.svg" class="img-fluid"></a>
                                                <input type="text number" name="text" class="form-control" placeholder="Type a message here...">
                                               <a href="#"> 
                                                <img src="{{ $assets_url }}/img/send.svg" class="img-fluid" style="width: 33px">
                                               </a>
                                                
                                               <!--  <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2"> <i class="fa fa-paperclip icon1"></i> <i class="fa fa-arrow-circle-right icon2"></i> </div> -->
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                <!--End  Chatbox -->
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </section>
    <!-- main content section end -->

    <!-- Edit Add item Modal -->
    <div class="modal right fade PropaslModal" id="ProposalModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper py-5" style="padding-bottom: 0px !important;">
                        <p>What Do you want to add</p>
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Items</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Service</a>
                            </li>
                        </ul>
                        <form action="#" method="get" class="">

                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show proposalform" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <!--  <input type="text" class="form-control" placeholder=" "  id="cc_name">
                                                 <label for="cc_name" class="md-form-lable">Propasl Name</label> -->
                                                <Select id="part_id" name="part_id" class="form-control">

                                                </Select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade proposalform" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                Service Not Available
                                                <!--      <input type="text" class="form-control" placeholder=" "  id="cc_name">
                                                     <label for="cc_name" class="md-form-lable">Search to add Services</label> -->
                                                {{--  <Select id="colorselector2" class="form-control">
                                                      <option value="secondoption2">1</option>
                                                      <option value="secondoption2">2</option>
                                                      <option value="secondoption2">3</option>
                                                  </Select>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tabcontent_wrapper py-5" id="show_item_box" style="padding-top: 0px !important;display: none;">


                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="modal right fade" id="ProposalItemModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Item Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper py-4" id="show_proposal_item_box">


                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


@endsection
@section('script')
    <script>

        $('#colorselector').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption"){
                $('#PropaslModal_2').modal("show");
            }

        });

        $('#colorselector2').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption2"){
                $('#Services_Modal_3').modal("show");
            }
        });

        $('#colorselector3').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption3"){
                $('#Services_Modal_4').modal("show");
            }
        });

        $('#colorselector4').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption4"){
                $('#Services_Modal_5').modal("show");
            }
        });


        // For quantity
        $(document).ready(function(){
            $('.count').prop('disabled', true);
            $(document).on('click','.plus',function(){
                $('.count').val(parseInt($('.count').val()) + 1 );
            });
            $(document).on('click','.minus',function(){
                $('.count').val(parseInt($('.count').val()) - 1 );
                if ($('.count').val() == 0) {
                    $('.count').val(1);
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page)
        {
            //  var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/rfq/search-rfq-item') }}',
                    type: "POST",
                    data: {
                        // search_string: search_string,
                        proposal_id:'{{ $data->id }}',
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function view_proposal_item_modal(id)
        {
            $.ajax(
                {
                    url: '{{ url('/rfq/single-rfq-item') }}/'+id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#show_proposal_item_box').html(data);
                $('#ProposalItemModal').modal('show');
                validateForm('ProposalItemModal');
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
    </script>

@endsection
