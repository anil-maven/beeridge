@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @php $read_permission = 0 @endphp
    {{-- @if(role('Proposal','All','amend_permission'))
         @php $edit_permission = 1 @endphp
     @endif
     @if(role('Proposal','All','delete_permission'))
         @php $delete_permission = 1 @endphp
     @endif
     @if(role('Proposal','All','read_permission'))
         @php $read_permission = 1 @endphp
     @endif--}}
    <div class="col-md-12">
        <div class="responsive_table">
            <table class="table mb-0">
                <thead>
                <tr>
                    <th>Item details</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th class="text-lg-right">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && count($data) != 0)
                    @foreach($data AS $key => $value)
                        <tr>
                            <td class="common_td">
                                {{ $value->part_number }}
                            </td>
                            <td class="common_td text-lg-right">{{ $value->qty }}</td>
                            <td class="common_td text-lg-right"> {!! get_rfq_item_status($value->item_status)['btn'] !!}</td>
                            <td class="">
                                <a type="button" href="javascript:void(0)" onclick="view_proposal_item_modal('{{ $value->id }}')" class="btn common_btn p-0"><i class="fas fa-eye"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
    </div>
    {{ $data->render("layouts.pagination") }}



@endif


