@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
    <style>
        .select2-container .selection {
            width: 100% !important;
        }
        .select2-container {
            width: 100% !important;
        }
        .select2-selection
        {
            height: 45px !important;
        }
    </style>
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp
    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-xl-4 col-lg-4">
                        <h6 class="h6-title mt-1">
                            <span><a href="{{ url('/rfq') }}">RFQ</a></span>
                            <img src="{{ $assets_url }}/img/right_arrow.svg" class="{{ $assets_url }}/img-fluid">
                            @if(isset($data->proposal_name) && !empty($data->proposal_name))
                                <span><a href="{{ url('/rfq/view/'.$data->id) }}">{{ $data->proposal_name }}</a></span>
                            @endif
                            <img src="{{ $assets_url }}/img/right_arrow.svg" class="{{ $assets_url }}/img-fluid">
                            <a href="javascript:void(0)"> <span>Details</span></a>
                        </h6>
                    </div>
                    <div class="col-xl-8 col-lg-8 pl-xs-0">
                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li>
                                    @if(isset($data->quotation_status) && $data->quotation_status == 0)
                                    <button class="btn new_jobs_btn" onclick="submit_quotation()">
                                        <i class="fas fa-plus"></i>
                                        <span>Submit Quotation</span>
                                    </button>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End  -->
                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->
            </div>
        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="ProposalItemModal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Item Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabcontent_wrapper py-4" id="show_proposal_item_box">


                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


@endsection
@section('script')
    <script>

        $('#colorselector').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption"){
                $('#PropaslModal_2').modal("show");
            }

        });

        $('#colorselector2').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption2"){
                $('#Services_Modal_3').modal("show");
            }
        });

        $('#colorselector3').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption3"){
                $('#Services_Modal_4').modal("show");
            }
        });

        $('#colorselector4').change(function() {
            var opval = $(this).val();
            if(opval=="secondoption4"){
                $('#Services_Modal_5').modal("show");
            }
        });


        // For quantity
        $(document).ready(function(){
            $('.count').prop('disabled', true);
            $(document).on('click','.plus',function(){
                $('.count').val(parseInt($('.count').val()) + 1 );
            });
            $(document).on('click','.minus',function(){
                $('.count').val(parseInt($('.count').val()) - 1 );
                if ($('.count').val() == 0) {
                    $('.count').val(1);
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page)
        {
            //  var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/rfq/search-item-details') }}',
                    type: "POST",
                    data: {
                        // search_string: search_string,
                        proposal_id:'{{ $data->id }}',
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function view_proposal_item_modal(id)
        {
            $.ajax(
                {
                    url: '{{ url('/rfq/single-rfq-item-with-quotation') }}/'+id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#show_proposal_item_box').html(data);
                $('#ProposalItemModal').modal('show');
                //validateForm('ProposalItemModal');
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function update_client_amount(select_id,input_id,proposal_item_id)
        {
            var select_value = $('#'+select_id).val();
            var input_value = $('#'+input_id).val();
            var error_status = false;
            var error_message = "";
            if(input_value <1)
            {
                error_status = true;
                error_message = 'Price is required.';
            }
            else if(select_value == 'percentage')
            {
                if(input_value >100)
                {
                    error_status = true;
                    error_message = 'Percentage value not greater then 100.';
                }
            }
            if(error_status)
            {
                error_msg(error_message);
                return false;
            }
            else
            {
                $.ajax(
                    {
                        url: '{{ url('/rfq/update-client-price') }}',
                        type: "POST",
                        data: {
                            client_price_type: select_value,
                            client_price: input_value,
                            proposal_item_id: proposal_item_id,
                            _token: $("input[name=_token]").val()
                        },
                        datatype: "json"
                    }).done(function (data) {
                    success_msg(data.msg);
                    searchData(1);
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    var errorData = JSON.parse(jqXHR.responseText);
                    error_msg(errorData.msg);
                });
            }
        }
        function submit_quotation()
        {
            swal({
                title: "Are you sure ??",
                text: '{{ trans('msg.want_to_submit_quotation') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/rfq/submit-quotation') }}',
                            type: "POST",
                            data: {
                                // search_string: search_string,
                                proposal_id:'{{ $data->id }}',
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "html"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                            error_msg(jqXHR.responseJSON.msg);

                    });
                } else {
                    swal.close()
                }
            });

        }
    </script>

@endsection
