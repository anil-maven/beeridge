@if(isset($data) && count($data) != 0)
    @foreach($data AS $key => $value)
        <div class="media">
            @if(!empty($value->icon))
                <img class="mr-3 img-fluid" src="{{ asset('/assets') }}/img/{{ $value->icon }}">
            @endif
            <div class="media-body draft_box">
                <p class="mb-0">{{ Date('Y-m-d', strtotime($value->created_at)) }} |  {{ Date('g:i a', strtotime($value->created_at)) }}</p>
                <h5 class="mt-0">
                    @if($value->title == 'RFQ Sent')
                        RFQ Received
                    @else
                    {{ $value->title }}
                    @endif
        @if($value->icon == 'QuotationSaved.png')
            <br>
            <a href="{{ url('/rfq/details/'.$value->proposal_id) }}">View Quotation</a>
        @endif
       </h5>
</div>
</div>
@endforeach
@endif
