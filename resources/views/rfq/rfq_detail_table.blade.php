@if(isset($data) && count($data) != 0)
    @php $edit_permission = 0 @endphp
    @php $delete_permission = 0 @endphp
    @php $read_permission = 0 @endphp
    {{-- @if(role('Proposal','All','amend_permission'))
         @php $edit_permission = 1 @endphp
     @endif
     @if(role('Proposal','All','delete_permission'))
         @php $delete_permission = 1 @endphp
     @endif
     @if(role('Proposal','All','read_permission'))
         @php $read_permission = 1 @endphp
     @endif--}}
    <div class="col-md-12">
        <div class="responsive_table">
            <table class="table mb-0">
                <thead>
                <tr>
                    <th>Item details</th>
                    <th>Serial No.</th>
                {{--    <th>Name</th>--}}
                  {{--  <th>Manufacturer</th>--}}
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Amount</th>
                    <th @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 0) style="width: 24%" @endif>Client Price</th>
                    @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 1)
                          <th>Total Client Price</th>
                    @endif
                    <th class="text-lg-right">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && count($data) != 0)
                    @foreach($data AS $key => $value)
                        @php $unit_price = 0 @endphp
                        @if(isset($value->quotation_unit_price) && !empty($value->quotation_unit_price))
                            @php $unit_price = $value->quotation_unit_price @endphp
                        @endif
                        <tr>
                            <td class="common_td">{{ $value->part_number }}</td>
                            <td class="common_td">{{ $value->serial_no }}</td>
                       {{--     <td class="common_td">{{ $value->name }}</td>--}}
                          {{--  <td class="common_td">{{ !empty($value->manufacturer)?$value->manufacturer:'NA' }}</td>--}}
                            <td class="common_td text-lg-right">{{ $value->qty }}</td>
                            <td class="common_td text-lg-right">
                                @if($unit_price != 0)
                                    {{ convert($unit_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif
                                @else
                                    NA
                                @endif
                            </td>
                            <td class="common_td text-lg-right">
                                @if($unit_price != 0)
                                    {{ convert($unit_price*$value->qty) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif
                                @else
                                    NA
                                @endif
                            </td>
                            <td class="common_td text-lg-right">
                                @if($unit_price != 0)
                                    @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 0)
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                           <select class="form-control" style="border-radius:0px;" id="client_price_by_{{ $value->id }}">
                                               <option @if(isset($value->client_price_type) && $value->client_price_type == 'percentage') selected @endif  value="percentage">%</option>
                                               <option @if(isset($value->client_price_type) && $value->client_price_type == 'value') selected @endif value="value">$</option>
                                           </select>
                                        </div>
                                        <input type="number"
                                               class="form-control number_validation"
                                               min="1"
                                               step="0.01"
                                               @if(isset($value->client_price) && !empty($value->client_price))
                                                   value="{{ $value->client_price }}"
                                               @endif
                                               id="client_price_field_{{ $value->id }}"
                                               onkeypress="if(this.value.length>10) return false;else return event.charCode >= 48 && event.charCode <= 57;"
                                               onkeyup="checkDec(this);"
                                               placeholder="New Price">
                                        <div class="input-group-append">
                                            <button type="button"
                                                    onclick="update_client_amount('client_price_by_{{ $value->id }}','client_price_field_{{ $value->id }}','{{ $value->id }}')"
                                                    class="btn new_jobs_btn">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                    @else
                                        @if(isset($value->client_price_type) && $value->client_price_type == 'percentage')
                                            {{ $value->client_price }}%
                                        @elseif(isset($value->client_price_type) && $value->client_price_type == 'value')
                                            {{ convert($value->client_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif
                                        @endif

                                    @endif
                                @else
                                    NA
                                @endif
                            </td>
                            @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 1)
                                <td class="common_td text-lg-right">
                                    {{ convert($value->total_client_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif * {{ $value->qty }} = {{ convert($value->qty * $value->total_client_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }}@endif
                                </td>
                            @endif
                           {{-- <td class="common_td text-lg-right">
                                @if($unit_price != 0)
                                    {{ $unit_price.' '.config('constants.price_symbol') }}
                                @else
                                    NA
                                @endif
                            </td>--}}
                            <td class="text-center" style="padding: inherit;">
                                <button type="button"  onclick="view_proposal_item_modal('{{ $value->id }}')" class="btn new_jobs_btn"><i class="fas fa-eye"></i>View</button>
                                @if(isset($proposal_data->quotation_status) && $proposal_data->quotation_status == 0)
                                <button type="button" onclick="window.location.href='{{ url('/rfq/quotation/'.$value->id) }}'"  title="Manage Prices" class="btn new_jobs_btn"><i class="fas fa-plus"></i> Manage Prices</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
    </div>
    {{ $data->render("layouts.pagination") }}



@endif


