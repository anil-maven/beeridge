@extends('layouts.front_master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('section')
    @php $assets_url = asset('/assets/') @endphp
    <section class="login_section">
        <div class="container">
            <div class="col-md-9 m-auto">
                <div class="login_wrapper">
                    <div class="row no-gutters">
                        <div class="col-md-12 col-lg-12 col-xl-6 d-none d-xl-block">
                            <img src="{{ $assets_url }}/img/bglogin.png" alt="">
                        </div>
                        <div class="col-md-12 col-lg-12 col-xl-6 pt-md-4 pt-lg-4 px-md-5 px-lg-5">

                            <div class="login_head">
                                <img src="{{ $assets_url }}/img/cdllogo.png" alt="">
                                <h5 class="h5-title">Reset Password</h5>
                            </div>
                            <div class="login_body">
                                <form action="{{ route('reset.password.post') }}" method="post" id="postForm">
                                    @csrf
                                    @include('layouts.flash_msg')
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="form-group">
                                        <input type="password"
                                               class="form-control"
                                               placeholder="Enter new your password"
                                               name="password"
                                               readonly
                                               onfocus="this.removeAttribute('readonly');"
                                               autocomplete="new-password"
                                               id="password">
                                        {{-- <label for="login_pwd" class="lg_form_lable">Enter your password</label>--}}
                                        <img src="{{ $assets_url }}/img/key-icon.png" alt="">
                                        <img onclick="show_hide_password('password')" class="eye_img" src="{{ $assets_url }}/img/eye-icon.png" alt="">
                                        <!-- <i class="fas fa-eye-slash"></i> -->
                                        @if($errors->has('password'))
                                            <label  class="error mt-2 text-danger" for="password">{{ $errors->first('password') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input type="password"
                                               class="form-control"
                                               placeholder="Enter Confirm Password"
                                               name="confirm_password"
                                               readonly
                                               onfocus="this.removeAttribute('readonly');"
                                               autocomplete="new-password"
                                               id="confirm_password">
                                        {{-- <label for="login_pwd" class="lg_form_lable">Enter your password</label>--}}
                                        <img src="{{ $assets_url }}/img/key-icon.png" alt="">
                                        <img onclick="show_hide_password('confirm_password')" class="eye_img" src="{{ $assets_url }}/img/eye-icon.png" alt="">
                                        <!-- <i class="fas fa-eye-slash"></i> -->
                                        @if($errors->has('confirm_password'))
                                            <label  class="error mt-2 text-danger" for="confirm_password">{{ $errors->first('password_confirmation') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group mb-1">
                                        <div class="row">
                                            <div class="col-md-7 col-md-12">

                                            </div>
                                            <div class="col-md-5 col-md-12 text-lg-right">
                                                <a href="{{ url('/') }}" class="forgot_pwd_link">Login Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn login_btn">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            var some_id = $('#email');
            some_id.prop('type', 'text');
            some_id.removeAttr('autocomplete');
        });
        function show_hide_password(id)
        {
            var x = document.getElementById(id);
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
        $("#postForm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                },
                confirm_password: {
                    required: true,
                    equalTo: "#password"
                }
            },
            messages: {
                email: {
                    required: "{{ trans('msg.email_req') }}",
                    email: "{{ trans('msg.email_invalid') }}"
                },
                password: {
                    minlength: '{{ trans('msg.new_min_password') }}',
                    required: "{{ trans('msg.password_req') }}",
                },
                confirm_password: {
                    required: '{{ trans('msg.password_confirm_req') }}',
                    equalTo: "{{ trans('msg.same_confirm_password') }}"
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            },submitHandler: function (form) {
                if (!this.beenSubmitted) {
                    this.beenSubmitted = true;
                    form.submit();
                }
            }
        });

    </script>
@endsection
