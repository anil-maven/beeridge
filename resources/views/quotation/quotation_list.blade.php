@extends('layouts.master')
@section('page_title')
    @if(isset($page_title)){{ $page_title }}@else{{ env('APP_NAME') }}@endif
@endsection
@section('style')
    <style>
        .select2-container .selection {
            width: 100% !important;
        }
        .select2-container {
            width: 100% !important;
        }
        .select2-selection
        {
            height: 45px !important;
        }
    </style>
@endsection
@section('section')
    @php $assets_url = asset('/assets') @endphp
    <!-- main content section -->
    <section class="main-content">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-xl-7 col-lg-7">
                        <h6 class="h6-title mt-1">
                            <span><a href="{{ url('/rfq') }}">RFQ</a></span>
                            <img src="{{ $assets_url }}/img/right-arrow.png" class="{{ $assets_url }}/img-fluid">
                            @if(isset($data->proposal_data->proposal_name) && !empty($data->proposal_data->proposal_name))
                                <span><a href="{{ url('/rfq/view/'.$data->proposal_data->id) }}">{{ $data->proposal_data->proposal_name }}</a></span>
                                <img src="{{ $assets_url }}/img/right-arrow.png" class="{{ $assets_url }}/img-fluid">
                            @endif
                            @if(isset($data->part_data->part_number) && !empty($data->part_data->part_number))
                                <span><a href="{{ url('/rfq/details/'.$data->proposal_data->id) }}">{{ $data->part_data->part_number }}</a></span>
                                <img src="{{ $assets_url }}/img/right-arrow.png" class="{{ $assets_url }}/img-fluid">
                            @endif
                           <a href="javascript:void(0)"> <span>Price details</span></a>
                        </h6>
                    </div>
                    <div class="col-xl-5 col-lg-5 pl-xs-0">
                        <div class="manage_jobs_right_wrapper mng_contacts sty_filter">
                            <ul class="nav justify-content-end align-items-center">
                                <li id="quotation_update_box" style="display: none;">
                                    <button class="btn new_jobs_btn" onclick="update_quotation()">
                                        <span>Select Price</span>
                                    </button>
                                </li>
                                <li>
                                    <button class="btn new_jobs_btn" onclick="show_add_quotation_modal()">
                                        <i class="fas fa-plus"></i>
                                        <span>New Price</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End  -->
                <div class="row" id="search_list_box">

                </div>
                <!-- End Table Row -->
            </div>
        </div>
    </section>
    <!-- Add Employee Modal -->
    <div class="modal right fade" id="add_quotation_modal"  role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog " role="document"  id="quotation_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class="modal right fade" id="add_contact_person_modal" style="z-index: 999999999999" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog modal-sm" role="document" id="contact_person_modal_box">

        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')
    <link href="{{ asset('/my_assets') }}/select2.min.css" rel="stylesheet" />
    <script src="{{ asset('/my_assets') }}/select2.min.js"></script>

    @if(isset($html_form))
        <script>
            function show_add_quotation_modal()
            {
                $('#quotation_modal_box').html(`{!! $html_form !!}`);
                $('#add_quotation_modal').modal('show');
                validateForm();
            }
            function validateForm()
            {

            $(function() {
                $("#contact_person_id").select2({
                    placeholder: "Search supplier",
                    allowClear: true,
                    ajax: {
                        url: '{{ url('/rfq/quotation/get-contact-person-list') }}',
                        dataType: 'json',
                        type: "POST",
                        data: function (term) {
                            return {
                                term: term,
                                _token: $("input[name=_token]").val()
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                   if(item.email)
                                   {
                                       return {
                                           text: item.supplier+' ('+item.email+')',
                                           id: item.id,
                                       }
                                   }
                                   else{
                                       return {
                                           text: item.supplier,
                                           id: item.id,
                                       }
                                   }

                                })
                            };
                        }

                    }
                });
            });
            $('#contact_person_id')
                .select2()
                .on('select2:open', () => {
                    $(".select2-results:not(:has(a))").append('<a href="javascript:void(0)" onclick="show_add_contact_person_modal()" style="padding: 6px;height: 20px;display: inline-table;">Create new supplier</a>');
                })


            $("#addUpdateQuotationForm").validate({
                    rules: {
                        contact_person_id_req: {
                            required: true,
                        },
                        quoted_part_number: {
                            required: false,
                            maxlength: 250,
                        },
                        unit_price: {
                            required: true,
                            maxlength: 250,
                        },
                    },
                    messages: {
                        contact_person_id_req: {
                            required: '{{ trans('msg.contact_person_id_req') }}',
                        },
                        quoted_part_number: {
                            required: '{{ trans('msg.quoted_part_number_req') }}',
                        },
                        unit_price: {
                            required: '{{ trans('msg.unit_price_req') }}',
                        },
                    },
                    errorPlacement: function (label, element) {
                        label.addClass('mt-2 text-danger');
                        label.insertAfter(element);
                    },
                    highlight: function (element, errorClass) {
                        $(element).parent().addClass('has-danger')
                        $(element).addClass('form-control-danger')
                    }, submitHandler: function (form,event) {
                        if (!this.beenSubmitted)
                        {
                            event.preventDefault();
                            form_submit_by_ajax();
                        }
                    }
                });
                function form_submit_by_ajax()
                {
                    var url = $('#addUpdateQuotationForm').attr('action');
                    add_update_details('addUpdateQuotationForm',url,'add_quotation_modal','searchData');
                }
            }
        </script>
    @endif
    <script>
        function edit_quotation(quotation_id)
        {
            $.ajax(
                {
                    url: '{{ url('/rfq/quotation/single-quotation') }}/'+quotation_id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#quotation_modal_box').html(data);
                $('#add_quotation_modal').modal('show');
                validateForm();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
        function delete_quotation(quotation_id)
        {
            swal({
                title: "Are you sure ??",
                text: '{{ trans('msg.want_delete_quotation_price') }}',
                icon: "warning",
                buttons: true,
            }).then((willConfirm) =>
            {
                if (willConfirm)
                {
                    $.ajax(
                        {
                            url: '{{ url('/rfq/quotation/delete-quotation') }}',
                            type: "POST",
                            data: {
                                quotation_id: quotation_id,
                                _token: $("input[name=_token]").val()
                            },
                            datatype: "json"
                        }).done(function (data) {
                        success_msg(data.msg);
                        searchData(1);
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        var errorData = JSON.parse(jqXHR.responseText);
                        error_msg(errorData.msg);
                    });
                } else {
                    swal.close()
                }
            });
        }
        function show_quotation_status_btn()
        {
            $('#quotation_update_box').show();
        }
        function update_quotation()
        {
            var quotation_id = $('input[name="quotation_radio"]:checked').val();
            if(quotation_id)
            {
                $.ajax(
                    {
                        url: '{{ url('/rfq/quotation/update-quotation-status') }}',
                        type: "POST",
                        data: {
                            quotation_id: quotation_id,
                            _token: $("input[name=_token]").val()
                        },
                        datatype: "json"
                    }).done(function (data) {
                    success_msg(data.msg);
                    setTimeout(function (){
                        window.location.href='{{ url('/rfq/details/'.$data->proposal_id) }}';
                    },300)
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    var errorData = JSON.parse(jqXHR.responseText);
                    error_msg(errorData.msg);
                });
            }
            else{
                error_msg('Please select one price details');
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                searchData(page);
            });

        });
        function searchData(page)
        {
            //  var search_string = $('#search_string').val();
            $.ajax(
                {
                    url: '{{ url('/rfq/quotation/search-quotation') }}',
                    type: "POST",
                    data: {
                        // search_string: search_string,
                        proposal_item_id:'{{ $data->id }}',
                        page: page,
                        _token: $("input[name=_token]").val()
                    },
                    datatype: "html"
                }).done(function (data) {
                $("#search_list_box").empty().html(data);
                history.pushState("", document.title, window.location.pathname);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });
        }
        searchData(1);
        function view_proposal_item_modal(id)
        {
            $.ajax(
                {
                    url: '{{ url('/rfq/single-rfq-item') }}/'+id,
                    type: "GET",
                    datatype: "html"
                }).done(function(data){
                $('#show_proposal_item_box').html(data);
                $('#ProposalItemModal').modal('show');
                //validateForm('ProposalItemModal');
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                var errorData = JSON.parse(jqXHR.responseText);
                error_msg(errorData.msg);
            });
        }
    </script>

    @if(isset($contact_person_html_form))
        <script>
            function show_add_contact_person_modal()
            {
                $('#contact_person_modal_box').html(`{!! $contact_person_html_form !!}`);
                $('#add_contact_person_modal').modal('show');
                contactValidateForm(true);
            }
            function contactValidateForm()
            {
                $("#addUpdateForm").validate({
                    rules: {
                        supplier: {
                            required: true,
                            maxlength: 250,
                        },
                        name: {
                            required: false,
                            maxlength: 250,
                        },
                        email: {
                            required: false,
                            email: true
                        },
                        phone_number: {
                            required: false,
                        },
                        department: {
                            required: false,
                            maxlength: 250,
                        },
                    },
                    messages: {
                        supplier: {
                            required: "{{ trans('msg.supplier_name_req') }}",
                            maxlength: "{{ trans('msg.supplier_name_limit') }}",
                        },
                        name: {
                            required: "{{ trans('msg.contact_person_name_req') }}",
                            maxlength: "{{ trans('msg.contact_person_name_limit') }}",
                        },
                        email: {
                            required: "{{ trans('msg.email_req') }}",
                            email: "{{ trans('msg.email_invalid') }}"
                        },
                        phone_number: {
                            required: "{{ trans('msg.contact_person_phone_number_req') }}",
                        },
                        department: {
                            required: "{{ trans('msg.contact_person_department_req') }}",
                            maxlength: "{{ trans('msg.contact_person_department_limit') }}",
                        },
                    },
                    errorPlacement: function (label, element) {
                        label.addClass('mt-2 text-danger');
                        label.insertAfter(element);
                    },
                    highlight: function (element, errorClass) {
                        $(element).parent().addClass('has-danger')
                        $(element).addClass('form-control-danger')
                    }, submitHandler: function (form, event) {
                        if (!this.beenSubmitted) {
                            event.preventDefault();
                            contact_form_submit_by_ajax();
                        }
                    }
                });
            }
            function contact_form_submit_by_ajax()
            {
                var url = $('#addUpdateForm').attr('action');
                add_update_details('addUpdateForm',url,'add_contact_person_modal','searchData');
            }
            function phoneFormat(input) {//returns (###) ###-####
                input = input.replace(/\D/g,'').substring(0,10); //Strip everything but 1st 10 digits
                var size = input.length;
                if (size>0) {input=""+input}
                if (size>3) {input=input.slice(0,3)+"- "+input.slice(3)}
                if (size>6) {input=input.slice(0,8)+"-" +input.slice(8)}
                return input;
            }

        </script>
    @endif

@endsection
