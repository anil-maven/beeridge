@php $assets_url = asset('/assets') @endphp
@php $segment1 = Request::segment(1) @endphp

<div class="sty_inp_bg">
    <div class="item_details">
        <h5 class="sty_title">RFQ Details</h5>

        <div class="row">
            <div class="col-md-12">
                <label>Quantity</label>
                <p>
                    @if(isset($proposal_item_data->qty) && !empty($proposal_item_data->qty))
                        {{ $proposal_item_data->qty }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-12">
                <label>Description</label>
                <p>
                    @if(isset($proposal_item_data->description) && !empty($proposal_item_data->description))
                        {{ $proposal_item_data->description }}
                    @else
                        NA
                    @endif
                </p>
            </div>
        </div>
    </div>
    @if(isset($quotation_data) && !empty($quotation_data))
        @if($segment1 == 'rfq')
        <hr>
        <div class="item_details">
            <h5 class="sty_title {{ $segment1 }}">Supplier Details</h5>
            <div class="row">
                <div class="col-md-4">
                    <label>Supplier</label>
                    <p>
                        @if(isset($quotation_data->contact_person->supplier) && !empty($quotation_data->contact_person->supplier))
                            {{ $quotation_data->contact_person->supplier }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Name</label>
                    <p>
                        @if(isset($quotation_data->contact_person->name) && !empty($quotation_data->contact_person->name))
                            {{ $quotation_data->contact_person->name }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Department</label>
                    <p>
                        @if(isset($quotation_data->contact_person->department) && !empty($quotation_data->contact_person->department))
                            {{ $quotation_data->contact_person->department }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Email</label>
                    <p>
                        @if(isset($quotation_data->contact_person->email) && !empty($quotation_data->contact_person->email))
                            {{ $quotation_data->contact_person->email }}
                        @else
                            NA
                        @endif
                    </p>
                </div>


            </div>
        </div>
        @endif
        <hr>
        <div class="item_details">
            <h5 class="sty_title">Quotation Details</h5>
            <div class="row">
                <div class="col-md-4">
                    <label>Quoted Part Number</label>
                    <p>
                        @if(isset($quotation_data->quoted_part_number) && !empty($quotation_data->quoted_part_number))
                            {{ $quotation_data->quoted_part_number }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Alternate 1/ Offered</label>
                    <p>
                        @if(isset($quotation_data->alternate) && !empty($quotation_data->alternate))
                            {{ $quotation_data->alternate }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>NSN</label>
                    <p>
                        @if(isset($quotation_data->nsn) && !empty($quotation_data->nsn))
                            {{ $quotation_data->nsn }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>OEM</label>
                    <p>
                        @if(isset($quotation_data->oem) && !empty($quotation_data->oem))
                            {{ $quotation_data->oem }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>RFQ Platform</label>
                    <p>
                        @if(isset($quotation_data->rfq_platform) && !empty($quotation_data->rfq_platform))
                            {{ $quotation_data->rfq_platform }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Quote Via</label>
                    <p>
                        @if(isset($quotation_data->quote_via) && !empty($quotation_data->quote_via))
                            {{ $quotation_data->quote_via }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Quote Reference</label>
                    <p>
                        @if(isset($quotation_data->quote_reference) && !empty($quotation_data->quote_reference))
                            {{ $quotation_data->quote_reference }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Condition</label>
                    <p>
                        @if(isset($quotation_data->condition) && !empty($quotation_data->condition))
                            {{ $quotation_data->condition }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Delivery Time</label>
                    <p>
                        @if(isset($quotation_data->delivery_time) && !empty($quotation_data->delivery_time))
                            {{ $quotation_data->delivery_time }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Trace Certificate</label>
                    <p>
                        @if(isset($quotation_data->trace_certificate) && !empty($quotation_data->trace_certificate))
                            {{ $quotation_data->trace_certificate }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Shipping</label>
                    <p>
                        @if(isset($quotation_data->shipping) && !empty($quotation_data->shipping))
                            {{ $quotation_data->shipping }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Payment Terms</label>
                    <p>
                        @if(isset($quotation_data->payment_terms) && !empty($quotation_data->payment_terms))
                            {{ $quotation_data->payment_terms }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Description</label>
                    <p>
                        @if(isset($quotation_data->description) && !empty($quotation_data->description))
                            {{ $quotation_data->description }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <label>Remarks</label>
                    <p>
                        @if(isset($quotation_data->remarks) && !empty($quotation_data->remarks))
                            {{ $quotation_data->remarks }}
                        @else
                            NA
                        @endif
                    </p>
                </div>
            </div>
        </div>
    @endif
    <hr>
    <div class="item_details">
        <h5 class="sty_title">Item Details</h5>

        <div class="row">
            <div class="col-md-4">
                <label>Part Name</label>
                <p>
                    @if(isset($data->name) && !empty($data->name))
                        {{ $data->name }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>Part No.</label>
                <p>
                    @if(isset($data->part_number) && !empty($data->part_number))
                        {{ $data->part_number }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>ATL PN</label>
                <p>
                    @if(isset($data->atl_pn) && !empty($data->atl_pn))
                        {{ $data->atl_pn }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>ATL 2 PN</label>
                <p>
                    @if(isset($data->atl_2_pn) && !empty($data->atl_2_pn))
                        {{ $data->atl_2_pn }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>ATL 2 PN</label>
                <p>
                    @if(isset($data->atl_2_pn) && !empty($data->atl_2_pn))
                        {{ $data->atl_2_pn }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>UI</label>
                <p>
                    @if(isset($data->ui) && !empty($data->ui))
                        {{ $data->ui }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>NSN</label>
                <p>
                    @if(isset($data->nsn) && !empty($data->nsn))
                        {{ $data->nsn }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>NSN 2</label>
                <p>
                    @if(isset($data->nsn_2) && !empty($data->nsn_2))
                        {{ $data->nsn_2 }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>Cage Code</label>
                <p>
                    @if(isset($data->cage_code) && !empty($data->cage_code))
                        {{ $data->cage_code }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>Manufacturer</label>
                <p>
                    @if(isset($data->manufacturer) && !empty($data->manufacturer))
                        {{ $data->manufacturer }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>NIIN</label>
                <p>
                    @if(isset($data->niin) && !empty($data->niin))
                        {{ $data->niin }}
                    @else
                        NA
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label>Codification Country</label>
                <p>
                    @if(isset($data->codification_country) && !empty($data->codification_country))
                        {{ $data->codification_country }}
                    @else
                        NA
                    @endif
                </p>
            </div>
        </div>
        <div class="row">
            @if(isset($data->basic_details) && !empty($data->basic_details))
                <div class="col-md-6">
                    <label>Basic Details</label>
                    <p>{{ $data->basic_details }}</p>
                </div>
            @endif
            @if(isset($data->description) && !empty($data->description))
                <div class="col-md-6">
                    <label>Description</label>
                    <p>{{ $data->description }}</p>
                </div>
            @endif
        </div>
        <div class="row mt-5">
            @if(isset($data->part_image_url) && !empty($data->part_image_url))
                <div class="col-md-6">
                    <label>Image</label>
                    <img src="{{ $data->part_image_url }}" height="300" class="img-fluid">
                </div>
            @endif
            @if(isset($data->part_video_url) && !empty($data->part_video_url))
                <div class="col-md-6">
                    <label class="w-100">Video</label>
                    <video height="300" controls>
                        <source src="{{ $data->part_video_url }}" type="video/mp4">
                        <source src="{{ $data->part_video_url }}" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>
                </div>
            @endif
        </div>
    </div>
</div>
</div>
