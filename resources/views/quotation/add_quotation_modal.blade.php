@php $assets_url = asset('/assets') @endphp
<style>
    .form-control:disabled, .form-control[readonly] {
        background-color: #fff;
    }
</style>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">@if(isset($data->id) && !empty($data->id)) Edit @else Add @endif Price @if(isset($proposal_data->part_data->part_number) && !empty($proposal_data->part_data->part_number)) for "{{ $proposal_data->part_data->part_number }}" item @endif</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class="tabcontent_wrapper ">
            <form class="mt-4" id="addUpdateQuotationForm" action="{{ url('/rfq/quotation/add-update-quotation') }}" method="POST">
              <div class="sty_inp_bg">
                <input type="hidden" name="id"  value="@if(isset($data->id) && !empty($data->id)){{ $data->id }}@else 0 @endif">
                <input type="hidden" name="proposal_item_id"  value="{{ $proposal_item_id }}">
                @csrf
                <div class="form-row pt-3">
                    <div class="form-group col-md-6">
                        <select name="contact_person_id" id="contact_person_id" class="form-control">
                            @if(isset($data->contact_person->id) && !empty($data->contact_person->id))
                                <option value="{{ $data->contact_person->id }}">{{ $data->contact_person->name }} @if(!empty($data->contact_person->email)) ({{ $data->contact_person->email }}) @endif</option>
                            @endif
                        </select>
                        <label  class="error mt-1 text-danger common-error" id="contact_person_id_error" for="contact_person_id_name"></label>
                    </div>
                  {{--  <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="supplier_company"
                               id="supplier_company"
                               value="@if(isset($data->supplier_company)){{ $data->supplier_company }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Supplier Company *</label>
                        <label  class="error mt-1 text-danger common-error" id="supplier_company_error" for="supplier_company"></label>
                    </div>--}}
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="quoted_part_number"
                               id="quoted_part_number"
                               value="@if(isset($data->quoted_part_number)){{ $data->quoted_part_number }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Quoted Part Number</label>
                        <label  class="error mt-1 text-danger common-error" id="quoted_part_number_error" for="quoted_part_number"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="alternate"
                               id="alternate"
                                value="@if(isset($data->alternate)){{ $data->alternate }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Alternate 1/ Offered</label>
                        <label  class="error mt-1 text-danger common-error" id="alternate_error" for="alternate"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="nsn"
                               id="nsn"
                               value="@if(isset($data->nsn)){{ $data->nsn }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">NSN</label>
                        <label  class="error mt-1 text-danger common-error" id="nsn_error" for="nsn"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="oem"
                               id="oem"
                               value="@if(isset($data->oem)){{ $data->oem }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">OEM</label>
                        <label  class="error mt-1 text-danger common-error" id="oem_error" for="oem"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="rfq_platform"
                               id="rfq_platform"
                               value="@if(isset($data->rfq_platform)){{ $data->rfq_platform }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Rfq Platform</label>
                        <label  class="error mt-1 text-danger common-error" id="rfq_platform_error" for="rfq_platform"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="quote_via"
                               id="quote_via"
                               value="@if(isset($data->quote_via)){{ $data->quote_via }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Quote Via</label>
                        <label  class="error mt-1 text-danger common-error" id="quote_via_error" for="quote_via"></label>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="quote_reference"
                               id="quote_reference"
                               value="@if(isset($data->quote_reference)){{ $data->quote_reference }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Quote Reference</label>
                        <label  class="error mt-1 text-danger common-error" id="quote_reference_error" for="quote_reference"></label>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="condition"
                               id="condition"
                               value="@if(isset($data->condition)){{ $data->condition }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Condition</label>
                        <label  class="error mt-1 text-danger common-error" id="condition_error" for="condition"></label>
                    </div>

                    <div class="form-group col-md-6 @if(isset($proposal_data->part_data->ui) && !empty($proposal_data->part_data->ui)) input-group @endif">
                        <input type="number"
                               class="form-control"
                               name="unit_price"
                               min="1"
                               max="99999999"
                               step="0.01"
                               id="unit_price"
                               value="@if(isset($data->unit_price)){{ $data->unit_price }}@endif"
                               placeholder=" ">
                               <label class="md-form-lable">Unit Price * </label>
                               @if(isset($proposal_data->part_data->ui) && !empty($proposal_data->part_data->ui))
                               <div class="input-group-append">
                                    <span class="input-group-text">{{ $proposal_data->part_data->ui }}</span>
                                </div>
                                @endif
                       
                        <label  class="error mt-1 text-danger common-error" style="width:100%" id="unit_price_error" for="unit_price"></label>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="delivery_time"
                               id="delivery_time"
                               value="@if(isset($data->delivery_time)){{ $data->delivery_time }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Delivery / Lead Time</label>
                        <label  class="error mt-1 text-danger common-error" id="delivery_time_error" for="delivery_time"></label>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="trace_certificate"
                               id="trace_certificate"
                               value="@if(isset($data->trace_certificate)){{ $data->trace_certificate }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Trace / Certificate</label>
                        <label  class="error mt-1 text-danger common-error" id="trace_certificate_error" for="trace_certificate"></label>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="shipping"
                               id="shipping"
                               value="@if(isset($data->shipping)){{ $data->shipping }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Shipping</label>
                        <label  class="error mt-1 text-danger common-error" id="shipping_error" for="shipping"></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text"
                               class="form-control"
                               name="payment_terms"
                               id="payment_terms"
                               value="@if(isset($data->payment_terms)){{ $data->payment_terms }}@endif"
                               placeholder=" ">
                        <label class="md-form-lable">Payment Terms</label>
                        <label  class="error mt-1 text-danger common-error" id="payment_terms_error" for="payment_terms"></label>
                    </div>

                    <div class="form-group col-md-6">
                          <textarea class="form-control"
                                    rows="5"
                                    name="description"
                                    id="description"
                                    placeholder=" ">@if(isset($data->description)){{ $data->description }}@endif</textarea>
                       <label class="md-form-lable">Description</label>
                        <label  class="error mt-1 text-danger common-error" id="description_error" for="description"></label>
                    </div>
                    <div class="form-group col-md-6">
                          <textarea class="form-control"
                                    rows="5"
                                    name="remarks"
                                    id="remarks"
                                    placeholder=" ">@if(isset($data->remarks)){{ $data->remarks }}@endif</textarea>
                        <label class="md-form-lable">Remarks</label>
                        <label  class="error mt-1 text-danger common-error" id="remarks_error" for="remarks"></label>
                    </div>


                    </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group cta_btn">
                            <button type="submit" id="addUpdateQuotationForm_btn" class="btn search_btn">Save</button>
                            <button type="button" class="btn clear_btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>


            </form>
        </div>
    </div>
</div><!-- modal-content -->


