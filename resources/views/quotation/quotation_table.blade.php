@if(isset($data) && count($data) != 0)
    @php $delete_permission = 1 @endphp
    @php $edit_permission = 1 @endphp
  {{--  @if(role('RFQ','All','delete_permission'))
        @php $delete_permission = 1 @endphp
    @endif
    @if(role('RFQ','All','read_permission'))
        @php $edit_permission = 1 @endphp
    @endif--}}

    <div class="col-md-12">
        <div class="responsive_table manage_contacts_table sty_proposal_table" >
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Quoted Part Number</th>
                    <th>Supplier Company</th>
                    <th>Qty</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data AS $key => $value)
                    <tr>
                        <td class="td_one">
                            <label class="custom_checkbox">{{ ($value->quoted_part_number)?$value->quoted_part_number:"NA" }}
                                <input type="radio"
                                       name="quotation_radio"
                                       onclick="show_quotation_status_btn()"
                                       value="{{ $value->id }}"
                                        @if($value->flag == 1)
                                            checked
                                        @endif
                                >
                                <span class="checkmark"></span>
                            </label>
                        </td>
                      {{--  <td class="icon_td">{{ $value->req_part_number }}</td>--}}
                      {{--  <td class="icon_td">{{ $value->quoted_part_number }}</td>--}}
                        <td class="icon_td">{{ (@$value->contact_person->supplier)?$value->contact_person->supplier:'NA' }}</td>
                        <td class="icon_td">{{ $value->proposal_item->qty }}</td>
                        <td class="icon_td">{{ convert($value->unit_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }} @endif</td>
                        <td class="icon_td">{{ convert($value->proposal_item->qty*$value->unit_price) }} @if(isset($value->ui) && !empty($value->ui)){{ $value->ui }} @endif</td>
                        <td class="icon_td sty_action">
                            @if($edit_permission == 1)
                                <button type="button" onclick="edit_quotation('{{ $value->id }}')" class="btn common_btn"><i class="fas fa-edit"></i></button>
                            @endif
                             @if($delete_permission == 1)
                                 <button type="button" onclick="delete_quotation('{{ $value->id }}')" class="btn common_btn"><i class="far fa-trash-alt"></i></button>
                             @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->render("layouts.pagination") }}


@else
    <div class="not_found">
        <p style="text-align: center;">Record Not Found</p>
    </div>
@endif


