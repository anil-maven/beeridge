<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('created_by');
            $table->string('serial_no')->unique()->index();
            $table->string('part_number')->unique()->index();
            $table->string('name')->nullable();
            $table->string('atl_pn')->nullable();
            $table->string('atl_2_pn')->nullable();
            $table->text('description')->nullable()->comment('SPECS / DESCRIPTION');
            $table->string('ui')->nullable()->comment('U/I');
            $table->string('nsn')->nullable();
            $table->string('nsn_2')->nullable();
            $table->string('oem')->nullable();
            $table->text('basic_details')->nullable();
            $table->string('cage_code',10)->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('niin')->nullable();
            $table->string('codification_country')->nullable();
            $table->string('part_image')->nullable();
            $table->string('part_video')->nullable();
            $table->boolean('flag')->default(0)->comment('0:manual add,1:import data');
            $table->boolean('status')->default(1)->comment('1:active,0:block');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
