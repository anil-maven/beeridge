<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddItemStatusToProposalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal_items', function (Blueprint $table) {
            $table->boolean('item_status')->default(0)->comment('0:pending,1:search price,2:select price,3:update new price,4:submit quotation')->after('new_client_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal_items', function (Blueprint $table) {
            $table->dropColumn('item_status');
        });
    }
}
