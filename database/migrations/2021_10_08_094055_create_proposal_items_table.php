<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_items', function (Blueprint $table) {
            $table->id()->index();
            $table->unsignedBigInteger('proposal_id')->comment('ID from proposals table')->index();
            $table->unsignedBigInteger('part_id')->comment('ID from parts table')->index();
            $table->integer('qty')->default(0);
            $table->text('description')->nullable();
            $table->foreign('part_id')->references('id')->on('parts')->onDelete('cascade');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal_items');
    }
}
