<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permissions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_id');
            $table->string('module_name');
            $table->string('sub_module_name')->nullable();
            $table->boolean('select_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('read_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('write_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('create_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('delete_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('submit_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('cancel_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('amend_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('print_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('email_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('report_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('import_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('export_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('share_permission')->default(1)->comment('1:active,0:block');
            $table->boolean('set_user_permission')->default(1)->comment('1:active,0:block');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
    }
}
