<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id()->index();
            $table->string('first_name',20)->index();
            $table->string('last_name',20)->index();
            $table->integer('created_by')->default(0)->nullable();
            $table->string('email',50)->unique()->index();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('image',50)->nullable();
            $table->integer('role_id')->default(0)->index()->comment('id from "roles" table');
            $table->boolean('status')->default(1)->comment('1:active,0:deactive');
            $table->boolean('is_online')->default(0)->comment('1:yes,0:no');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
