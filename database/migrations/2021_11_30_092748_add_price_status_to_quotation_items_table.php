<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceStatusToQuotationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->boolean('price_status')->after('flag')->default(0)->comment('0:price for quotation,1:price from price mudule');
            $table->unsignedBigInteger('proposal_item_id')->nullable()->change();
            $table->integer('part_id')->default(0)->after('proposal_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->dropColumn('price_status');
            $table->dropColumn('part_id');
        });
    }
}
