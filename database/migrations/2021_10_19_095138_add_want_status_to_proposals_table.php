<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWantStatusToProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->tinyInteger('client_want_status')->default(0)->comment('1:pricing,2:purchase,3:research,4:other')->after('status');
            $table->date('faq_receive_date')->nullable()->after('client_want_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
           // $table->dropColumn('client_want_status');
           // $table->dropColumn('faq_receive_date');
        });
    }
}
