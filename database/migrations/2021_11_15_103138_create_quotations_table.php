<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('created_by')->comment('id from users table');
            $table->unsignedBigInteger('proposal_item_id')->comment('id from proposal_items table');
            $table->string('req_part_number')->nullable()->comment('requested part no.');
            $table->string('quoted_part_number')->nullable()->comment('Quoted part no.');
            $table->string('alternate')->nullable()->comment('ALTERNATE 1/ OFFERED');
            $table->text('description')->nullable();
            $table->string('nsn')->nullable();
            $table->string('oem')->nullable();
            $table->string('rfq_platform')->nullable();
            $table->string('quote_via')->nullable();
            $table->string('quote_reference')->nullable();
            $table->string('supplier_company')->nullable();
            $table->integer('contact_person_id')->default(0)->nullable()->comment('id from contact_person table');
            $table->string('condition')->nullable();
            $table->float('unit_price')->default(0)->nullable();
            $table->string('delivery_time')->nullable()->comment('DELIVERY /LEAD TIME');
            $table->string('trace_certificate')->nullable()->comment('TRACE / CERTIFICATE');
            $table->string('shipping')->nullable();
            $table->string('payment_terms')->nullable();
            $table->text('remarks')->nullable();
            $table->boolean('flag')->default(0)->comment('1:selected quotation');
            $table->foreign('proposal_item_id')->references('id')->on('proposal_items')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
