<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('created_by');
            $table->integer('reporting_to')->default(0)->comment('role_id default is 0');
            $table->string('role_name',50)->index();
            $table->text('modules')->nullable();
            $table->boolean('flag')->default(1)->comment('0:default roles which is not deleted,1:custom');
            $table->boolean('status')->index()->default(1)->comment('1:active,0:deactive');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
