<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClientPriceToProposalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal_items', function (Blueprint $table) {
            $table->string('client_price_type')->nullable()->after('description');
            $table->double('client_price')->default(0)->nullable()->after('client_price_type');
            $table->double('total_client_price')->default(0)->nullable()->after('client_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal_items', function (Blueprint $table) {
            $table->dropColumn('client_price_type');
            $table->dropColumn('client_price');
            $table->dropColumn('total_client_price');
        });
    }
}
