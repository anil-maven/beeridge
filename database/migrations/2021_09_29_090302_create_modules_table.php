<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->id()->index();
            $table->string('module_name',50)->index();
            $table->integer('parent_id')->index()->default(0)->comment('ID from modules table for sub modules');
            $table->string('url')->nullable();
            $table->string('icon',50)->nullable();
            $table->integer('order_no')->default(1)->index();
            $table->boolean('status')->default(1)->comment('1:active,0:deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
