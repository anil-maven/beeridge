<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminData = User::where('role_id','=',1)->first();
        if(empty($adminData))
        {
            $adminModel = new User();
            $adminModel->first_name = 'Beeridge';
            $adminModel->last_name = 'Admin';
            $adminModel->role_id = 1;
            $adminModel->email = 'admin@mailinator.com';
            $adminModel->password = bcrypt('Admin@123');
            $adminModel->save();
        }
        else{
            $adminModel = User::find($adminData->id);
            $adminModel->first_name = 'Beeridge';
            $adminModel->last_name = 'Admin';
            $adminModel->role_id = 1;
            $adminModel->email = 'admin@mailinator.com';
            $adminModel->password = bcrypt('Admin@123');
            $adminModel->save();
        }
    }
}
