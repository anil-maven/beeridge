<?php

namespace Database\Seeders;

use App\Models\Module;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleArr = Module::where('status','=',1)->where('parent_id','=',0)->pluck('module_name');
        $moduleArr = Module::where('status','=',1)->where('parent_id','=',0)->pluck('module_name');
        $newModuleArr = [];
        foreach ($moduleArr as $key => $value)
        {
            $newModuleArr[$value] = ['All'];
        }
        $moduleData = json_encode($newModuleArr);
        $arr = [
            ['id' => 1,'created_by' => 1,'role_name' => 'Super Admin','reporting_to' => '0','flag' => 0,'status' => 1],
            ['id' => 2,'created_by' => 1,'role_name' => 'Service Provider','reporting_to' => '1','flag' => 0,'status' => 1],
            ['id' => 3,'created_by' => 1,'role_name' => 'Client','reporting_to' => '1','flag' => 0,'status' => 1],
        ];
        foreach ($arr as $key => $value)
        {
            $roleModel = Role::firstOrNew(['id' => $value['id']]);
            $roleModel->id = $value['id'];
            $roleModel->created_by = $value['created_by'];
            $roleModel->role_name = $value['role_name'];
            $roleModel->modules = $moduleData;
            $roleModel->reporting_to = $value['reporting_to'];
            $roleModel->flag = $value['flag'];
            $roleModel->status = $value['status'];
            $response = $roleModel->save();

            foreach ($moduleArr as $key2 => $value2)
            {
                $rolePermissionModel = RolePermission::firstOrNew(['role_id' => $value['id'],'module_name' => $value2,'sub_module_name' => 'All']);
                $rolePermissionModel->role_id =$value['id'];
                $rolePermissionModel->module_name = $value2;
                $rolePermissionModel->sub_module_name = 'All';
                $rolePermissionModel->save();
            }
        }
    }
}
