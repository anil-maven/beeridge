<?php

namespace Database\Seeders;

use App\Models\Module;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ['id' => 1,'parent_id' => 0,'module_name' => 'Task','url' => '/task','icon' => '/public/assets/img/proposal.png','status' => 0,'order_no' => 1],
            ['id' => 2,'parent_id' => 0,'module_name' => 'Contracts','url' => '/contracts','icon' => '/public/assets/img/contracts_main.png','status' => 0,'order_no' => 2],
            ['id' => 3,'parent_id' => 0,'module_name' => 'Parts','url' => '/parts','icon' => '/public/assets/img/recruirment_icon.svg','status' => 1,'order_no' => 3],
            ['id' => 4,'parent_id' => 0,'module_name' => 'Inventory','url' => '/inventory','icon' => '/public/assets/img/report_icon.svg','status' => 1,'order_no' => 4],
            ['id' => 5,'parent_id' => 0,'module_name' => 'Role & Permission','url' => '/role-and-permission','icon' => '/public/assets/img/report_icon.svg','status' => 0,'order_no' => 5],
            ['id' => 6,'parent_id' => 0,'module_name' => 'HRM','url' => '/users','icon' => '/public/assets/img/recruirment_icon.svg','status' => 0,'order_no' => 6],
            ['id' => 7,'parent_id' => 0,'module_name' => 'RFQ','url' => '/rfq','icon' => '/public/assets/img/proposal.png','status' => 0,'order_no' => 7],
            ['id' => 8,'parent_id' => 0,'module_name' => 'Supplier','url' => '/contact-person','icon' => '/public/assets/img/recruirment_icon.svg','status' => 1,'order_no' => 8],
            ['id' => 9,'parent_id' => 0,'module_name' => 'Prices','url' => '/prices','icon' => '/public/assets/img/recruirment_icon.svg','status' => 1,'order_no' => 9],
        ];
        foreach ($arr as $key => $value)
        {
            $moduleModel = Module::firstOrNew(['id' => $value['id']]);
            $moduleModel->id = $value['id'];
            $moduleModel->parent_id = $value['parent_id'];
            $moduleModel->module_name = $value['module_name'];
            $moduleModel->url = $value['url'];
            $moduleModel->icon = $value['icon'];
            $moduleModel->status = $value['status'];
            $moduleModel->order_no = $value['order_no'];
            $moduleModel->save();
        }
    }
}
