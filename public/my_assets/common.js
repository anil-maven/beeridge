function success_msg(msg)
{
    swal('Success',msg,'success');
}
function error_msg(msg)
{
    swal('Error',msg,'error');
}

function add_update_details(form_id,url,model_id = "",callback_search = "")
{
    $('.common-error').hide();
    $('#'+form_id+'_btn').prop('disabled', true);
 /*   $('.form-custom-btn').buttonLoader('stop');*/
    var data = $('#' + form_id).serialize();
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (response)
        {
            success_msg(response.msg);
            if(model_id != "")
            {
                $('#'+model_id).modal('hide');
            }
            if(callback_search == "searchData")
            {
                searchData(1);
            }
            if(form_id == 'proposalItemForm')
            {
                $('#no_of_item_count').text(response.item_count);
                if(response.service_provider)
                {
                    $('#service_provider_name_box').text(response.service_provider);
                    $('#what_do_you_want_box').show();
                }
                if(response.proposal_activity_html != '')
                {
                    $('#proposal_activity_box').html(response.proposal_activity_html);
                }
                if(response.item_count == 0)
                {
                    $('#add_proposal_item_box').show();
                }
                else{
                    $('#add_proposal_item_box').hide();
                }
            }
            if(model_id == 'add_proposal_modal')
            {
                if(response.url != '')
                {
                    setTimeout(function () {
                        window.location.href = response.url;
                    }, 1000);
                }
            }
            if(callback_search == "pageReload")
            {
                setTimeout(function () {
                    location.reload(true);
                }, 3000);
            }
            $('#'+form_id+'_btn').prop('disabled', false);
        },
        error:function (reject) {
            if (reject.status === 422) {
                var errors = $.parseJSON(reject.responseText);
                $('.common-error').show();
                $.each(errors.errors, function (key, val) {
                    $("#" + key + "_error").text(val[0]);
                });
            } else {
                var responseJSON = reject.responseJSON;
                error_msg(responseJSON);
            }
            $('#'+form_id+'_btn').prop('disabled', false);
        }
    });
}

function add_update_details_with_image(form_id,url,image_id,image_name = 'image',model_id = "",callback_search = "")
{
    $('#'+form_id+'_btn').prop('disabled', true);
    $('.common-error').hide();
    var formData = $('#' + form_id).serializeArray();
    var fd = new FormData();
    var image = document.getElementById(image_id);
    fd.append(image_name, image.files[0]);
    formData.forEach(function (item,key)
    {
        fd.append(item.name, item.value);
    });
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        data: fd,
        success: function (response)
        {
            $('#'+form_id+'_btn').prop('disabled', false);
            success_msg(response.msg);
            if(model_id != "")
            {
                $('#'+model_id).modal('hide');
            }
            if(callback_search == "searchData")
            {
                searchData(1);
            }
            if(form_id == 'profileForm')
            {
                console.log(response.full_image_url);
                if(response.full_image_url)
                {
                    $('#user_full_image_url_preview').attr("src",response.full_image_url);
                }
                if(response.full_name)
                {
                    $('#user_full_name_text').text(response.full_name);
                }


            }
            if(callback_search == "pageReload")
            {
               /* setTimeout(function () {
                    location.reload(true);
                }, 5000);*/
            }

        },
        error:function (reject) {
            if (reject.status === 422) {
                var errors = $.parseJSON(reject.responseText);
                $('.common-error').show();
                var last_error = "";
                $.each(errors.errors, function (key, val) {
                    $("#" + key + "_error").text(val[0]);
                    last_error = val[0];
                });
                if(image_id == 'price_import')
                {
                    if(last_error != "")
                    {
                        error_msg(last_error);
                    }
                   
                }
            } else {
                if(image_id == 'price_import')
                {
                    var responseJSON = reject.responseJSON;
                    console.log(responseJSON);
                    $('#price_import').val('');
                    error_msg(responseJSON.msg);
                }
                else{
                    var responseJSON = JSON.parse(reject.responseJSON);
                    console.log(responseJSON);
                    error_msg(responseJSON.msg);
                }
              
            }
            $('#'+form_id+'_btn').prop('disabled', false);
            }
    });
}
function show_hide_msg(show_box,hide_box)
{
    $('#'+show_box).show();
    $('#'+hide_box).hide();
}
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
    {
        return false;
    }
    return true;
}
function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0,el.value.length - 1);
    }
}
