// Custom JS File
$( document ).ready(function() {
    $(".sidebarclose_btn").click(function() {
        $(".admin-sidebar").addClass("open");
    });
    $(".sidebar_add_btn").click(function() {
        $(".admin-sidebar").removeClass("open");
    });
    // location toggle btn
    $(".state_btn").click(function() {
        $(".city_btn").removeClass("active");
        $(this).addClass("active");
    });
    $(".city_btn").click(function() {
        $(".city_btn").addClass("active");
        $(".state_btn").removeClass("active");
    });
});
