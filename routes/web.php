<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PartController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProposalController;
use App\Http\Controllers\RFQController;
use App\Http\Controllers\ContactPersonController;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\PriceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[LoginController::class,'login_view']);
Route::post('/login',[LoginController::class,'login_web'])->name('login');
Route::get('/forget-password',[LoginController::class,'forget_password']);
Route::post('/send-forget-password-link',[LoginController::class,'send_forget_password_link']);
Route::get('reset-password/{token}', [LoginController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [LoginController::class, 'submitResetPasswordForm'])->name('reset.password.post');

Route::group(['middleware' => ['CheckAuth','RevalidateBackHistory']], function ()
{
    Route::get('/dashboard', [HomeController::class, 'dashboard_view']);
    Route::get('/contracts', [HomeController::class, 'dashboard_view']);
    Route::get('/logout',[LoginController::class,'logout']);
    Route::post('/update-profile',[HomeController::class,'update_profile']);
    Route::post('/change-password',[HomeController::class,'change_password']);
    Route::group(['prefix' => 'notification'], function ()
    {
        Route::get('/',[HomeController::class,'notification_view']);
        Route::post('/search-notification',[HomeController::class,'search_notification']);
        Route::post('/delete-single-notification',[HomeController::class,'delete_single_notification']);
    });
    Route::group(['prefix' => 'parts'], function ()
    {
        Route::get('/', [PartController::class, 'part_view']);
        Route::get('/add', [PartController::class, 'add_part_view']);
        Route::get('/edit/{part_id}', [PartController::class, 'edit_part_view']);
        Route::post('/add-update-part', [PartController::class, 'add_update_parts']);
        Route::post('/import-parts', [PartController::class, 'import_part_data']);
        Route::post('/export-parts', [PartController::class, 'export_part_data']);
        Route::post('/search-parts', [PartController::class, 'search_parts']);
        Route::post('/delete-single-part', [PartController::class, 'delete_single_part']);
    });
    Route::group(['prefix' => 'inventory'], function ()
    {
        Route::get('/', [InventoryController::class, 'inventory_view']);
        Route::post('/get-parts-list', [PartController::class, 'get_part_list']);
        Route::post('/add-inventory', [InventoryController::class, 'add_part_inventory']);
        Route::post('/search-inventory', [InventoryController::class, 'search_inventory']);
        Route::post('/delete-single-inventory', [InventoryController::class, 'delete_single_inventory']);
    });
    Route::group(['prefix' => 'role-and-permission'], function ()
    {
        Route::get('/', [RoleController::class, 'role_permission_view']);
        Route::post('/search-roles',[RoleController::class,'search_roles']);
        Route::post('/get-role-permission',[RoleController::class,'get_role_permission_view']);
        Route::post('/update-role-permission',[RoleController::class,'update_permission_status']);
        Route::get('/single-role-detail/{role_id}',[RoleController::class,'get_single_role_detail']);
        Route::post('/add-update-roles',[RoleController::class,'add_update_roles']);
        Route::post('/delete-single-role',[RoleController::class,'delete_single_role']);
    });
    Route::group(['prefix' => 'users'], function ()
    {
        Route::get('/', [UserController::class, 'user_view']);
        Route::post('/add-update-user', [UserController::class, 'add_update_user']);
        Route::post('/search-users', [UserController::class, 'search_users']);
        Route::get('/single-user-detail/{user_id}', [UserController::class, 'single_user_detail']);
        Route::post('/delete-single-user', [UserController::class, 'delete_single_user']);
        Route::post('/export-users', [UserController::class, 'user_export_data']);
        Route::post('/update-user-status', [UserController::class, 'update_user_status']);
    });
    Route::group(['prefix' => 'proposal'], function ()
    {
        Route::get('/', [ProposalController::class, 'proposal_view']);
        Route::post('/add-update-proposal', [ProposalController::class, 'add_update_proposal']);
        Route::post('/search-proposal', [ProposalController::class, 'search_proposal']);
        Route::get('/single-proposal-detail/{proposal_id}', [ProposalController::class, 'single_proposal_detail']);
        Route::post('/delete-single-proposal', [ProposalController::class, 'delete_single_proposal']);
        Route::get('/view/{id}', [ProposalController::class, 'single_proposal_view']);
        Route::post('/get-parts-list', [ProposalController::class, 'get_part_list']);
        Route::get('/single-item-detail/{part_id}/{proposal_id}', [ProposalController::class, 'single_item_detail']);
        Route::post('/add-item', [ProposalController::class, 'add_update_proposal_item']);
        Route::post('/search-proposal-item', [ProposalController::class, 'search_proposal_item']);
        Route::get('/single-proposal-item/{id}', [ProposalController::class, 'single_proposal_item']);
        Route::post('/delete-proposal-item', [ProposalController::class, 'delete_proposal_item']);
        Route::post('/update-client-want-status', [ProposalController::class, 'update_client_want_status']);
        Route::get('/new-item-add/{proposal_id}', [PartController::class, 'add_part_view']);
        Route::get('/quotation/{id}', [QuotationController::class, 'client_proposal_quotation']);
        Route::post('/search-client-quotation', [QuotationController::class, 'search_client_quotation']);
        Route::get('/single-rfq-item-with-quotation/{id}', [QuotationController::class, 'single_rfq_item_with_quotation']);
        Route::post('/export-client-quotation', [QuotationController::class, 'export_client_quotation']);
        Route::post('/quotation/update-client-new-price', [QuotationController::class, 'update_client_new_price']);
    });
    Route::group(['prefix' => 'rfq'], function ()
    {
        Route::get('/', [RFQController::class, 'rfq_view']);
        Route::post('/search-rfq', [RFQController::class, 'search_rfq']);
        Route::get('/view/{id}', [RFQController::class, 'single_rfq_view']);
        Route::post('/search-rfq-item', [RFQController::class, 'search_rfq_item']);
        Route::get('/single-rfq-item/{id}', [RFQController::class, 'single_rfq_item']);
        Route::get('/details/{id}', [RFQController::class, 'rfq_details']);
        Route::post('/search-item-details', [RFQController::class, 'search_rfq_item_details']);
        Route::post('/update-client-price', [RFQController::class, 'update_client_price']);
        Route::post('/submit-quotation', [QuotationController::class, 'submit_quotation_to_client']);
        Route::get('/single-rfq-item-with-quotation/{id}', [QuotationController::class, 'single_rfq_item_with_quotation']);
        Route::group(['prefix' => 'quotation'], function ()
        {
            Route::get('/{proposal_item_id}', [QuotationController::class, 'quotation_view']);
            Route::post('/get-contact-person-list', [ContactPersonController::class, 'get_contact_person_list']);
            Route::post('/add-update-quotation', [QuotationController::class, 'add_update_quotation']);
            Route::post('/search-quotation', [QuotationController::class, 'search_quotation']);
            Route::get('/single-quotation/{quotation_id}', [QuotationController::class, 'get_single_quotation']);
            Route::post('/delete-quotation', [QuotationController::class, 'delete_quotation']);
            Route::post('/update-quotation-status', [QuotationController::class, 'update_quotation_status']);
        });

    });
    Route::group(['prefix' => 'contact-person'], function ()
    {
        Route::get('/', [ContactPersonController::class, 'contact_person_view']);
        Route::post('/add-update-person', [ContactPersonController::class, 'add_update_contact_person']);
        Route::post('/search-person', [ContactPersonController::class, 'search_contact_person']);
        Route::get('/single-contact-person/{person_id}', [ContactPersonController::class, 'single_contact_person']);
        Route::post('/delete-single-contact-person', [ContactPersonController::class, 'delete_contact_person']);
    });
    Route::group(['prefix' => 'prices'], function ()
    {
        Route::get('/', [PriceController::class, 'price_view']);
        Route::post('/add-update-price', [PriceController::class, 'add_update_price'])->name('add_update_price');
        Route::post('/search-price', [PriceController::class, 'search_price'])->name('search_price_data');
        Route::get('/single-price/{id}', [PriceController::class, 'single_price'])->name('edit_part_price');
        Route::post('/delete-price', [PriceController::class, 'delete_price'])->name('delete_part_price');
        Route::post('/part-price-history', [PriceController::class, 'part_price_history'])->name('part_price_history');
        Route::post('/import-prices', [PriceController::class, 'import_price_data'])->name('import_prices');
        Route::get('/view-price-details/{price_id}', [PriceController::class, 'view_price_details'])->name('show_part_price');
        
    });

});


